from server to client
-----------------

Game name packet:

"cmd", "getName"
"name", name_of_game
"size", int_num_of_players
"has", int_amount_of_logged_players

>>>>>>>>>>>

Player name packet (other player):

"cmd", "player"
"action", "add"
"index", id
"name", name_of_player
"color", color_of_player

>>>>>>>>>>>

Player left the game:

"cmd", "player"
"action", "remove"
"index", id
"name", name_of_player

>>>>>>>>>>>

Start game (to client):

"cmd", "start"
"name", name_of_other_player
"my_index", my_id
"my_color", my_color
"other_color", other_color
"other_index", other_id

>>>>>>>>>>>

Start game (intern, i.e. to Server application)

"cmd", "start"
"names", [name1, ..., nameN]
"colors", [color1, ..., colorN]

>>>>>>>>>>>

Exception:

"cmd", "exception"
"text", text

>>>>>>>>>>>

ColorOK:

"cmd", "color"
"value", [cholor1, ..., colorN]
"color", "ok"			// only if the color is still available

>>>>>>>>>>>

A client rejoined:

"cmd", "rejoined"
"id", id

>>>>>>>>>>>

from client to server:
----------------

Player name

"cmd", "player"
"action", "name"
"name", client_name
"color", color

>>>>>>>>>>>

Player choice:

"cmd", "player"
"action","choice"
"index", index_of_player

>>>>>>>>>>>

Leave:
"cmd", "leave"

>>>>>>>>>>>

