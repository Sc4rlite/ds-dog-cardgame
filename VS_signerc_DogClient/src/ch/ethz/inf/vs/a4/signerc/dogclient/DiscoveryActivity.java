package ch.ethz.inf.vs.a4.signerc.dogclient;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionListener;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionManager;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionUtils;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionUtils.Color;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionUtils.State;


public class DiscoveryActivity extends ListActivity implements ConnectionListener
{

    ConnectionManager manager;
    Handler mCallbackHandler;
    ArrayAdapter<String> adapter;
    MyAbstractAdapter gameAdapter;
    int chosen = -1;
    String TAG = "DiscoveryActivity";
    String username;
    String color;

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        username = getIntent().getStringExtra("name");
        color = getIntent().getStringExtra("color");
        Log.i(TAG, "color = " + color);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // keep the screen awake

        setContentView(R.layout.activity_discovery);

        gameAdapter = new GameListAdapter();
        setListAdapter(gameAdapter);

        mCallbackHandler = new Handler();
        if (BluetoothAdapter.getDefaultAdapter() != null)
        {
            if (!BluetoothAdapter.getDefaultAdapter().isEnabled())
            {
                Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBT, 1);
            }
            else
            {
                manager = new ConnectionManager(getApplicationContext());
                manager.registerListener(this);
                manager.findGames();
            }
        }

        getListView().setOnItemClickListener(itemClickHandler);

    }

    @Override public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.discovery, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @Override public Handler getHandler()
    {
        return mCallbackHandler;
    }

    @Override public void onBackPressed()
    {
        manager.disconnect();
        super.onBackPressed();
    }

    @Override public void ReceiveEvent(final JSONObject json)
    {
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                Log.i(TAG, json.toString());
                synchronized (gameAdapter)
                {	// otherwise: concurrent changes in lists
                    try
                    {
                        String cmd = json.getString(ConnectionUtils.CMD);
                        // Add / remove a game from the list
                        if (cmd.equals(ConnectionUtils.GET_NAME))
                        {
                            // name of game
                            String size = "";
                            String name = json.getString(ConnectionUtils.VALUE);
                            String address = json.getString(ConnectionUtils.ADDRESS);
                            if (json.has(ConnectionUtils.SIZE) && json.has(ConnectionUtils.HAS))
                            {
                                size = json.getString(ConnectionUtils.HAS) + "/" + json.getString(ConnectionUtils.SIZE);
                            }
                            if (json.has(ConnectionUtils.SIZE) && json.getInt(ConnectionUtils.SIZE) == -1)
                            {
                                gameAdapter.remove(address);
                                gameAdapter.notifyDataSetChanged();
                                Log.i(TAG, "removed " + address);
                                return;
                            }
                            gameAdapter.add(name, size, address);
                            // add / remove a player
                        }
                        else if (cmd.equals(ConnectionUtils.PLAYER))
                        {
                            if (json.getString(ConnectionUtils.ACTION).equals(ConnectionUtils.ADD))
                            {
                                gameAdapter.add(json.getString(ConnectionUtils.VALUE),
                                        json.getString(ConnectionUtils.COLOR), json.getString(ConnectionUtils.INDEX));
                            }
                            else
                            {
                                if (chosen == json.getInt(ConnectionUtils.INDEX))
                                    chosen = -1;
                                gameAdapter.remove(json.getString(ConnectionUtils.INDEX));
                            }
                        }
                        else if (cmd.equals(ConnectionUtils.EXCEPTION))
                        {
                            manager.disconnect();
                        }
                        else if (cmd.equals(ConnectionUtils.COLOR))
                        {

                            if (!json.has("color"))
                            {
                                JSONArray value = json.getJSONArray("value");
                                List<String> tempColors = new ArrayList<String>();
                                for (int i = 0; i < value.length(); i++)
                                {
                                    tempColors.add(value.get(i).toString());
                                }
                                showColorDialog(tempColors);
                            }
                        }
                        else if (cmd.equals("start"))
                        {
                            manager.setState(ConnectionUtils.State.STATE_CONNECTED);
                            ConnectionUtils.manager = manager;
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.putExtra("start", new String(json.toString().getBytes())).putExtra("username",
                                    username);
                            manager.removeListener(DiscoveryActivity.this);
                            startActivityForResult(intent, 0);
                            Log.i("startGame", "ready to play");
                        }

                        gameAdapter.notifyDataSetChanged();

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 1)
        {
            manager = new ConnectionManager(getApplicationContext());
            manager.registerListener(this);
            manager.findGames();
        }
        else
        {
            manager.disconnect();
            finish();
        }
    }

    boolean listItemClicked = false;

    OnItemClickListener itemClickHandler = new OnItemClickListener()
    {
        @Override public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
        {

            Log.i(TAG, chosen + " chosen at position " + position);
            if (manager.getState() == State.STATE_DISCOVERING)
            {
                final String key = gameAdapter.getKey(position);
                new Thread(new Runnable()
                {
                    public void run()
                    {
                        if (listItemClicked)
                        {
                            return;
                        }
                        listItemClicked = true;
                        if (manager.connectTo(key))
                        {
                            JSONObject json = new JSONObject();
                            try
                            {
                                json.put(ConnectionUtils.CMD, ConnectionUtils.PLAYER);
                                json.put(ConnectionUtils.VALUE, username);
                                json.put(ConnectionUtils.COLOR, color);
                                json.put(ConnectionUtils.ACTION, ConnectionUtils.VALUE);
                                json.put(ConnectionUtils.CONNECTION, ConnectionUtils.INTERN);
                                Log.i(TAG, "attempt to send " + json.toString());
                                manager.send(json);
                                runOnUiThread(new Runnable()
                                {
                                    public void run()
                                    {
                                        gameAdapter.clear(); // clear values
                                        ((TextView)findViewById(R.id.discovery_message))
                                                .setText("Chose a player to play with:");
                                        gameAdapter = new PlayerListAdapter();
                                        setListAdapter(gameAdapter);
                                    }
                                });
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                            finally
                            {
                                listItemClicked = false;
                            }
                        }
                        else
                        {
                            gameAdapter.remove(gameAdapter.getKey(position));
                            runOnUiThread(new Runnable()
                            {
                                public void run()
                                {
                                    gameAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }
                }).start();
            }
            else if (manager.getState() == State.STATE_CONNECTING && chosen == -1 && !listItemClicked)
            {
                // send the player prefered to play with (only once)
                chosen = Integer.parseInt(gameAdapter.getKey(position));
                JSONObject json = new JSONObject();
                try
                {
                    json.put(ConnectionUtils.CMD, ConnectionUtils.PLAYER);
                    json.put(ConnectionUtils.INDEX, gameAdapter.getKey(position));
                    json.put(ConnectionUtils.ACTION, ConnectionUtils.CHOICE);
                    json.put(ConnectionUtils.CONNECTION, ConnectionUtils.INTERN);
                    manager.send(json);
                    gameAdapter.modify(gameAdapter.nameAt(position) + " [chosen]", position);
                    gameAdapter.notifyDataSetChanged();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    };

    String discovery_message;

    public void showColorDialog(List<String> usedcolors)
    {
        final TextView tv = (TextView)findViewById(R.id.discovery_message);
        discovery_message = tv.getText().toString();
        runOnUiThread(new Runnable()
        {

            @Override public void run()
            {
                tv.setText("Your color was already chosen. Please chose a different one:");
            }
        });

        LayoutInflater mInflater = LayoutInflater.from(this);
        View contentView = mInflater.inflate(R.layout.activity_discovery, null);
        LinearLayout colorLayout = (LinearLayout)contentView.findViewById(R.id.disc_color_chose_list);
        colorLayout.setVisibility(View.VISIBLE);

        for (Color c: Color.values())
        {
            if (!contains(usedcolors, c.ordinal() + ""))
            {
                ImageButton t = new ImageButton(this);
                LayoutParams lp = new LayoutParams(200, 200);
                lp.setMargins(10, 10, 10, 10);
                t.setLayoutParams(lp);

                t.setBackgroundResource(R.drawable.color);
                t.setBackgroundColor(c.hex);
                t.setContentDescription("color");
                t.setTag(c.ordinal());
                t.setOnClickListener(colorChosen);
                Log.i(TAG, c.toString());
                colorLayout.addView(t);
            }
        }

        setContentView(contentView);
        getListView().setVisibility(View.INVISIBLE);

        // for (int i = 0; i < layout.getChildCount(); i++) {
        // LinearLayout v = (LinearLayout)layout.getChildAt(i);
        // Log.i(TAG, v.toString());
        // for(int j = 0; j < v.getChildCount(); j++){
        // View btn = v.getChildAt(j);
        // if (btn instanceof ImageButton) {
        // String temp = btn.getTag().toString();
        // Log.i(TAG, temp);
        // if (contains(usedcolors, temp)) {
        // btn.setVisibility(ImageButton.INVISIBLE);
        // }
        // }
        // }
        //
        // }
    }

    private boolean contains(List<String> usedcolors, String temp)
    {
        for (String s: usedcolors)
        {
            Log.i(TAG, s + " " + temp);
            if (s.equals(temp))
                return true;
        }
        return false;
    }

    ImageButton current = null;

    OnClickListener colorChosen = new OnClickListener()
    {

        @Override public void onClick(View v)
        {
            setContentView(R.layout.activity_discovery);
            getListView().setOnItemClickListener(itemClickHandler);
            TextView tv = (TextView)findViewById(R.id.discovery_message);
            tv.setText(discovery_message);
            ImageButton bt = (ImageButton)v;
            String color = v.getTag().toString();
            bt.setImageDrawable(getResources().getDrawable(R.drawable.color_chosen));
            if (current != null)
            {
                current.setImageDrawable(getResources().getDrawable(R.drawable.color));
            }
            current = bt;
            JSONObject json = new JSONObject();
            try
            {
                json.put(ConnectionUtils.CMD, ConnectionUtils.PLAYER);
                json.put(ConnectionUtils.VALUE, username);
                json.put(ConnectionUtils.COLOR, color);
                json.put(ConnectionUtils.ACTION, ConnectionUtils.VALUE);
                json.put(ConnectionUtils.CONNECTION, ConnectionUtils.INTERN);
                manager.send(json);
            }
            catch (Exception e)
            {}
        }
    };

    // public void colorChosen(View v) {
    // TextView tv = (TextView) findViewById(R.id.discovery_message);
    // tv.setText(discovery_message);
    // getListView().setVisibility(View.VISIBLE);
    // LinearLayout layout = (LinearLayout)
    // findViewById(R.id.disc_color_chose_list);
    // layout.setVisibility(View.INVISIBLE);
    // ImageButton bt = (ImageButton) v;
    // String color = v.getTag().toString();
    // bt.setImageDrawable(getResources().getDrawable(R.drawable.color_chosen));
    // if (current != null) {
    // current.setImageDrawable(getResources().getDrawable(
    // R.drawable.color));
    // }
    // current = bt;
    // JSONObject json = new JSONObject();
    // try {
    // json.put(ConnectionUtils.CMD, ConnectionUtils.PLAYER);
    // json.put(ConnectionUtils.VALUE, username);
    // json.put(ConnectionUtils.COLOR, color);
    // json.put(ConnectionUtils.ACTION, ConnectionUtils.VALUE);
    // json.put(ConnectionUtils.CONNECTION, ConnectionUtils.INTERN);
    // manager.send(json);
    // } catch (Exception e) {
    // }
    // }

    // Adapter for holding devices found through scanning.
    private class GameListAdapter extends MyAbstractAdapter
    {
        private ArrayList<String> name;
        private ArrayList<String> amount;
        private ArrayList<String> address;

        public GameListAdapter()
        {
            super();
            name = new ArrayList<String>();
            amount = new ArrayList<String>();
            address = new ArrayList<String>();
            DiscoveryActivity.this.getLayoutInflater();
        }

        public void add(String name, String amount, String address)
        {
            if (this.address.contains(address))
            {
                remove(address);
            }
            this.name.add(name);
            this.amount.add(amount);
            this.address.add(address);

        }

        public void remove(String addr)
        {
            int i = 0;
            boolean found = false;
            for (i = 0; i < this.address.size(); i++)
            {
                if (this.address.get(i).equals(addr))
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
                this.address.remove(i);
                this.name.remove(i);
                this.amount.remove(i);
            }
        }

        public String getKey(int position)
        {
            Log.i(TAG, position + " in " + address.size());
            return address.get(position);
        }

        public void clear()
        {
            name.clear();
            address.clear();
            amount.clear();
        }

        @Override public int getCount()
        {
            return name.size();
        }

        @Override public Object getItem(int i)
        {
            return name.get(i);
        }

        @Override public long getItemId(int i)
        {
            return i;
        }

        public View getView(int position, View view, ViewGroup parent)
        {
            LayoutInflater inflater = getLayoutInflater();
            ViewHolder viewHolder;
            if (view == null)
            {
                view = inflater.inflate(R.layout.game_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.name = (TextView)view.findViewById(R.id.game_scan_name);
                viewHolder.amount = (TextView)view.findViewById(R.id.game_scan_amount);

                view.setTag(viewHolder);
            }
            else
            {
                viewHolder = (ViewHolder)view.getTag();
            }

            viewHolder.name.setText(name.get(position)); // no need to check for
                                                         // null because no
                                                         // null values in
                                                         // list
            viewHolder.amount.setText(amount.get(position));
            return view;

        }

        @Override public int getIndex(int position)
        {
            return position;
        }

        @Override public void modify(String newValue, int position)
        {
            // ignore
        }

        @Override public String nameAt(int position)
        {
            return amount.get(position);
        }
    }

    static class ViewHolder
    {
        TextView name;
        TextView amount;
    }

    // Adapter for holding other players in the game
    private class PlayerListAdapter extends MyAbstractAdapter
    {
        private ArrayList<String> name;
        private ArrayList<String> chosenColor;
        private ArrayList<String> index;

        public PlayerListAdapter()
        {
            super();
            name = new ArrayList<String>();
            chosenColor = new ArrayList<String>();
            index = new ArrayList<String>();
            DiscoveryActivity.this.getLayoutInflater();
        }

        public void add(String name, String color, String index)
        {
            if (!this.index.contains(index))
            {
                this.name.add(name);
                this.chosenColor.add(color);
                this.index.add(index);
            }
        }

        public void remove(String index)
        {
            int i = 0;
            boolean found = false;
            for (i = 0; i < this.index.size(); i++)
            {
                if (this.index.get(i).equals(index))
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
                this.index.remove(i);
                this.name.remove(i);
                this.chosenColor.remove(i);
            }
        }

        public int getIndex(int position)
        {
            return position;
        }

        public void clear()
        {
            name.clear();
            chosenColor.clear();
            index.clear();
        }

        @Override public int getCount()
        {
            return name.size();
        }

        @Override public Object getItem(int i)
        {
            return name.get(i);
        }

        @Override public long getItemId(int i)
        {
            return i;
        }

        public View getView(int position, View view, ViewGroup parent)
        {
            LayoutInflater inflater = getLayoutInflater();
            PlayerViewHolder viewHolder;
            if (view == null)
            {
                view = inflater.inflate(R.layout.player_item, parent, false);
                viewHolder = new PlayerViewHolder();
                viewHolder.name = (TextView)view.findViewById(R.id.player_item_name);
                viewHolder.color = (ImageView)view.findViewById(R.id.player_color);

                view.setTag(viewHolder);
            }
            else
            {
                viewHolder = (PlayerViewHolder)view.getTag();
            }

            viewHolder.name.setText(name.get(position)); // no need to check for
                                                         // null because no
                                                         // null values in
                                                         // list
            try
            {
                Log.i(TAG, chosenColor.get(position));
                viewHolder.color.setBackgroundColor(Color.toColour(Integer.parseInt(chosenColor.get(position))).hex);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                index.remove(position);
                name.remove(position);
                chosenColor.remove(position);

            }
            return view;

        }

        @Override public String getKey(int position)
        {
            return index.get(position);
        }

        @Override public void modify(String newValue, int position)
        {
            name.set(position, newValue);
        }

        @Override public String nameAt(int position)
        {
            return name.get(position);
        }
    }

    static class PlayerViewHolder
    {
        TextView name;
        ImageView color;
    }

    // Adapter for holding other players in the game
    private abstract class MyAbstractAdapter extends BaseAdapter
    {

        public MyAbstractAdapter()
        {
            super();
        }

        public abstract void modify(String newValue, int position);

        public abstract String nameAt(int position);

        public abstract void add(String name, String value, String key);

        public abstract void remove(String s);

        public abstract int getIndex(int position);

        public abstract String getKey(int position);

        public abstract void clear();

        @Override public abstract int getCount();

        @Override public abstract Object getItem(int i);

        @Override public abstract long getItemId(int i);

        public abstract View getView(int position, View view, ViewGroup parent);
    }

}
