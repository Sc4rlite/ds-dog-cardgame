package ch.ethz.inf.vs.a4.signerc.dogclient.util;

import ch.ethz.inf.vs.a4.signerc.dogclient.R;


public class Util
{

    public Util()
    {}

    public static enum CardType
    {
        ONE(1, R.drawable.c11),
        TWO(2, R.drawable.c2),
        THREE(3, R.drawable.c3),
        FOUR(4, R.drawable.c4),
        FIVE(5, R.drawable.c5),
        SIX(6, R.drawable.c6),
        SEVEN(7, R.drawable.c7),
        EIGHT(8, R.drawable.c8),
        NINE(9, R.drawable.c9),
        TEN(10, R.drawable.c10),
        TWELVE(12, R.drawable.c12),
        THIRTEEN(13, R.drawable.c13),
        SWITCH(14, R.drawable.t),
        JOKER(15, R.drawable.j);

        public final int ID;
        public final int res;

        private CardType(int ID, int res)
        {
            this.ID = ID;
            this.res = res;
        }

        public int toInt()
        {
            return ID;
        }

    }

    public static CardType toCardType(int i)
    {
        switch (i)
        {
        case 1:
            return CardType.ONE;
        case 2:
            return CardType.TWO;
        case 3:
            return CardType.THREE;
        case 4:
            return CardType.FOUR;
        case 5:
            return CardType.FIVE;
        case 6:
            return CardType.SIX;
        case 7:
            return CardType.SEVEN;
        case 8:
            return CardType.EIGHT;
        case 9:
            return CardType.NINE;
        case 10:
            return CardType.TEN;
        case 15:
            return CardType.JOKER;
        case 12:
            return CardType.TWELVE;
        case 13:
            return CardType.THIRTEEN;
        case 14:
            return CardType.SWITCH;
        default:
            assert (false);
            return CardType.TWO;
        }
    }
}
