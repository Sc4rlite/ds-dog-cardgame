package ch.ethz.inf.vs.a4.signerc.dogclient;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;


public class StartActivity extends Activity
{

    private static final String TAG = "StartActivity";
    int currentColorId = -1;
    SharedPreferences preferences;
    SharedPreferences.Editor editPrefs;
    String username = "";
    private GridView colorLayout;

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        preferences = getSharedPreferences("playerPref", MODE_PRIVATE);
        username = preferences.getString("name", "");
        currentColorId = preferences.getInt("color", -1);

        setContentView(R.layout.activity_start);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // keep the screen awake

        colorLayout = (GridView)findViewById(R.id.color_chose_list);
        colorLayout.setAdapter(new ColorAdapter(this));
        colorLayout.setOnItemClickListener(colorChosen);

        if (!username.equals(""))
        {
            ((EditText)findViewById(R.id.user_name)).setText(username);
        }
    }

    @Override public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    OnItemClickListener colorChosen = new OnItemClickListener()
    {

        @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            Log.i(TAG, "chose " + position);
            currentColorId = position; // change the color of current
            colorLayout.invalidateViews();
        }
    };

    public void start(View v)
    {
        if (currentColorId == -1)
        {
            ((TextView)findViewById(R.id.chose_color)).setTextColor(android.graphics.Color.RED);
        }
        else if ((username = ((EditText)findViewById(R.id.user_name)).getText().toString()).equals(""))
        {
            ((TextView)findViewById(R.id.chose_name)).setTextColor(android.graphics.Color.RED);
        }
        else
        {
            editPrefs = preferences.edit();
            editPrefs.putString("name", username);
            editPrefs.putInt("color", currentColorId);
            editPrefs.commit();
            Intent intent = new Intent(v.getContext(), DiscoveryActivity.class);
            intent.putExtra("name", username).putExtra("color", Integer.toString(currentColorId));
            startActivity(intent);
        }
    }

}
