package ch.ethz.inf.vs.a4.signerc.dogclient.clientlogic;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;
import ch.ethz.inf.vs.a4.signerc.dogclient.MainActivity;
import ch.ethz.inf.vs.a4.signerc.dogclient.R;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionListener;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionManager;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionUtils;
import ch.ethz.inf.vs.a4.signerc.dogclient.util.Util.CardType;
import ch.ethz.inf.vs.a4.signerc.dogclient.util.Util;


public class Game implements ConnectionListener
{

    public Game(final MainActivity ma, JSONObject json, String name, ArrayList<CardType> blockedCards)
    {
        this.ma = ma;
        this.blockedCards = blockedCards;
        handID = new ArrayList<Integer>();
        String partnerName = "";
        int partnerColor = Color.BLUE;
        this.name = name;
        int color = Color.BLUE;
        this.manager = ConnectionUtils.manager;
        manager.registerListener(this);
        try
        {
            color = json.getInt("my_color");
            partnerColor = json.getInt("other_color");
            partnerName = json.getString("name");
        }
        catch (JSONException e)
        {
            Log.i("game", "game constructor", e);
            e.printStackTrace();
        }
        this.color = color;
        this.partnerName = partnerName;
        this.partnerColor = partnerColor;
        playForPartner = false;
    }

    /**
     * running instance of the main activity
     */
    final private MainActivity ma;

    /**
     * the player's color
     */
    final public int color;

    /**
     * own name
     */
    final private String name;

    /**
     * team member's color
     */
    final public int partnerColor;
    /**
     * team member's name
     */
    final public String partnerName;

    /**
     * set to true once all own figures are home but the team member's figures
     * aren't yet
     */
    private boolean playForPartner;

    public boolean getPlayForPartner()
    {
        return playForPartner;
    }

    /**
     * for sending messages to the server
     */
    private final ConnectionManager manager;

    /**
     * stores which card id's the player has for all card types
     */
    private final CustArrayList handType = new CustArrayList();

    private class CustArrayList extends ArrayList<CardType>
    {
        /**
         * 
         */
        private static final long serialVersionUID = -3174836350627839990L;

        public void add(CardType ct, Integer id)
        {
            // add at start?
            if (handType.isEmpty())
            {
                handType.add(ct);
                handID.add(id);
                return;
            }
            else
            {
                // add in between?
                for (int i = 0; i < handType.size(); i++)
                {
                    if (handType.get(i).toInt() >= (ct.ID))
                    {
                        handType.add(i, ct);
                        handID.add(i, id);
                        return;
                    }
                }
                // add at the end
                handType.add(ct);
                handID.add(id);
            }
        }
    }

    private final ArrayList<Integer> handID;

    public ArrayList<CardType> getHandCards()
    {
        return handType;
    }

    Handler mCallbackHandler = new Handler();

    /**
     * contains the cards which were already attempted to play this turn
     */
    private ArrayList<CardType> blockedCards;

    public ArrayList<CardType> getBlockedCards()
    {
        return blockedCards;
    }

    private boolean blockView = false;

    public boolean getBlockView()
    {
        return blockView;
    }

    private void turnChange()
    {
        if (yourTurn)
        {
            yourTurn = false;
            ma.yourTurn().setVisibility(View.GONE);
        }
        else
        {
            yourTurn = true;
            ma.yourTurn().setVisibility(View.VISIBLE);
            ma.blink();
        }
    }

    /**
     * receive information if hand has to be discarded due to no possible move
     * and if from now one the player moves his team mates figures
     * 
     * eventually call playTurn if the hand wasn't discarded
     */
    public void turn(JSONObject json)
    {
        blockedCards.clear();
        try
        {
            if (json.getBoolean("accept"))
            {
                ma.showToast(R.string.yourTurn);
                turnChange(); // my turn now
            }
            else
            {
                handID.clear();
                handType.clear();
                ma.showToast(R.string.noMove);
                blockView = false; // don't show any cards as blocked
                ma.updateCards();
            }
        }
        catch (JSONException e)
        {
            Log.i("game", "turn", e);
            e.printStackTrace();
        }

    }

    private boolean yourTurn = false;

    public boolean yourTurn()
    {
        return yourTurn;
    }

    public void receiveCards(JSONObject json)
    {
        Log.i("ClientGame", "received cards");
        blockedCards.clear();
        boolean swap = false; // did we receive the swap card this turn? Or rejoined and don't want to swap afterwards.
        try
        {
            JSONObject cards = json.getJSONObject("cards");
            swap = json.getBoolean("swap");
            for (Iterator<?> iter = cards.keys(); iter.hasNext();)
            {
                String idS = (String)iter.next();
                CardType ct = Util.toCardType(cards.getInt(idS));
                int id = Integer.valueOf(idS);
                handType.add(ct, id);
            }
        }
        catch (JSONException e)
        {
            Log.i("game", "receiveCards", e);
            e.printStackTrace();
        }
        if (swap)
        { // we need to play a swap card next
            ma.showToast(R.string.swapCard);
            switchCard = true;
            turnChange();
        }
        blockView = false;
        ma.updateCards();
        /*
         * ma.runOnUiThread(new Runnable() { public void run() {
         * ma.updateCards(); } });
         */
    }

    /**
     * last card to be played so that a move can be undone if playing it was
     * rejected
     */
    private int onHoldCard;
    private CardType onHoldType;
    private boolean switchCard = false;

    private boolean joker;

    public boolean joker()
    {
        return joker;
    }

    public void backFromJokerView()
    {
        joker = false;
        handType.add(onHoldType, onHoldCard);
        ma.updateCards();
    }

    private JSONObject jokerJSON;

    /**
     * play the card, respectively give a card to switch
     * 
     * @param id
     * @param card
     *        of type Card or rather some CardType still tbd
     * @param switchCard
     *        , true -> do a switchCard instead of a regular playCard
     * @return true if the send was completed, false if the card is declined as
     *         the player already attempted to play this CardType and there's no
     *         possible move
     */
    public boolean playCard(CardType card)
    {
        // create json Object to send
        if (!joker)
        {
            int handIndex = handType.indexOf(card);
            onHoldCard = handID.get(handIndex);
            onHoldType = card;
            JSONObject j = new JSONObject();
            try
            {
                if (switchCard)
                {
                    j.put("cmd", "switchCard");
                }
                else
                {
                    if (blockedCards.contains(card))
                    {
                        ma.showToast(R.string.unplayable);
                        return false; // this CardType can't be played as there's no possible move
                    }
                    j.put("cmd", "playCard");
                }
                handID.remove(handIndex);
                handType.remove(handIndex);
                j.put("id", onHoldCard);
                j.put("type", card.toInt());
                if (card == CardType.JOKER && !switchCard)
                {
                    joker = true;
                    ma.showToast(R.string.playJoker);
                    jokerJSON = j;
                }
                else
                {
                    blockedCards.add(card); // blocked for the next time
                    turnChange();
                    j.put("type", card.toInt());
                    manager.send(j);

                }
                if (switchCard)
                    switchCard = false;
            }
            catch (JSONException e)
            {
                Log.i("game", "playCard", e);
                e.printStackTrace();
            }
        }
        else
        {
            if (blockedCards.contains(card))
            {
                ma.showToast(R.string.unplayable);
                return false; // this CardType can't be played as there's no possible move
            }
            blockedCards.add(card);
            joker = false;
            turnChange();
            try
            {
                jokerJSON.put("type", card.toInt());
            }
            catch (JSONException e)
            {
                Log.i("game", "playCard2", e);
                e.printStackTrace();
            }
            manager.send(jokerJSON);
        }
        blockView = false;
        ma.updateCards();
        return true;

    }

    /**
     * adds the card back to the hand and calls main activity to display a
     * message
     */
    public void rejected()
    {
        turnChange();
        handType.add(onHoldType, onHoldCard);
        ma.showToast(R.string.unplayable);
        blockView = true;
        ma.updateCards();
    }

    @Override public Handler getHandler()
    {
        return this.mCallbackHandler;
    }

    private PopupWindow popup = null;

    // analog to dispatchEvent from chat server, in ChatEventSource
    @Override public void ReceiveEvent(final JSONObject json)
    {

        String cmd = ""; // prevent possible null pointer exception in the equals statement
        try
        {
            cmd = json.getString("cmd");
        }
        catch (JSONException e)
        {
            Log.i("game", "receiveEvent", e);
            e.printStackTrace();
        }
        if (cmd.equals("turn"))
        {
            getHandler().post(new Runnable()
            {
                public void run()
                {
                    turn(json);
                }

            });
        }
        else if (cmd.equals("distributeCards"))
        {
            getHandler().post(new Runnable()
            {
                public void run()
                {
                    receiveCards(json);
                }

            });
        }
        else if (cmd.equals("rejectCard"))
        {
            getHandler().post(new Runnable()
            {
                public void run()
                {
                    rejected();
                }

            });
        }
        else if (cmd.equals("finished"))
        {
            getHandler().post(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        JSONObject j = json.getJSONObject("players");
                        String p1 = j.getString("player1");
                        String p2 = j.getString("player2");
                        if ((p1.equals(name) || p2.equals(name)))
                            ma.showNotification("Congratulations!\nYou and your partner have won!", true);
                        else
                            ma.showNotification(p1 + " and " + p2 + " have won.\nMaybe next time ;)", true);
                    }
                    catch (JSONException e)
                    {
                        Log.i("game", "receiveEvent2", e);
                        e.printStackTrace();
                    }
                }
            });
        }
        else if (cmd.equals("reconnect"))
        {
            getHandler().post(new Runnable()
            {
                public void run()
                {
                    popup = ma.showNotification(
                            "This client lost the connection to the server.\n Attempting to reconnect...", false);
                }
            });
        }
        else if (cmd.equals("connected"))
        {
            getHandler().post(new Runnable()
            {
                public void run()
                {
                    ma.showToast("The connection to the server was reestablished.");
                    if (popup != null)
                    {
                        popup.dismiss();
                        popup = null;
                    }
                }
            });
        }
        else if (cmd.equals("exception"))
        {
            getHandler().post(new Runnable()
            {
                public void run()
                {
                    if (popup != null)
                    {
                        popup.dismiss();
                    }
                    popup = ma
                            .showNotification(
                                    "The connection to the server can't be reestablished.\n Please restart the game on your phone.",
                                    true);
                }
            });
        }
    }

}
