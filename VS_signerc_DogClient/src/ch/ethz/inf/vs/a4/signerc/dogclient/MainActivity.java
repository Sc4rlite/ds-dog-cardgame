package ch.ethz.inf.vs.a4.signerc.dogclient;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import ch.ethz.inf.vs.a4.signerc.dogclient.clientlogic.Game;
import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionUtils;
import ch.ethz.inf.vs.a4.signerc.dogclient.util.Util.CardType;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * for running the actual game
 * 
 * @author Chris
 *
 */
public class MainActivity extends Activity implements OnClickListener
{

    private TextView toast_text;
    private Toast toast;

    private Game game;
    public GridView gridview;
    public RelativeLayout relLayoutE;
    public RelativeLayout relLayoutU;
    private int width;
    private int height;
    private ImageButton[] even;
    private ImageButton[] uneven;

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        JSONObject json = null;
        try
        {
            json = new JSONObject(new JSONTokener(getIntent().getStringExtra("start")));
        }
        catch (JSONException e)
        {
            Log.i(getLocalClassName(), "onCreate JSON Problem", e);
            e.printStackTrace();
        }

        String username = getIntent().getStringExtra("username");
        game = new Game(this, json, username, blockedCards);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // keep the screen awake
        gridview = (GridView)findViewById(R.id.gridview);
        gridview.setAdapter(new JokerImageAdapter(this, game));

        gridview.setOnItemClickListener(new OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                if (game.yourTurn() && game.joker())
                {
                    game.playCard(CardType.values()[position]);
                }
            }
        });

        // toast stuff
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup)findViewById(R.id.toast_layout_root));

        toast_text = (TextView)layout.findViewById(R.id.text);

        toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        // Get screen size
        WindowManager windowManager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        windowManager.getDefaultDisplay().getSize(size);

        // Set width as longer edge
        if (size.x > size.y)
        {
            width = size.x;
            height = size.y;
        }
        else
        {
            width = size.y;
            height = size.x;
        }

        // create image buttons:

        // even card number
        relLayoutE = (RelativeLayout)findViewById(R.id.handRelLayoutE);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL, 1);
        lp.addRule(RelativeLayout.CENTER_VERTICAL, 1);
        even = new ImageButton[6];
        double horizTranslation = -width / 3;
        double verticTranslation = 0;
        float angle = -45; // rotation angle
        for (int i = 0; i < 6; i++)
        {
            even[i] = new ImageButton(this);
            even[i].setLayoutParams(lp);
            even[i].setOnClickListener(cardOnClickListener);
            even[i].setBackgroundColor(Color.TRANSPARENT);
            even[i].setRotation(angle);
            even[i].setMinimumHeight(height / 2);
            even[i].setMaxHeight(height / 2);
            even[i].setAdjustViewBounds(true);
            even[i].setTranslationX((int)horizTranslation);
            horizTranslation += (width / 3) / 2.5;
            switch (i)
            {
            case 0:
            case 5:
                verticTranslation = height / 6;
                break;
            case 1:
            case 4:
                verticTranslation = height / 6 - height / 6.5;
                break;
            case 2: // as 3 is the same as 2 we don't have to do anything after before setting Y translation in case 3
                verticTranslation = height / 6 - height / 6.5 - height / 11.5;
                break;
            default:
                break;
            }
            even[i].setTranslationY((int)verticTranslation);
            angle += 18; // 90/5
            relLayoutE.addView(even[i]);
        }
        relLayoutE.setVisibility(View.GONE);

        // allocate image buttons for uneven card number
        uneven = new ImageButton[5];
        relLayoutU = (RelativeLayout)findViewById(R.id.handRelLayoutU);
        angle = -45;
        horizTranslation = -width / 4;
        for (int i = 0; i < 5; i++)
        {
            uneven[i] = new ImageButton(this);
            uneven[i].setLayoutParams(lp);
            uneven[i].setOnClickListener(cardOnClickListener);
            uneven[i].setBackgroundColor(Color.TRANSPARENT);
            uneven[i].setRotation(angle);
            uneven[i].setMinimumHeight(height / 2);
            uneven[i].setMaxHeight(height / 2);
            uneven[i].setAdjustViewBounds(true);
            uneven[i].setTranslationX((int)horizTranslation);
            switch (i)
            {
            case 0:
            case 4:
                verticTranslation = height / 6;
                break;
            case 1:
            case 3:
                verticTranslation = height / 6 - height / 6.5;
                break;
            case 2: // as 3 is the same as 2 we don't have to do anything after before setting Y translation in case 3
                verticTranslation = height / 6 - height / 6.5 - height / 20;
                break;
            default:
                break;
            }
            uneven[i].setTranslationY((int)verticTranslation);
            horizTranslation += (width / 4) / 2;
            angle += 22.5; // 90/4
            relLayoutU.addView(uneven[i]);
        }
        relLayoutU.setVisibility(View.GONE);

        // blinking yourTurn text:
        yourTurn = (TextView)findViewById(R.id.yourTurn);
        yourTurn.setVisibility(View.GONE);

        JSONObject startJson = new JSONObject();
        try
        {
            startJson.put("cmd", "ready");
            ConnectionUtils.manager.send(startJson);
        }
        catch (JSONException e)
        {
            Log.i(getLocalClassName(), "onCreate ready JSON Problem", e);
            e.printStackTrace();
        }
    }

    @Override public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @Override public void onBackPressed()
    { // don't do anything if the back button is pressed except go back from joker gridview if in there
        if (game.joker())
            game.backFromJokerView();
    }

    @Override public void onResume()
    {
        super.onResume();
    }

    @Override public void onPause()
    {
        super.onPause();
    }

    private final ArrayList<CardType> blockedCards = new ArrayList<CardType>(13);

    private View.OnClickListener cardOnClickListener = new View.OnClickListener()
    {

        @Override public void onClick(View v)
        {
            if (game.yourTurn() && !game.joker())
            {
                game.playCard((CardType)v.getTag());
            }
        }
    };

    private void setImageButton(ImageButton ib, int no)
    {
        CardType ct = game.getHandCards().get(no);
        ib.setVisibility(View.VISIBLE);
        ib.setTag(ct);
        ib.setImageResource(ct.res);
        if (game.getBlockView() && blockedCards.contains(ct))
        {
            ib.setAlpha(0.5f);
        }
        else
            ib.setAlpha(1f);
    }

    /**
     * update the cards currently displayed
     */
    public void updateCards()
    {
        if (!game.joker())
        {
            gridview.setVisibility(View.GONE);
            int numHandCards = game.getHandCards().size();
            boolean evenH = (numHandCards & 1) == 0;

            // set the image buttons of the set we want to display initially to GONE
            if (evenH)
            { // even number of cards?
                relLayoutE.setVisibility(View.VISIBLE);
                relLayoutU.setVisibility(View.GONE);
                for (int i = 0; i < 6; i++)
                {
                    even[i].setVisibility(View.GONE);
                }
            }
            else
            { // uneven number of cards
                relLayoutE.setVisibility(View.GONE);
                relLayoutU.setVisibility(View.VISIBLE);
                for (int i = 0; i < 5; i++)
                {
                    uneven[i].setVisibility(View.GONE);
                }
            }

            // activate the ones we want
            switch (numHandCards)
            {
            case 1:
                setImageButton(uneven[2], 0);
                break;
            case 2:
                setImageButton(even[2], 0);
                setImageButton(even[3], 1);
                break;
            case 3:
                setImageButton(uneven[1], 0);
                setImageButton(uneven[2], 1);
                setImageButton(uneven[3], 2);
                break;
            case 4:
                setImageButton(even[1], 0);
                setImageButton(even[2], 1);
                setImageButton(even[3], 2);
                setImageButton(even[4], 3);
                break;
            case 5:
                setImageButton(uneven[0], 0);
                setImageButton(uneven[1], 1);
                setImageButton(uneven[2], 2);
                setImageButton(uneven[3], 3);
                setImageButton(uneven[4], 4);
                break;
            case 6:
                setImageButton(even[0], 0);
                setImageButton(even[1], 1);
                setImageButton(even[2], 2);
                setImageButton(even[3], 3);
                setImageButton(even[4], 4);
                setImageButton(even[5], 5);
                break;
            default:
                break;
            }
        }
        // display the joker's gridview
        else
        {
            gridview.setVisibility(View.VISIBLE);
            gridview.invalidateViews();
            relLayoutE.setVisibility(View.GONE);
            relLayoutU.setVisibility(View.GONE);
        }

    }

    public PopupWindow showNotification(String message, boolean restartAvailable)
    {

        // text for the popup
        TextView popupText = new TextView(this);
        popupText.setGravity(Gravity.CENTER_HORIZONTAL);
        popupText.setTextSize(20);
        popupText.setText(message);
        popupText.setPadding(0, 0, 0, 0);
        popupText.setTextColor(Color.WHITE);

        // layout of the popup
        LinearLayout popupLayout = new LinearLayout(this);
        popupLayout.setOrientation(1);
        popupLayout.addView(popupText);
        if (restartAvailable)
        {// buttons for the popup
            Button restart = new Button(this);
            restart.setTag("restartButton");
            restart.setTextColor(Color.WHITE);
            restart.setText("Restart");
            restart.setOnClickListener(this);
            popupLayout.addView(restart);
        }
        popupLayout.setBackgroundColor(Color.BLACK);
        popupLayout.setBackground(getResources().getDrawable(R.drawable.popupborder));

        PopupWindow popup = new PopupWindow(popupLayout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
        popup.showAtLocation(popupLayout, Gravity.CENTER, 0, 0);
        return popup;
    }

    public void showToast(int stringID)
    {
        toast_text.setText(getString(stringID));
        toast.show();
    }

    public void showToast(String s)
    {
        toast_text.setText(s);
        toast.show();
    }

    private TextView yourTurn;

    public TextView yourTurn()
    {
        return yourTurn;
    }

    public void blink()
    {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(400);
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(9);
        yourTurn.startAnimation(anim);
    }

    @Override public void onClick(View v)
    {

        if (v.getTag() instanceof String)
        {
            String tag = (String)v.getTag();
            if (tag.equals("restartButton"))
            {
                this.finish();
            }
        }

    }
}
