package ch.ethz.inf.vs.a4.signerc.dogclient;

import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionUtils.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


public class ColorAdapter extends BaseAdapter
{
    private StartActivity mContext;

    public ColorAdapter(StartActivity c)
    {
        mContext = c;
    }

    public int getCount()
    {
        return Color.values().length; //TODO
    }

    public Object getItem(int position)
    {
        return null;
    }

    public long getItemId(int position)
    {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ImageView imageView;
        if (convertView == null)
        {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setContentDescription("color");
            imageView.setTag(position);
        }
        else
        {
            imageView = (ImageView)convertView;
        }
        imageView.setBackgroundColor(Color.values()[position].hex); // that's somehow bugged if setting it in the convertView only
        if (mContext.currentColorId >= 0 && mContext.currentColorId == position)
        {
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.color));
        }
        else
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.color_chosen));

        return imageView;
    }
}
