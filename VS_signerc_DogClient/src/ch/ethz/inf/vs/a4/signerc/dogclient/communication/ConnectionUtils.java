package ch.ethz.inf.vs.a4.signerc.dogclient.communication;

import java.util.UUID;


public class ConnectionUtils
{

    public static enum State
    {
        STATE_DISCOVERING,
        STATE_CONNECTING,
        STATE_CONNECTED,
        STATE_DISCONNECTED
    }

    public static UUID uuid = UUID.fromString("4e5d48e0-75df-11e3-981f-0800200c9a66");
    public static UUID scan_uuid = UUID.fromString("dbfab162-68ee-11e4-b116-123b93f75cba");
    public static UUID reconnect_uuid = UUID.fromString("7414d85b-1fdb-4052-95c5-8f24a62a7c19");
    public static String SPLIT_PATTERN = "xxxxxxxxx";

    protected static final long KEEP_ALIVE = 20000;
    public static final int ACK_TIMEOUT = 3000;

    public static ConnectionManager manager;

    public static final String CMD = "cmd";
    public static final String CONNECTION = "connection";
    public static final String INTERN = "intern";
    public static final String EXTERN = "extern";
    public static final String GET_NAME = "getName";
    public static final String PLAYER = "player";
    public static final String VALUE = "name";
    public static final String GAME_SIZE = "size";
    public static final String LEAVE = "leave";
    public static final String ACTION = "action";
    public static final String EXCEPTION = "exception";
    public static final String ADD = "add";
    public static final String REMOVE = "remove";
    public static final String INDEX = "index";
    public static final Object START = "start";
    public static final String NAMES = "NAMES";
    public static final String SIZE = "size";
    public static final String HAS = "has";
    public static final String COLOR = "color";
    public static final String COLORS = "colors";
    public static final String MY_COLOR = "my_color";
    public static final String OTHER_COLOR = "other_color";
    public static final String MY_INDEX = "my_index";
    public static final String OTHER_INDEX = "other_index";
    public static final String CHOICE = "choice";
    public static final String ADDRESS = "address";

    public enum Color
    {
        BLUE(0, android.graphics.Color.BLUE, 0xff9999ff),
        RED(1, android.graphics.Color.RED, 0xffff9999),
        GREEN(2, 0xff009900, android.graphics.Color.GREEN),
        YELLOW(3, 0xffdddd00, 0xffffff99),
        CYAN(4, 0xff00dddd, 0xff99ffff),
        PURPLE(5, 0xffaa00ff, 0xffff99ff),
        ORANGE(6, 0xffeeaa00, 0xffffdd99),
        GREY(7, 0xff888888, 0xffcccccc);

        public final int ID;
        public final int hex;
        public final int hexLight;

        private Color(int ID, int hex, int hexLight)
        {
            this.ID = ID;
            this.hex = hex;
            this.hexLight = hexLight;
        }

        /**
         * converts player number i to the colour
         * 
         * @param i
         * @return
         */
        public static Color toColour(int i)
        {
            for (Color color: values())
                if (color.ID == i)
                    return color;
            assert (false);
            return Color.BLUE;
        }
    }

}
