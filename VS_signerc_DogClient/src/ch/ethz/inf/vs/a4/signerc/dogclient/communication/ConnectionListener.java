package ch.ethz.inf.vs.a4.signerc.dogclient.communication;

import org.json.JSONObject;

import android.os.Handler;


public abstract interface ConnectionListener
{

    public Handler mCallbackHandler = new Handler();

    public Handler getHandler();

    public void ReceiveEvent(JSONObject json);
}
