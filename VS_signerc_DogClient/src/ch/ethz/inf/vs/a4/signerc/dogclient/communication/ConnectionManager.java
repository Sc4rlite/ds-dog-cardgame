package ch.ethz.inf.vs.a4.signerc.dogclient.communication;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;

import ch.ethz.inf.vs.a4.signerc.dogclient.communication.ConnectionUtils.State;
import android.content.Context;
import android.util.Log;


public class ConnectionManager
{
    private State state;
    private Connection serverConnection;
    private List<ConnectionListener> listener;

    public ConnectionManager(Context context)
    {
        this.serverConnection = new BluetoothConnection(context, this);
        listener = new LinkedList<ConnectionListener>();
        state = State.STATE_DISCONNECTED;
    }

    public void findGames()
    {
        state = State.STATE_DISCOVERING;
        serverConnection.discover();
    }

    public void registerListener(ConnectionListener l)
    {
        listener.add(l);
    }

    public void removeListener(ConnectionListener l)
    {
        listener.remove(l);
    }

    public void send(JSONObject json)
    {
        serverConnection.send(json);
    }

    public void ConnectionEvent(int who, final JSONObject what)
    {
        Log.i("Connection", "event");
        for (final ConnectionListener l: listener)
        {
            new Thread(new Runnable()
            {

                @Override public void run()
                {
                    Log.i("Handler", "entered");
                    l.ReceiveEvent(what);
                }
            }).start();

        }
    }

    public void disconnect()
    {
        serverConnection.disconnect();
    }

    public State getState()
    {
        return state;
    }

    public void setState(State s)
    {
        state = s;
    }

    public boolean connectTo(String address)
    {
        boolean res = serverConnection.connectTo(address);
        state = State.STATE_CONNECTING;
        return res;
    }

}
