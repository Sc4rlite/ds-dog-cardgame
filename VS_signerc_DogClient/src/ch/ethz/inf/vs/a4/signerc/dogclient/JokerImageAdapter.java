package ch.ethz.inf.vs.a4.signerc.dogclient;

import ch.ethz.inf.vs.a4.signerc.dogclient.clientlogic.Game;
import ch.ethz.inf.vs.a4.signerc.dogclient.util.Util.CardType;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


public class JokerImageAdapter extends BaseAdapter
{
    private Context mContext;
    private Game game;

    public JokerImageAdapter(Context c, Game game)
    {
        mContext = c;
        this.game = game;
    }

    public int getCount()
    {
        if (game.joker())
            return 13; // we don't want the joker which is number 15
        else
            return 0;
    }

    public Object getItem(int position)
    {
        return null;
    }

    public long getItemId(int position)
    {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ImageView imageView;
        if (convertView == null)
        {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else
        {
            imageView = (ImageView)convertView;
        }
        imageView.setImageResource(CardType.values()[position].res);
        if (game.getBlockView() && game.getBlockedCards().contains(CardType.values()[position]))
        {
            imageView.setAlpha(0.5f);
        }
        else
        {
            imageView.setAlpha(1f);
        }

        return imageView;
    }
}
