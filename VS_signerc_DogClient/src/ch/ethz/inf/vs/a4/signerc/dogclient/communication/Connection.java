package ch.ethz.inf.vs.a4.signerc.dogclient.communication;

import org.json.JSONObject;

import ch.ethz.inf.vs.a4.signerc.dogclient.communication.BluetoothConnection.Game;


public abstract class Connection
{
    public Connection()
    {
        // do what connections usually do or whatever...
    }

    public abstract boolean discover();

    public abstract void send(JSONObject msg);

    protected Runnable receive;

    public abstract void broadcast(String msg);

    public abstract void disconnect();

    public abstract boolean connectTo(String address);

}
