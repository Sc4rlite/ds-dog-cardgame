package ch.ethz.inf.vs.a4.signerc.dogclient.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;


public class BluetoothConnection extends Connection
{

    protected static final String TAG = "BluetoothConnection";
    private BluetoothSocket socket;
    private OutputStream os;
    private InputStream is;
    private List<Game> games;
    private boolean running;
    BluetoothAdapter adapter;
    Context context;
    Thread receiveThread;
    Thread scanner;
    private ConnectionManager manager;
    private boolean doScan = true;
    AckManager ackMan;
    Object lock = new Object();

    public BluetoothConnection(Context c, ConnectionManager m)
    {
        manager = m;
        context = c;
        adapter = BluetoothAdapter.getDefaultAdapter();
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        c.registerReceiver(discoveryReceiver, filter);
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        c.registerReceiver(discoveryReceiver, filter);
        running = false;
        games = new LinkedList<Game>();
        ackMan = new AckManager();
        // scanner = new Thread(deviceScanner);
        // scanner.start();
    }

    @Override public void send(JSONObject msg)
    {
        sendRaw(ackMan.add(msg).toString().getBytes());
    }

    void sendRaw(byte[] msg)
    {
        try
        {
            Log.i(TAG, new String(msg));
            if (ackMan.connected)
            {
                synchronized (lock)
                {
                    os.write(msg);
                    os.write(ConnectionUtils.SPLIT_PATTERN.getBytes());
                    os.flush();
                }
            }
        }
        catch (Exception e)
        {
            ackMan.initReconnect();
            e.printStackTrace();
        }
    }

    Runnable receive = new Runnable()
    {
        public void run()
        {
            Log.i(TAG, "starting receive");
            int bufferSize = 1024;
            int bytesRead = -1;
            byte[] buffer = new byte[bufferSize];
            // Keep reading the messages while connection is open...
            while (!Thread.interrupted() && running)
            {
                Log.i(TAG, "running");
                try
                {
                    bytesRead = is.read(buffer);
                    if (!running)
                    {
                        return;
                    }

                    if (bytesRead != -1)
                    {
                        Log.i(TAG, "got something " + bytesRead + " bytes");
                        String result = new String(buffer, 0, bytesRead);
                        Log.i(TAG, result);
                        String[] res = result.split(ConnectionUtils.SPLIT_PATTERN);
                        for (String r: res)
                        {
                            while (r.contains(ConnectionUtils.SPLIT_PATTERN))
                            {
                                r = r.replace(ConnectionUtils.SPLIT_PATTERN, "");

                            }
                            Log.i(TAG, r + " before continue");
                            if (r.equals(""))
                            {
                                continue; // last one might be empty
                            }
                            Log.i(TAG, r + " after continue");
                            if (r.startsWith("ACK"))
                            {
                                ackMan.remove(r.substring(3));
                            }
                            else if (r.equals("live"))
                            {
                                ackMan.live();
                            }
                            else
                            {
                                Log.i(TAG, "received a message in receive");
                                JSONTokener tokener = new JSONTokener(r);
                                JSONObject myObj = new JSONObject(tokener);
                                if (ackMan.ack(myObj))
                                {
                                    // Pass message on UIThread
                                    manager.ConnectionEvent(-1, myObj);
                                    Log.i(TAG, "received " + new String(myObj.toString().getBytes()));
                                }
                            }
                        }
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    break;
                }
                catch (JSONException e)
                {
                    // ignore
                }

            }
        }
    };

    @Override public void broadcast(String msg)
    {

    }

    @Override public void disconnect()
    {
        if (running)
        {
            sendRaw(leave().toString().getBytes());
            try
            {
                Thread.sleep(500);
            }
            catch (Exception e)
            {}
        }// doScan = false;
        running = false;
        ackMan.notifyExit();
        try
        {
            if (receiveThread != null)
            {
                receiveThread.interrupt();
            }
            context.unregisterReceiver(discoveryReceiver);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (socket != null)
        {
            try
            {
                is.close();
                os.close();
                socket.close();
            }
            catch (Exception e)
            {}
        }
    }

    private JSONObject leave()
    {
        JSONObject result = new JSONObject();
        try
        {
            result.put(ConnectionUtils.CMD, ConnectionUtils.LEAVE);
            result.put(ConnectionUtils.CONNECTION, ConnectionUtils.INTERN);
        }
        catch (Exception e)
        {}
        return result;

    }

    // discover, connect

    @Override public boolean discover()
    {
        if (adapter != null && adapter.isDiscovering())
        {
            adapter.cancelDiscovery();
            return false;
        }
        for (BluetoothDevice d: adapter.getBondedDevices())
        {
            synchronized (games)
            {
                Game g = new Game(d);
                g.setName(d.getName());
                Log.i(TAG, new String(g.getJSON(true).toString().getBytes()));
                games.add(g);
                manager.ConnectionEvent(-1, g.getJSON(true));
                games.notify();
            }

            Log.i("paired to device", d.getName());
        }
        adapter.startDiscovery();
        return true;
    }

    /**
     * connect to the
     * 
     * @param i
     *        'th game listed on the phone screen (i.e. the ith device found)
     */

    boolean connFlag = false;
    String address = "";

    @Override public boolean connectTo(String address)
    {
        this.address = address;
        connFlag = true;
        Game g = gameAt(address);
        if (g == null)
            return false; // avoid nullpointer, wrong device
        doScan = false;
        adapter.cancelDiscovery();
        UUID uuid = ConnectionUtils.uuid;
        try
        {
            socket = g.getDevice().createRfcommSocketToServiceRecord(uuid);
            if (socket == null)
            {
                Log.d("bconn.147", "Socket null");
            }
            socket.connect();
            is = socket.getInputStream();
            os = socket.getOutputStream();
            running = true;
            receiveThread = new Thread(receive);
            receiveThread.start();
            return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            connFlag = false;
            return false;
        }
    }

    private Game gameAt(String address)
    {
        for (Game g: games)
        {
            if (g.getDevice().getAddress().equals(address))
            {
                return g;
            }
        }
        return null;
    }

    private BroadcastReceiver discoveryReceiver = new BroadcastReceiver()
    {

        @Override public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
            {
                Log.i(TAG, "Discovery ended");
                return;
            }
            BluetoothDevice current = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            synchronized (games)
            {

                String name = current.getName();
                Game g = new Game(current);
                g.setName(name);
                if (gameAt(current.getAddress()) != null)
                { // maybe already
                  // bonded
                    return;
                }
                games.add(g);
                manager.ConnectionEvent(-1, g.getJSON(true));
                Log.i(TAG, action);
                games.notify();
            }
        }
    };

    private Runnable deviceScanner = new Runnable()
    {
        // TODO maybe BLE?
        BluetoothSocket scanSocket;

        Runnable stopConnect = new Runnable()
        {

            public void run()
            {
                synchronized (scanSocket)
                {
                    try
                    {
                        Log.i(TAG, "stop connect");
                        scanSocket.wait(8000);
                        if (scanSocket != null && !scanSocket.isConnected())
                        {

                            scanSocket.close();
                            Log.i(TAG, "stopped connect");
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        };

        public void run()
        {
            synchronized (games)
            {
                try
                {
                    games.wait();
                }
                catch (InterruptedException e1)
                {
                    e1.printStackTrace();
                }
                while (doScan)
                {
                    try
                    {
                        for (Game g: games)
                        {
                            if (g.donotScan())
                                continue;
                            UUID uuid = ConnectionUtils.scan_uuid;
                            boolean received = false;
                            try
                            {
                                scanSocket = g.getDevice().createInsecureRfcommSocketToServiceRecord(uuid);
                                new Thread(stopConnect).start();
                                scanSocket.connect();
                                synchronized (scanSocket)
                                {
                                    scanSocket.notify();
                                }
                                try
                                {
                                    InputStream localIn = scanSocket.getInputStream();
                                    int bufferSize = 1024;
                                    int bytesRead = -1;
                                    byte[] buffer = new byte[bufferSize];
                                    bytesRead = localIn.read(buffer);
                                    if (bytesRead != -1)
                                    {
                                        Log.i(TAG, "got something " + bytesRead + " bytes");
                                        String result = new String(buffer, 0, bytesRead);
                                        Log.i(TAG, result);

                                        JSONTokener tokener = new JSONTokener(result);
                                        JSONObject myObj;
                                        myObj = new JSONObject(tokener);
                                        if (!g.fromJSON(myObj))
                                        {}
                                        else if (!connFlag)
                                        {
                                            manager.ConnectionEvent(-1, g.getJSON(true));
                                        }
                                        received = true;
                                        Log.i(TAG, "received " + new String(myObj.toString().getBytes()));

                                    }
                                    localIn.close();
                                    scanSocket.close();
                                }
                                catch (Exception e)
                                {
                                    Log.i(TAG, "couldn't read");
                                }

                            }
                            catch (IOException e)
                            {
                                Log.i(TAG, "couldn't connect");
                                e.printStackTrace();

                            }
                            if (!received)
                            {
                                g.plusOne();
                                if (g.getMissed() && !connFlag)
                                {
                                    manager.ConnectionEvent(-1, g.getJSON(false));
                                }
                            }
                        }
                        games.wait(5000); // repeat in 5 sec
                        Log.i(TAG, "interrupted");
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }

            }
        }
    };

    /**
     * 
     * @author Dominik Bruggisser
     * 
     *         Game is a little helper class for found bluetooth devices
     * 
     * 
     */
    public class Game implements Comparable<Game>
    {
        BluetoothDevice d;
        String name;
        int registeredPlayer = 0;
        int totalPlayer = 0;
        int misses;

        public Game(BluetoothDevice d)
        {
            this.d = d;
        }

        public JSONObject getJSON(boolean add)
        {
            JSONObject json = new JSONObject();
            try
            {
                if (!add && totalPlayer <= 0)
                { // only remove if no information
                  // available before (otherwise
                  // is server)
                    totalPlayer = -1;
                }
                json.put("cmd", "getName");
                json.put("name", name);
                if (totalPlayer != 0)
                {
                    json.put("size", totalPlayer);
                }
                json.put("has", registeredPlayer);
                json.put("address", d.getAddress());
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return json;
        }

        /**
         * get the values from the jsonobject
         * 
         * @param myObj
         * @return true if space left, false if full
         */
        public boolean fromJSON(JSONObject myObj)
        {
            try
            {
                int size = myObj.getInt("size");
                int has = myObj.getInt("has");
                if (size > has)
                {
                    this.totalPlayer = size;
                    this.registeredPlayer = has;
                    this.name = myObj.getString("name");
                    misses = 0; // reset missrate
                    return true;
                }
            }
            catch (Exception e)
            {

            }
            return false;
        }

        public String getName()
        {
            return name;
        }

        public void plusOne()
        {
            misses++;
        }

        public boolean donotScan()
        {
            if (misses > 10)
            {
                Log.i(TAG, name + " is no game");
                return true;
            }
            return false;
        }

        public boolean getMissed()
        {
            if (misses > 3)
            {
                return true;
            }
            return false;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public int getRegisteredPlayer()
        {
            return registeredPlayer;
        }

        public void setRegisteredPlayer(int registeredPlayer)
        {
            this.registeredPlayer = registeredPlayer;
        }

        public int getTotalPlayer()
        {
            return totalPlayer;
        }

        public void setTotalPlayer(int totalPlayer)
        {
            this.totalPlayer = totalPlayer;
        }

        public BluetoothDevice getDevice()
        {
            return d;
        }

        @Override public int compareTo(Game another)
        {
            // comparing the addresses is enough to decide whether the games are
            // equal
            return d.getAddress().compareTo(another.getDevice().getAddress());
        }

    }

    boolean initiatedReconnect = false;
    boolean blockedReconnect = false;

    boolean reconnect()
    {
        // the connection was lost; try to reestablish the connection
        if (initiatedReconnect)
            return false;
        initiatedReconnect = true;
        try
        {
            socket.close();
            JSONObject notification = new JSONObject();
            notification.put("cmd", "reconnect");
            manager.ConnectionEvent(-1, notification);
            Log.i(TAG, " closed socket? " + !socket.isConnected());

        }
        catch (Exception e)
        { /* probably will throw an exception! */
        }
        for (int i = 0; i < 3; i++)
        {
            try
            {
                UUID uuid = ConnectionUtils.uuid;
                Game g = gameAt(address);
                Log.i(TAG, "try to reconnect to " + g.d.getName());
                socket = g.getDevice().createRfcommSocketToServiceRecord(uuid);
                socket.connect();
                if (blockedReconnect)
                    return false;
                initiatedReconnect = false;
                Log.i(TAG, "connect successfull? " + socket.isConnected());
                is.close();
                os.close();
                is = socket.getInputStream();
                os = socket.getOutputStream();

                JSONObject notification2 = new JSONObject();
                notification2.put("cmd", "connected");
                manager.ConnectionEvent(-1, notification2);
                return true;
            }
            catch (Exception e)
            {
                // wait KEEP_ALIVE ms which is the longest possible period for
                // the server to realise connection is lost
                try
                {
                    Thread.sleep(ConnectionUtils.KEEP_ALIVE + new Random().nextInt(1000));
                }
                catch (Exception e2)
                {}
            }
        }
        try
        {
            // Client lost
            if (blockedReconnect)
                return false;
            JSONObject exception = new JSONObject();
            exception.put(ConnectionUtils.CMD, ConnectionUtils.EXCEPTION);
            manager.ConnectionEvent(-1, exception);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @SuppressLint("UseSparseArrays")
    class AckManager
    {

        HashMap<Integer, JSONObject> messages;
        HashMap<Integer, Timeout> timeout;
        HashMap<Integer, JSONObject> acked;
        long lastLive;
        int counter;
        Thread worker;
        boolean connected;

        public AckManager()
        {
            messages = new HashMap<Integer, JSONObject>();
            timeout = new HashMap<Integer, Timeout>();
            acked = new HashMap<Integer, JSONObject>();
            counter = 0;
            connected = true;
            worker = new Thread(ackHandler);
            worker.start();
        }

        public void live()
        {
            lastLive = System.currentTimeMillis();
        }

        public JSONObject add(JSONObject message)
        {
            try
            {
                int id = uniqueInt();
                message.put("uniqueInt", id);
                synchronized (messages)
                {
                    messages.put(id, message);
                    timeout.put(id, new Timeout(System.currentTimeMillis(), 0));
                    messages.notify();
                }
                return message;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public void remove(String rawID)
        {
            int id = Integer.parseInt(rawID);
            synchronized (messages)
            {
                messages.remove(id);
                timeout.remove(id);
                messages.notify();
            }
        }

        /**
         * ack the received message
         * 
         * @param message
         *        incoming JSON Object
         * @return returns true if the message was not acked before, false
         *         otherwise
         */
        public boolean ack(JSONObject message)
        {
            try
            {
                int key = message.getInt("uniqueInt");
                sendRaw(("ACK" + key).getBytes());
                if (acked.put(key, message) != null)
                {
                    // Already acked
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                return false;

            }
        }

        Runnable ackHandler = new Runnable()
        {
            public void run()
            {
                synchronized (messages)
                {
                    try
                    {
                        messages.wait();
                    }
                    catch (InterruptedException e1)
                    {}
                    lastLive = System.currentTimeMillis();
                    while (running)
                    {
                        long sleep = ConnectionUtils.KEEP_ALIVE;
                        long now = System.currentTimeMillis();

                        if (Math.abs(now - lastLive) > 3 * ConnectionUtils.KEEP_ALIVE)
                        {
                            initReconnect();
                        }
                        Set<Integer> keys = timeout.keySet();
                        for (int key: keys)
                        {
                            long waiting = (timeout.get(key).timeout + ConnectionUtils.ACK_TIMEOUT) - now;
                            if (waiting < 100)
                            {
                                resend(key);
                            }
                            else
                            {
                                sleep = Math.min(sleep, waiting);
                            }
                        }
                        if (connected && messages.isEmpty())
                        {
                            sendRaw(("live" + ConnectionUtils.SPLIT_PATTERN).getBytes());
                        }
                        try
                        {
                            messages.wait(sleep);
                        }
                        catch (InterruptedException e)
                        {}
                    }
                }
            }
        };

        private void notifyExit()
        {
            synchronized (messages)
            {
                messages.notify();
            }
        }

        void resend(int key)
        {
            // keep old one in the List
            if (connected)
            {
                Log.i(TAG, "resending " + messages.get(key).toString());
                sendRaw(messages.get(key).toString().getBytes());
            }
            int age = timeout.get(key).age;
            if (age < 3)
            {
                timeout.put(key, new Timeout(System.currentTimeMillis(), age + 1));
            }
            else
            {
                initReconnect();
            }
        }

        Runnable controlReconnect = new Runnable()
        {
            public void run()
            {
                try
                {
                    Thread.sleep(90000 + ConnectionUtils.KEEP_ALIVE);	// as long as the server tries to reconnect, i.e. 1.5s, + time to realize that connection is lost
                    if (!connected)
                    {
                        blockedReconnect = true;
                        socket.close();
                        JSONObject exception = new JSONObject();
                        exception.put(ConnectionUtils.CMD, ConnectionUtils.EXCEPTION);
                        manager.ConnectionEvent(-1, exception);
                    }
                }
                catch (Exception e)
                {
                    //
                }
            }
        };

        void initReconnect()
        {
            connected = false;
            new Thread(controlReconnect).start();
            if (reconnect())
            {
                receiveThread = new Thread(receive);
                receiveThread.start();
                connected = true;
            }
            // otherwise an exception is thrown in "reconnect()"
        }

        int uniqueInt()
        {
            counter++;
            return counter;
        }

        class Timeout
        {
            public int age;
            public long timeout;

            public Timeout(long timeout, int age)
            {
                this.timeout = timeout;
                this.age = age;
            }
        }
    }
}
