package ch.ethz.inf.vs.a4.signerc.dogserver.util;

import java.util.LinkedList;
import java.util.List;


/**
 * 
 * @author Paige and Sphinx
 *
 * @param <L>
 *        Type of left argument
 * @param <R>
 *        Type of right argument
 */
public class Tuple<L, R>
{
    /**
     * Left argument of Tuple
     */
    public L left;

    /**
     * Right argument of Tuple
     */
    public R right;

    /**
     * Constructor
     * 
     * @param l
     *        Value of left argument
     * @param r
     *        Value of right argument
     */
    public Tuple(L l, R r)
    {
        left = l;
        right = r;
    }

    /**
     * Unzips a List of Tuples and returns the List of left arguments
     * 
     * @param list
     *        List of Tuples
     * @return List of left arguments
     */
    public List<L> returnListLeft(List<Tuple<L, R>> list)
    {
        List<L> res = new LinkedList<L>();
        for (Tuple<L, R> tuple: list)
            res.add(tuple.left);
        return res;
    }

    /**
     * Unzips a List of Tuples and returns the List of right arguments
     * 
     * @param list
     *        List of Tuples
     * @return List of right arguments
     */
    public List<R> returnListRight(List<Tuple<L, R>> list)
    {
        List<R> res = new LinkedList<R>();
        for (Tuple<L, R> tuple: list)
            res.add(tuple.right);
        return res;
    }

}
