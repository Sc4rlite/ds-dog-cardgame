package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import java.util.LinkedList;
import java.util.List;

import ch.ethz.inf.vs.a4.signerc.dogserver.util.Tuple;


/**
 * Class that represents a Field on the Board
 * 
 * @author Paige and Sphinx
 *
 */
public abstract class Field
{
    /**
     * Indicates on which edge of the board the field is located
     */
    public final int playerID;

    /**
     * Together with playerID, this number uniquely defines a field
     */
    public final int fieldID;

    /**
     * Figure that occupies this field. Null if field is free
     */
    protected Figure occupant;

    protected Board board;

    public Field(int playerID, int fieldID, Board board)
    {
        this.playerID = playerID;
        this.fieldID = fieldID;
        this.board = board;
    }

    public void setOccupant(Figure figure)
    {
        //        System.out.println("New occupant set");
        occupant = figure;
    }

    /**
     * Checks if figure can be moved the given number of steps
     * 
     * @param steps
     *        number of steps that figure will move
     * @param isSeven
     *        Determines if card is SevenCard and sends all Figures on the way
     *        home
     * @return Tuple of Field that is reached and List of Tuples of Figures that
     *         are sent home, null if invalid move
     */
    public abstract List<Tuple<Field, List<Figure>>> moveSteps(int steps, boolean isSeven);

    /**
     * Checks if figure can be moved the given number of steps after the first
     * step
     * 
     * @param steps
     *        number of steps that figure will move
     * @param isSeven
     *        Determines if card is SevenCard and sends all Figures on the way
     *        home
     * @param player
     *        Player that owns the moving Figure
     * @return
     */
    protected abstract List<Tuple<Field, List<Figure>>> moveForward(int steps, boolean isSeven, Player player);

    /**
     * t Sets the given player as occupant of this Field
     * 
     * @param player
     *        Player that occupies field from now on
     */
    public void moveTo(Figure figure)
    {
        occupant = figure;
    }

    /**
     * Gets Figure that occupies Field or null if Field is free
     * 
     * @return Figure occupant
     */
    public Figure isOccupiedBy()
    {
        return occupant;
    }

    //_________________________________________________________________________

    /**
     * Class that represents a PublicField on the Board where the Figures can
     * move forward or backwards
     * 
     * @author Paige and Sphinx
     *
     */
    public static class PublicField extends Field
    {
        /**
         * Points to the next Field on Board
         */
        public Field next;

        /**
         * Points to the previous Field on Board
         */
        private Field previous;

        /**
         * Constructor
         */
        public PublicField(int playerID, int fieldID, Board board)
        {
            super(playerID, fieldID, board);
            //TODO intialize fields if needed
        }

        /**
         * sets the next and previous Field on the Board
         * 
         * @param nextField
         *        next Field on Board
         * @param prevField
         *        previous Field on Board
         */
        public void setAdjacentFields(Field nextField, Field prevField)
        {
            next = nextField;
            previous = prevField;
        }

        /**
         * Setter for Field next
         * 
         * @param nextField
         *        Next Field on Board
         */
        public void setNext(Field nextField)
        {
            next = nextField;
        }

        /**
         * Setter for Field previous
         * 
         * @param prevField
         *        Previous Field on Board
         */
        public void setPrevious(Field prevField)
        {
            previous = prevField;
        }

        @Override public List<Tuple<Field, List<Figure>>> moveSteps(int steps, boolean isSeven)
        {
            if (steps > 0)
            {
                return next.moveForward(steps - 1, isSeven, this.occupant.owner);
            }
            else if (steps == 0)
            {
                // move is invalid
                return new LinkedList<Tuple<Field, List<Figure>>>();
            }
            else
            {
                //if (steps < 0)
                // move is started with -4
                return previous.moveForward(steps + 1, isSeven, this.occupant.owner);
            }
            // TODO DONE Implement moveSteps of PublicField
        }

        @Override protected List<Tuple<Field, List<Figure>>> moveForward(int steps, boolean isSeven, Player player)
        {
            LinkedList<Figure> f = new LinkedList<Figure>();
            if (steps > 0)
            {
                if (isSeven && occupant != null)
                {
                    // figure on this field is burned, added to list. All other 3 possible cases of isSeven and occupant do nothing.

                    List<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                    List<Tuple<Field, List<Figure>>> recursive = next.moveForward(steps - 1, isSeven, player);
                    List<Figure> burned;
                    for (Tuple<Field, List<Figure>> tuple: recursive)
                    {
                        burned = new LinkedList<Figure>(tuple.right);
                        burned.add(occupant);
                        re.add(new Tuple<Field, List<Figure>>(tuple.left, burned));
                    }
                    return re;
                }
                else
                {
                    return next.moveForward(steps - 1, isSeven, player);
                }
            }
            else if (steps == 0)
            {
                // ran out of steps. add occupant to burned list, if existent
                if (occupant != null)
                {
                    f.add(occupant);
                }
                List<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                re.add(new Tuple<Field, List<Figure>>(this, f));
                return re;

            }
            else
            {
                //if (steps < 0)
                // move is started with -4, isSeven is false -> no burning
                return previous.moveForward(steps + 1, isSeven, player);
            }
            // TODO DONE Implement moveForward of PublicField
        }

    }

    /**
     * Class that represents Field on Board that is owned by a Player and has
     * that Player's colour
     * 
     * @author Paige and Sphinx
     *
     */
    public abstract static class OwnedField extends Field
    {
        /**
         * Player that owns this Field
         */
        protected Player owner;

        /**
         * Constructor with given owner
         * 
         * @param owningPlayer
         *        Player that owns this Field
         */
        public OwnedField(Player owningPlayer, int fieldID, Board board)
        {
            super(owningPlayer.ID, fieldID, board);
            owner = owningPlayer;
        }

        /**
         * Getter method for the owner
         * 
         * @return Owner of this Field
         */
        public Player getOwner()
        {
            return owner;
        }
    }

    /**
     * Class that represents a StartField on the Board where the Figures other
     * Players can move forward or backwards. The Player that owns this Field
     * enters the race through this Field and blocks the passage for all other
     * Players when one of its Figures occupies this Field
     * 
     * @author Paige and Sphinx
     *
     */
    public static class StartField extends OwnedField
    {
        /**
         * Points to the next Field on Board
         */
        public Field next;

        /**
         * Points to the previous Field on Board
         */
        private Field previous;

        /**
         * Points to the first ParkingLotField of owner
         */
        private ParkingLotField parkingLot;

        /**
         * Constructor with given owner
         * 
         * @param owningPlayer
         *        Player that owns this Field
         */
        public StartField(Player owningPlayer, int fieldID, Board board)
        {
            super(owningPlayer, fieldID, board);
            //TODO intialize fields if needed
        }

        /**
         * Setter for all Field pointer
         * 
         * @param nextField
         *        Next Field on Board
         * @param prevField
         *        Previous Field on Board
         * @param parkingField
         *        First ParkingLotField of owner
         */
        public void setAdjacentFields(Field nextField, Field prevField, ParkingLotField parkingField)
        {
            next = nextField;
            previous = prevField;
            parkingLot = parkingField;
        }

        /**
         * Setter for Field next
         * 
         * @param nextField
         *        Next Field on Board
         */
        public void setNext(Field nextField)
        {
            next = nextField;
        }

        /**
         * Setter for Field previous
         * 
         * @param prevField
         *        Previous Field on Board
         */
        public void setPrevious(Field prevField)
        {
            previous = prevField;
        }

        /**
         * Setter for ParkingLotField parkingLot
         * 
         * @param parkingField
         *        First ParkingLotField of owner
         */
        public void setParkingLot(ParkingLotField parkingField)
        {
            parkingLot = parkingField;
        }

        @Override public List<Tuple<Field, List<Figure>>> moveSteps(int steps, boolean isSeven)
        {
            if (steps > 0)
            {
                return next.moveForward(steps - 1, isSeven, this.occupant.owner);
            }
            else if (steps == 0)
            {
                // move is invalid, no move should start with zero steps
                return new LinkedList<Tuple<Field, List<Figure>>>();
            }
            else
            {
                //if (steps < 0)
                // move is started with -4
                return previous.moveForward(steps + 1, isSeven, this.occupant.owner);
            }
            // TODO DONE Implement moveSteps of StartField
        }

        @Override protected List<Tuple<Field, List<Figure>>> moveForward(int steps, boolean isSeven, Player player)
        {
            LinkedList<Figure> f = new LinkedList<Figure>();
            if (owner.equals(player) && occupant == null)
            {
                // own home free, enter parking lot
                if (steps > 4)
                {
                    //you certainly overshot your parking lot, walk another round
                    return next.moveForward(steps - 1, isSeven, player);
                }
                if (steps > 0 && steps <= 4)
                {
                    //enter parking lot
                    LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                    List<Tuple<Field, List<Figure>>> parking = parkingLot.moveForward(steps - 1, isSeven, player);
                    List<Tuple<Field, List<Figure>>> nextF = next.moveForward(steps - 1, isSeven, player);

                    if (parking != null)
                        // if you cannot reach a free spot with your remaining number of steps, go another round.
                        for (Tuple<Field, List<Figure>> tuple: parking)
                            re.add(tuple);
                    if (nextF != null)
                        // if you cannot reach a free spot with your remaining number of steps, go another round.
                        for (Tuple<Field, List<Figure>> tuple: nextF)
                            re.add(tuple);
                    return re;
                }
                else if (steps == 0)
                {
                    // stop on own homefield.
                    LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                    re.add(new Tuple<Field, List<Figure>>(this, new LinkedList<Figure>()));
                    return re;
                }
                else
                {
                    //if (steps < 0)
                    return previous.moveForward(steps + 1, isSeven, player);
                    //return parkingLot.moveForward(-(steps + 1), isSeven,player);
                    //TODO can you enter parking lot walking backwards? NO
                }

            }
            else if (owner.equals(player) && occupant != null)
            {
                //own home, but someone is standing on it
                if (occupant.owner.equals(player) && (!isSeven || board.isVirtual))
                {
                    // you are blocked. you cannot step on this field, invalid move
                    return new LinkedList<Tuple<Field, List<Figure>>>();
                }
                else
                {
                    // burn the Figure standing on your homefield
                    if (steps > 4)
                    {
                        //you overshot your parking lot, walk another round  
                        if (isSeven)
                        {
                            f.add(occupant);
                        }
                        List<Tuple<Field, List<Figure>>> re = next.moveForward(steps - 1, isSeven, player);
                        for (Tuple<Field, List<Figure>> tuple: re)
                            tuple.right.addAll(f);
                        return re;
                    }
                    else if (steps > 0 && steps <= 4)
                    {
                        //enter parking lot
                        if (isSeven)
                        {
                            f.add(occupant);
                        }
                        List<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                        List<Tuple<Field, List<Figure>>> parking = parkingLot.moveForward(steps - 1, isSeven, player);
                        List<Tuple<Field, List<Figure>>> nextF = next.moveForward(steps - 1, isSeven, player);

                        if (parking != null)
                            // if you cannot reach a free spot with your remaining number of steps, go another round.
                            for (Tuple<Field, List<Figure>> tuple: parking)
                            {
                                tuple.right.addAll(f);
                                re.add(tuple);
                            }
                        if (nextF != null)
                            // if you cannot reach a free spot with your remaining number of steps, go another round.
                            for (Tuple<Field, List<Figure>> tuple: nextF)
                            {
                                tuple.right.addAll(f);
                                re.add(tuple);
                            }
                        return re;
                    }
                    else if (steps == 0)
                    {
                        // stop on own homefield.
                        f.add(occupant);
                        LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                        re.add(new Tuple<Field, List<Figure>>(this, f));
                        return re;
                    }
                    else
                    {
                        //if (steps < 0)
                        return previous.moveForward(steps + 1, isSeven, player);
                        //return parkingLot.moveForward(-(steps + 1), isSeven,player);
                        //TODO can you enter parking lot walking backwards? NO
                    }
                }

            }
            else if (!owner.equals(player) && occupant == null)
            {
                // foreign home, but free, treat like Public field
                if (steps > 0)
                {
                    return next.moveForward(steps - 1, isSeven, player);
                }
                else if (steps == 0)
                {
                    // ran out of steps. 
                    LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                    re.add(new Tuple<Field, List<Figure>>(this, f));
                    return re;
                }
                else
                {
                    //if (steps < 0)
                    // move is started with -4
                    return previous.moveForward(steps + 1, isSeven, player);
                }

            }
            else
            {
                //if (owner != player && occupant != null)
                //foreign home, but someone standing on it check if path blocked (Figure is in samecolored field)
                if (owner.equals(occupant.owner))
                {
                    // Your path is blocked; YOU SHALL NOT PASS!
                    return new LinkedList<Tuple<Field, List<Figure>>>();
                }
                else
                {
                    //colors do not match burn occupant
                    if (steps > 0)
                    {
                        if (isSeven)
                        {
                            // figure on this field is burned, and added to list.
                            List<Tuple<Field, List<Figure>>> re = next.moveForward(steps - 1, isSeven, player);
                            for (Tuple<Field, List<Figure>> tuple: re)
                                tuple.right.add(occupant);
                            return re;
                        }
                        else
                        {
                            return next.moveForward(steps - 1, isSeven, player);
                        }
                    }
                    else if (steps == 0)
                    {
                        // ran out of steps. add occupant to burned list
                        f.add(occupant);
                        LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                        re.add(new Tuple<Field, List<Figure>>(this, f));
                        return re;

                    }
                    else
                    {
                        //if (steps < 0)
                        // move is started with -4, isSeven is false -> no burning
                        return previous.moveForward(steps + 1, isSeven, player);
                    }
                }
            }

            // TODO DONE Implement moveForward of StartField
        }
    }

    /**
     * Class that represents a HomeField on the Board where the Figures of the
     * Player that owns this Field are stationed when the were sent home or have
     * not been touched yet
     * 
     * @author Paige and Sphinx
     *
     */
    public static class HomeField extends OwnedField
    {
        /**
         * Pointer to StartField belonging to the owner
         */
        private StartField startField;

        /**
         * Figures of owner that are currently home
         */
        private List<Figure> residents;

        /**
         * Constructor with given owner
         * 
         * @param owningPlayer
         *        Player that owns this Field
         */
        public HomeField(Player owningPlayer, int fieldID, Board board)
        {
            super(owningPlayer, fieldID, board);
            residents = new LinkedList<Figure>();
            //TODO intialize fields if needed
        }

        /**
         * Setter for StartField that this HomeField belongs to
         * 
         * @param start
         *        StartField of the owner
         */
        public void setStartField(StartField start)
        {
            startField = start;
        }

        /**
         * Adds Figure to this HomeField's residents. Assures that Figure is
         * owned by owner.
         * 
         * @param figure
         *        Figure that comes home
         */
        public void comeHome(Figure figure)
        {
            assert (figure.owner.equals(owner));
            residents.add(figure);
        }

        /**
         * Figure leaves this HomeField
         * 
         * @param figure
         *        Figure that leaves home
         */
        public void leaveHome(Figure figure)
        {
            assert (residents.contains(figure));
            residents.remove(figure);
        }

        @Override public List<Tuple<Field, List<Figure>>> moveSteps(int steps, boolean isSeven)
        {
            if (steps == 1 && !isSeven || steps == 13)
            {
                // move out card was plaved. Check if startField is free
                if (startField.isOccupiedBy() == null)
                {
                    LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                    re.add(new Tuple<Field, List<Figure>>(startField, null));
                    return re;
                }
                else
                {
                    LinkedList<Figure> f = new LinkedList<Figure>();
                    f.add(startField.isOccupiedBy());
                    LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                    re.add(new Tuple<Field, List<Figure>>(startField, f));
                    return re;
                    //TODO check burnablility of foreign Figures standing on startfield. BURNABLE
                }

            }
            else
            {
                // invalid move for a home field. If steps != 1 no move out card
                // was played. If steps == 1 and isSeven is true, a Seven is
                // pretending to be a move out card. Figure cannot be moved out.
                return new LinkedList<Tuple<Field, List<Figure>>>();
            }
            // TODO DONE Implement moveSteps of HomeField
        }

        @Override protected List<Tuple<Field, List<Figure>>> moveForward(int steps, boolean isSeven, Player player)
        {
            // TODO DONE Implement moveForward of HomeField
            // you should not be able to call moveForward on any Homefield, if
            // you do, your Board is probably not working correctly.
            return new LinkedList<Tuple<Field, List<Figure>>>();
        }
    }

    /**
     * Class that represents a ParkingLotField on the Board where the Figures of
     * the Player that owns this Field can move forward or are parked when
     * inactive.
     * 
     * @author Paige and Sphinx
     *
     */
    public static class ParkingLotField extends OwnedField
    {
        /**
         * Represents the next parking lot after this Field
         */
        private ParkingLotField nextLot;

        /**
         * Constructor with given owner
         * 
         * @param owningPlayer
         *        Player that owns this Field
         */
        public ParkingLotField(Player owningPlayer, int fieldID, Board board)
        {
            super(owningPlayer, fieldID, board);
            //TODO intialize fields if needed
        }

        /**
         * Setter for next ParkingLotField
         * 
         * @param next
         *        Next ParkingLotField
         */
        public void setNext(ParkingLotField next)
        {
            nextLot = next;
        }

        /**
         * Determines if ParkingLotField is inactive
         * 
         * @return
         */
        public boolean isInactiveParkingLotField()
        {
            return occupant != null
                    && (nextLot == null || (nextLot.occupant != null && nextLot.isInactiveParkingLotField()));
        }

        @Override public List<Tuple<Field, List<Figure>>> moveSteps(int steps, boolean isSeven)
        {
            if (nextLot == null)
            {
                // end of the parking lot, you cannot move any further than this Field.
                if (steps == 0)
                {
                    LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                    re.add(new Tuple<Field, List<Figure>>(this, new LinkedList<Figure>()));
                    return re;
                }
                else
                {
                    return new LinkedList<Tuple<Field, List<Figure>>>();
                }
            }
            else
            {
                if (steps > 0)
                {
                    return nextLot.moveForward(steps - 1, isSeven, owner);
                }
                else if (steps == 0)
                {
                    // stop on this Field.
                    LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                    re.add(new Tuple<Field, List<Figure>>(this, new LinkedList<Figure>()));
                    return re;
                }
                else
                {
                    // no moving backwards.
                    return new LinkedList<Tuple<Field, List<Figure>>>();
                }
            }
            // TODO DONE Implement moveSteps of ParkingLotField

        }

        @Override protected List<Tuple<Field, List<Figure>>> moveForward(int steps, boolean isSeven, Player player)
        {

            if (occupant != null && (!isSeven || board.isVirtual))
            {
                // someone is already on this Field. You cannot pass, invalid move.
                return new LinkedList<Tuple<Field, List<Figure>>>();
            }
            else
            {
                // Field is free
                if (nextLot != null)
                {
                    if (steps > 0)
                    {
                        return nextLot.moveForward(steps - 1, isSeven, owner);
                    }
                    else if (steps == 0)
                    {
                        // stop on this Field.
                        LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                        re.add(new Tuple<Field, List<Figure>>(this, new LinkedList<Figure>()));
                        return re;
                    }
                    else
                    {
                        // no moving backwards.
                        return new LinkedList<Tuple<Field, List<Figure>>>();
                    }
                }
                else
                {
                    // end of the parking lot, you cannot move any further than this Field.
                    if (steps == 0)
                    {
                        LinkedList<Tuple<Field, List<Figure>>> re = new LinkedList<Tuple<Field, List<Figure>>>();
                        re.add(new Tuple<Field, List<Figure>>(this, new LinkedList<Figure>()));
                        return re;
                    }
                    else
                    {
                        return new LinkedList<Tuple<Field, List<Figure>>>();
                    }
                }
            }
            // TODO DONE Implement moveForward of ParkingLotField
        }
    }
}
