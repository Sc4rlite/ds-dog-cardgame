package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Tuple;


public class Panel implements Touchable, Animation
{

    private static final String TAG = Panel.class.getSimpleName();

    private static final Paint black = new Paint();

    static
    {
        black.setColor(android.graphics.Color.BLACK);
    }

    private Paint paint;
    private final Tuple<Float, Float> coords;
    private final float radius; // Not static because needs to be transformed by player edge first

    public Panel(Paint paint, Tuple<Float, Float> coords, float radius)
    {
        this.paint = paint;
        this.coords = coords;
        this.radius = radius;
    }

    public Tuple<Float, Float> getCoords()
    {
        return coords;
    }

    public void draw(Canvas canvas)
    {
        canvas.drawCircle(coords.left, coords.right, radius, black);
        canvas.drawCircle(coords.left, coords.right, radius * 0.8f, paint);
    }

    //=========================================================================

    @Override public float calcDistance(MotionEvent event)
    {
        float dx = event.getRawX() - coords.left;
        float dy = event.getRawY() - coords.right;
        return (float)Math.sqrt(dx * dx + dy * dy);
    }

    //=========================================================================

    @Override public boolean update(long diff)
    {
        boolean result = false;
        if (updateFlash(diff))
            result = true;
        return result;
    }

    //-------------------------------------------------------------------------

    // Flash field
    private static final long flashTime = 500;
    private boolean flashRunning = false;
    private Paint oldPaint;
    private boolean isBlack = false;
    private long flashTimer;

    public void startFlash()
    {
        oldPaint = paint;
        paint = black;
        isBlack = true;
        flashTimer = flashTime;
        flashRunning = true;
    }

    public void stopFlash()
    {
        flashRunning = false;
        paint = oldPaint;
    }

    private boolean updateFlash(long diff)
    {
        if (flashRunning)
        {
            flashTimer -= diff;
            if (flashTimer < 0)
            {
                paint = isBlack? oldPaint: black;
                isBlack = !isBlack;
                flashTimer = flashTime;
            }
            return true;
        }

        return false;
    }

}
