package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

import ch.ethz.inf.vs.a4.signerc.dogserver.util.Tuple;


/**
 * This class will translates every given coordinate in a different space. The
 * coordinates given are interpreted as percentage of the screen height (smaller
 * edge). Afterwards, the coordinates will be scaled to real size pixels,
 * rotated as desired and moved the upper left corner to the given coordinate.
 * 
 * @author sc4rlite
 *
 */
public class Transform
{

    private static final String TAG = Transform.class.getSimpleName();

    private final float heightDensity;
    private final float x;
    private final float y;
    private final double angle;
    private final float cos;
    private final float sin;

    /**
     * 
     * @param height
     *            Screen height
     * @param x
     *            X translation
     * @param y
     *            Y translation
     * @param angle
     *            Rotation angle
     */
    public Transform(int height, float x, float y, double angle)
    {
        // Set height for scaling
        this.heightDensity = height / 100f;

        // Set x and y for transition
        this.x = x;
        this.y = y;

        // Set sine and cosine for rotation
        this.angle = angle;
        cos = (float)Math.cos(angle);
        sin = (float)Math.sin(angle);
    }

    //-------------------------------------------------------------------------

    /**
     * Mathematicly correct modulo on float
     * 
     * @param dividend
     *            Dividend
     * @param divisor
     *            Divisor
     * @return Mathematicly correct modulo result
     */
    private static double mod(double dividend, double divisor)
    {
        return ((dividend % divisor) + divisor) % divisor;
    }

    private static double radiansToDegrees(double angle)
    {
        return angle * 360.0 / (2 * Math.PI);
    }

    public Tuple<Float, Float> getCenter()
    {
        return new Tuple<Float, Float>(x, y);
    }

    /**
     * Scales a coordinate to the given screen height. A length of 100 means
     * full screen height
     * 
     * @param coord
     * @return
     */
    public float scale(float coord)
    {
        // Scale
        return coord * heightDensity;
    }

    public double rotateAngle(double angle)
    {
        return mod(angle + this.angle, 360.0);
    }

    public double rotateAngleDegrees(double angle)
    {
        return mod(radiansToDegrees(angle + this.angle), 360.0);
    }

    /**
     * Transforms a given point coordinate
     * 
     * @param point
     *            The point coordinates as tuple which should get transformed
     *            into the desired space
     * @return The transformed coordinates
     */
    public Tuple<Float, Float> transform(Tuple<Float, Float> point)
    {
        // Copy input
        Tuple<Float, Float> result = new Tuple<Float, Float>(point.left, point.right);

        // Scale
        result.left = scale(point.left);
        result.right = scale(point.right);

        // Store original values
        float n = result.left;
        float m = result.right;

        // Rotate and translate
        result.left = n * cos - m * sin + x;
        result.right = n * sin + m * cos + y;

        return result;
    }
}
