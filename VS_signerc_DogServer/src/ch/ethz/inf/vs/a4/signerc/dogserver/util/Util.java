package ch.ethz.inf.vs.a4.signerc.dogserver.util;

import ch.ethz.inf.vs.a4.signerc.dogserver.R;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.BlueCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.FourCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.JokerCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.OneElevenCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.SevenCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.SwitchCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.ThirteenCard;


/**
 * Class that hosts all helper methods, enums and constants
 * 
 * @author Paige and Sphinx
 *
 */
public class Util
{
    /**
     * Constants for Fields on Board
     */
    public final static int NOF_FIELDS_PER_PLAYER = 21;
    public final static int INDEX_OF_HOME_FIELD = 0;
    public final static int INDEX_OF_START_FIELD = 1;
    public final static int INDEX_OF_FIRST_PARKING_FIELD = 2;
    public final static int INDEX_OF_LAST_PARKING_FIELD = 5;
    public final static int INDEX_OF_FIRST_COMMON_FIELD = 6;
    public final static int INDEX_OF_LAST_COMMON_FIELD = 20;

    /**
     * Constants for Cards in Game
     */
    public final static int ID_OF_FIRST_1_11 = 1;
    public final static int NUMBER_OF_1_11_CARDS = 8;
    public final static int ID_OF_FIRST_2 = 9;
    public final static int NUMBER_OF_2_CARDS = 8;
    public final static int ID_OF_FIRST_3 = 17;
    public final static int NUMBER_OF_3_CARDS = 8;
    public final static int ID_OF_FIRST_4 = 25;
    public final static int NUMBER_OF_4_CARDS = 8;
    public final static int ID_OF_FIRST_5 = 33;
    public final static int NUMBER_OF_5_CARDS = 8;
    public final static int ID_OF_FIRST_6 = 41;
    public final static int NUMBER_OF_6_CARDS = 8;
    public final static int ID_OF_FIRST_7 = 49;
    public final static int NUMBER_OF_7_CARDS = 8;
    public final static int ID_OF_FIRST_8 = 57;
    public final static int NUMBER_OF_8_CARDS = 8;
    public final static int ID_OF_FIRST_9 = 65;
    public final static int NUMBER_OF_9_CARDS = 8;
    public final static int ID_OF_FIRST_10 = 73;
    public final static int NUMBER_OF_10_CARDS = 8;
    public final static int ID_OF_FIRST_12 = 81;
    public final static int NUMBER_OF_12_CARDS = 8;
    public final static int ID_OF_FIRST_13 = 89;
    public final static int NUMBER_OF_13_CARDS = 8;
    public final static int ID_OF_FIRST_SWITCH = 97;
    public final static int NUMBER_OF_SWITCH_CARDS = 8;
    public final static int ID_OF_FIRST_JOKER = 105;
    public final static int NUMBER_OF_JOKER_CARDS = 6;

    /**
     * Represents a possible colour on the board
     * 
     * @author Paige and Sphinx
     *
     */
    public enum Color
    {
        BLUE(0, android.graphics.Color.BLUE, 0xff9999ff),
        RED(1, android.graphics.Color.RED, 0xffff9999),
        GREEN(2, 0xff009900, android.graphics.Color.GREEN),
        YELLOW(3, 0xffdddd00, 0xffffff99),
        CYAN(4, 0xff00dddd, 0xff99ffff),
        PURPLE(5, 0xffaa00ff, 0xffff99ff),
        ORANGE(6, 0xffeeaa00, 0xffffdd99),
        GREY(7, 0xff888888, 0xffcccccc);

        public final int ID;
        public final int hex;
        public final int hexLight;

        private Color(int ID, int hex, int hexLight)
        {
            this.ID = ID;
            this.hex = hex;
            this.hexLight = hexLight;
        }

        /**
         * converts player number i to the colour
         * 
         * @param i
         * @return
         */
        public static Color toColour(int i)
        {
            for (Color color: values())
                if (color.ID == i)
                    return color;
            assert (false);
            return Color.BLUE;
        }
    }

    /**
     * Represents all Numbers that are possible on blue cards
     * 
     * @author Paige and Sphinx
     *
     */
    public enum Number
    {
        TWO,
        THREE,
        FIVE,
        SIX,
        EIGHT,
        NINE,
        TEN,
        TWELFE;

        public int toInt()
        {
            switch (this)
            {
            case EIGHT:
                return 8;
            case FIVE:
                return 5;
            case NINE:
                return 9;
            case SIX:
                return 6;
            case TEN:
                return 10;
            case THREE:
                return 3;
            case TWELFE:
                return 12;
            case TWO:
                return 2;
            default:
                return 2;
            }
        }
    }

    public enum GameReceiveEventType
    {
        GAME_START,
        RECEIVE_CARD,
        RECEIVE_SWITCH_CARD,
        READY,
        REJOINED
    }

    public enum GameSendEventType
    {
        TURN,
        DISTRIBUTE_CARDS,
        REJECT_CARD,
        FINISHED
    }

    public static enum CardType
    {
        ONE(1, R.drawable.c11),
        TWO(2, R.drawable.c2),
        THREE(3, R.drawable.c3),
        FOUR(4, R.drawable.c4),
        FIVE(5, R.drawable.c5),
        SIX(6, R.drawable.c6),
        SEVEN(7, R.drawable.c7),
        EIGHT(8, R.drawable.c8),
        NINE(9, R.drawable.c9),
        TEN(10, R.drawable.c10),
        TWELVE(12, R.drawable.c12),
        THIRTEEN(13, R.drawable.c13),
        JOKER(15, R.drawable.j),
        SWITCH(14, R.drawable.t);

        private final int ID;
        public final int res;

        private CardType(int ID, int res)
        {
            this.ID = ID;
            this.res = res;
        }

        public int toInt()
        {
            return ID;
        }
    }

    public static Card getCardofType(int id, int type)
    {
        switch (type)
        {
        case 1:
            return new OneElevenCard(id);
        case 2:
            return new BlueCard(id, 2);
        case 3:
            return new BlueCard(id, 3);
        case 4:
            return new FourCard(4);
        case 5:
            return new BlueCard(id, 5);
        case 6:
            return new BlueCard(id, 6);
        case 7:
            return new SevenCard(id);
        case 8:
            return new BlueCard(id, 8);
        case 9:
            return new BlueCard(id, 9);
        case 10:
            return new BlueCard(id, 10);
        case 15:
            return new JokerCard(id);
        case 12:
            return new BlueCard(id, 12);
        case 13:
            return new ThirteenCard(id);
        case 14:
            return new SwitchCard(id);
        default:
            assert (false);
            return new BlueCard(id, 2);
        }
    }

    public static CardType toCardType(int i)
    {
        switch (i)
        {
        case 1:
            return CardType.ONE;
        case 2:
            return CardType.TWO;
        case 3:
            return CardType.THREE;
        case 4:
            return CardType.FOUR;
        case 5:
            return CardType.FIVE;
        case 6:
            return CardType.SIX;
        case 7:
            return CardType.SEVEN;
        case 8:
            return CardType.EIGHT;
        case 9:
            return CardType.NINE;
        case 10:
            return CardType.TEN;
        case 15:
            return CardType.JOKER;
        case 12:
            return CardType.TWELVE;
        case 13:
            return CardType.THIRTEEN;
        case 14:
            return CardType.SWITCH;
        default:
            assert (false);
            return CardType.TWO;
        }
    }

}
