package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.util.Log;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Tuple;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.CardType;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.HomeField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.ParkingLotField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.StartField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.OwnedField;


;

/**
 * Class that represents a Card
 * 
 * @author Paige and Sphinx
 * 
 */
public abstract class Card
{
    /**
     * Unique ID of Card
     */
    public final int ID;

    protected CardType cardType;

    /**
     * Constructor
     * 
     * @param id
     *        Unique ID of Card
     */
    public Card(int id)
    {
        ID = id;
    }

    /**
     * Determines possible plays when given Player uses this Card
     * 
     * @param player
     *        Player that wants to play this Card
     * @return List of possible Moves
     */
    public abstract List<Move> play(Player player);

    /**
     * Calculate simple plays for only one number, boolean and player
     * 
     * @param player
     *        Player whose Figures are moved
     * @param number
     *        Steps Figures are moved
     * @param isSeven
     *        Determines whether SevenCard was played
     * @return possible Moves in this simple Move
     */
    protected List<Move> calculateSimplePlays(Player player, int number, boolean isSeven)
    {
        List<Tuple<Figure, Field>> list;
        List<Move> result = new LinkedList<Move>();

        for (Figure f: player.getFigures())
        {
            for (Tuple<Field, List<Figure>> t: player.getGame().getBoard().positions.get(f).moveSteps(number, isSeven))
                if (t != null)
                { // check if t is valid move, then add it.
                    list = new LinkedList<Tuple<Figure, Field>>();
                    list.add(new Tuple<Figure, Field>(f, t.left));
                    if (t.right != null && !t.right.isEmpty())
                    {
                        for (Figure burnedFigure: t.right)
                        {
                            list.add(new Tuple<Figure, Field>(burnedFigure, burnedFigure.owner.getGame().getBoard()
                                    .findHomeField(burnedFigure.owner)));
                        }
                    }
                    result.add(new Move(list));
                }
        }
        return result;
    }

    // ______________________________________________________________________________________
    /**
     * Class that represents a Card of type BlueCard. This Card has no special
     * properties, it just represents a number of steps
     * 
     * @author Paige and Sphinx
     * 
     */
    public static class BlueCard extends Card
    {
        /**
         * Number that represents the number of steps this Card enables to
         */
        public Number number;

        /**
         * Constructor
         * 
         * @param id
         *        Unique ID of this Card
         */
        public BlueCard(int id, int num)
        {
            super(id);
            number = num;
            cardType = Util.toCardType(num);
            // TODO DONE may do more in BlueCard constructor
        }

        @Override public List<Move> play(Player player)
        {
            return calculateSimplePlays(player, number.intValue(), false);
        }
    }

    /**
     * Class that represents a Card of type JokerCard. This Card can be any
     * other Card
     * 
     * @author Paige and Sphinx
     * 
     */
    public static class JokerCard extends Card
    {
        /**
         * Constructor
         * 
         * @param id
         *        Unique ID of this Card
         */
        public JokerCard(int id)
        {
            super(id);
            cardType = Util.toCardType(15);
            // TODO DONE may do more in JokerCard constructor
        }

        @Override public List<Move> play(Player player)
        {
            List<Move> res = new LinkedList<Move>();
            SwitchCard switchCard = new SwitchCard(ID);
            for (int i = 0; i < player.getFigures().length; i++)
            {
                if (!(calculateSimplePlays(player, 1, false).isEmpty() && switchCard.play(player).isEmpty()))
                {
                    List<Tuple<Figure, Field>> list = new LinkedList<Tuple<Figure, Field>>();
                    list.add(new Tuple<Figure, Field>(null, null));
                    res.add(new Move(list));
                    return res;
                }
            }
            // TODO DONE implement play for JokerCard
            return res;
        }

    }

    /**
     * Class that represents a Card of type OneElevenCard. This Card enables to
     * exit the home field or move 1 or 11 steps forward
     * 
     * @author Paige and Sphinx
     * 
     */
    public static class OneElevenCard extends Card
    {
        /**
         * Constructor
         * 
         * @param id
         *        Unique ID of this Card
         */
        public OneElevenCard(int id)
        {
            super(id);
            cardType = Util.toCardType(1);
            // TODO DONE may do more in OneElevenCard constructor
        }

        @Override public List<Move> play(Player player)
        {
            List<Move> result = calculateSimplePlays(player, 1, false);
            result.addAll(calculateSimplePlays(player, 11, false));
            return result;
            // TODO DONE implement play for OneElevenCard
        }
    }

    /**
     * Class that represents a Card of type FourCard. This Card enables to move
     * 4 steps forward or backwards
     * 
     * @author Paige and Sphinx
     * 
     */
    public static class FourCard extends Card
    {
        /**
         * Constructor
         * 
         * @param id
         *        Unique ID of this Card
         * @param special
         *        Determines if Card has special properties
         */
        public FourCard(int id)
        {
            super(id);
            cardType = Util.toCardType(4);
            // TODO DONE may do more in FourCard constructor
        }

        @Override public List<Move> play(Player player)
        {
            List<Move> result = calculateSimplePlays(player, 4, false);
            for (Move m: result)
                Log.i("FourCard", "Moves with +4: " + m.displayMove());
            result.addAll(calculateSimplePlays(player, -4, false));
            for (Move m: result)
                Log.i("FourCard", "Moves with +4 and -4: " + m.displayMove());
            return result;

            // TODO DONE implement play for FourCard
        }

    }

    /**
     * Class that represents a Card of type SevenCard. This Card enables to
     * split 7 steps for different Figures, in total there has to be 7 steps.
     * All Figures that are encountered when moving are sent home.
     * 
     * @author Paige and Sphinx
     * 
     */
    public static class SevenCard extends Card
    {
        /**
         * Constructor
         * 
         * @param id
         *        Unique ID of this Card
         */
        public SevenCard(int id)
        {
            super(id);
            cardType = Util.toCardType(7);
            // TODO DONE may do more in SevenCard constructor
        }

        @Override public List<Move> play(Player player)
        {

            List<Move> res = new LinkedList<Move>();
            // find all Figures of the player that can move.
            Figure[] figures = movableFigures(player);
            if (figures.length > 0)
            {
                // Calculate all valid Combinations by which the movable Figures
                // can move.
                List<Tuple<Integer, List<Tuple<Figure, Integer>>>> combinations = stepCombinations(figures, 7, 0);
                for (Tuple<Integer, List<Tuple<Figure, Integer>>> combination: combinations)
                    res.addAll(movesForStepCombination(combination, player));
            }
            return res;
            // DONE implement play for SevenCard
        }

        /**
         * Calculate helper List for SevenCard with given Figure and steps
         * 
         * @param figure
         *        Figure that moves
         * @param steps
         *        Number of moved steps
         * @return List for each possible Move of List of Tuples of Figure and
         *         Field to create Moves
         */
        private List<List<Tuple<Figure, Field>>> calculateMovesForFigureAndSteps(Figure figure, int steps, Board board)
        {
            List<Tuple<Field, List<Figure>>> list = board.positions.get(figure).moveSteps(steps, true);
            List<List<Tuple<Figure, Field>>> res = new LinkedList<List<Tuple<Figure, Field>>>();

            if (list != null && !list.isEmpty())
            {
                for (Tuple<Field, List<Figure>> tuple: list)
                {
                    List<Tuple<Figure, Field>> moves = new LinkedList<Tuple<Figure, Field>>();
                    moves.add(new Tuple<Figure, Field>(figure, tuple.left));
                    if (board.isVirtual)
                    {
                        if (!tuple.right.isEmpty())
                        {
                            for (Figure burnedFigure: tuple.right)
                            {
                                moves.add(new Tuple<Figure, Field>(burnedFigure, board
                                        .findHomeField(burnedFigure.owner)));
                            }
                        }
                    }
                    res.add(moves);
                }
                return res;
            }
            return res;
        }

        static int round = 0;

        Figure[] movableFigures(Player player)
        {
            ArrayList<Figure> movable = new ArrayList<Figure>();
            for (Figure figure: player.getFigures())
                if (figure.active() && !figure.atHome())
                    movable.add(figure);
            Figure[] res = new Figure[movable.size()];
            for (int i = 0; i < movable.size(); i++)
                res[i] = movable.get(i);
            return res;
        }

        /**
         * Calculate all combinations to divide the remaining steps on the given
         * figures.
         * 
         * @param figures
         *        Figures that have not yet been moved
         * @param stepsleft
         *        Steps that have not been used yet
         * @param depth
         *        the current depth of recursion
         * @return The list of possible combinations of steps per figure and
         *         remaining steps
         */
        List<Tuple<Integer, List<Tuple<Figure, Integer>>>> stepCombinations(Figure[] figures, int stepsleft, int depth)
        {
            List<Tuple<Integer, List<Tuple<Figure, Integer>>>> recursiveRecievedlist;
            List<Tuple<Integer, List<Tuple<Figure, Integer>>>> res = new LinkedList<Tuple<Integer, List<Tuple<Figure, Integer>>>>();

            if (depth == figures.length - 1)
            {
                // end of recursion reached, cannot go deeper.
                for (int i = 1; i <= stepsleft; i++)
                {
                    List<List<Tuple<Figure, Field>>> moves = calculateMovesForFigureAndSteps(
                            figures[figures.length - 1], i, figures[depth].owner.getGame().getBoard());
                    if (!moves.isEmpty())
                    {
                        List<Tuple<Figure, Integer>> list = new LinkedList<Tuple<Figure, Integer>>();
                        list.add(new Tuple<Figure, Integer>(figures[depth], i));
                        res.add(new Tuple<Integer, List<Tuple<Figure, Integer>>>(stepsleft - i, list));
                    }
                }
                res.add(new Tuple<Integer, List<Tuple<Figure, Integer>>>(stepsleft,
                        new LinkedList<Tuple<Figure, Integer>>()));
            }
            else
            {
                // get moves that require another recursion step
                for (int i = 0; i < stepsleft; i++)
                {
                    recursiveRecievedlist = stepCombinations(figures, stepsleft - i, depth + 1);
                    if (recursiveRecievedlist != null && !recursiveRecievedlist.isEmpty())
                    {
                        // if recursivePlay returns null, it is impossible to
                        // divide the remaining steps between the remaining
                        // figures
                        List<List<Tuple<Figure, Field>>> recent = calculateMovesForFigureAndSteps(figures[depth], i,
                                figures[depth].owner.getGame().getBoard());
                        if (recent != null && !recent.isEmpty())
                        {
                            for (Tuple<Integer, List<Tuple<Figure, Integer>>> recursive: recursiveRecievedlist)
                            {
                                List<Tuple<Figure, Integer>> list = recursive.right;
                                if (i > 0)
                                    list.add(new Tuple<Figure, Integer>(figures[depth], i));
                                res.add(new Tuple<Integer, List<Tuple<Figure, Integer>>>(recursive.left, list));
                            }
                        }
                    }
                }

                // get move that uses up all remaining steps on this figure - no
                // recursion necessary, since all figures can move zeros
                // steps(aka standing still).
                List<List<Tuple<Figure, Field>>> recent = calculateMovesForFigureAndSteps(figures[depth], stepsleft,
                        figures[depth].owner.getGame().getBoard());
                if (recent != null && !recent.isEmpty())
                {
                    List<Tuple<Figure, Integer>> list = new LinkedList<Tuple<Figure, Integer>>();
                    list.add(new Tuple<Figure, Integer>(figures[depth], stepsleft));
                    res.add(new Tuple<Integer, List<Tuple<Figure, Integer>>>(0, list));
                }
            }
            return res;
        }

        /**
         * @param stepCombination
         *        One combination of steps per Figure and remaining steps
         * @param player
         *        Player that played this Card
         * @return List of valid Moves of a single step combination
         */
        List<Move> movesForStepCombination(Tuple<Integer, List<Tuple<Figure, Integer>>> stepCombination, Player player)
        {
            List<Move> res = new LinkedList<Move>();
            List<Tuple<Board, List<Tuple<Figure, Field>>>> possiblePermutations = possiblePermutations(
                    stepCombination.right, player.getGame().getBoard());

            for (Tuple<Board, List<Tuple<Figure, Field>>> tuple: possiblePermutations)
            {
                if (stepCombination.left == 0)
                {
                    addValidMove(res, tuple.right);
                }
                else
                {
                    boolean allParked = true;
                    // Log.i("SevenPlayForPartner", "steps left");
                    for (int i = 0; i < player.getFigures().length; i++)
                    {
                        if (!((tuple.left.positions.get(player.getFigures()[i]) instanceof ParkingLotField) && ((ParkingLotField)tuple.left.positions
                                .get(player.getFigures()[i])).isInactiveParkingLotField()))
                        {
                            // Log.i("SevenPlayForPartner",
                            // "Figure not parked: Figure " +
                            // player.getFigures()[i]);
                            allParked = false;
                            break;
                        }
                    }

                    if (allParked)
                    {
                        Figure[] figures = movableFigures(player.teamMember);
                        Log.i("SevenPlayForPartner", figures.length > 0? "partner has figures left": "no figures left");
                        if (figures.length > 0)
                        {
                            List<Tuple<Integer, List<Tuple<Figure, Integer>>>> combinations = stepCombinations(figures,
                                    stepCombination.left, 0);
                            for (Tuple<Integer, List<Tuple<Figure, Integer>>> combination: combinations)
                            {
                                if (combination.left == 0)
                                {
                                    List<Tuple<Board, List<Tuple<Figure, Field>>>> partnerPermutions = possiblePermutations(
                                            combination.right, tuple.left);
                                    for (Tuple<Board, List<Tuple<Figure, Field>>> partnerTuple: partnerPermutions)
                                    {
                                        filter(partnerTuple.right, player);
                                        List<Tuple<Figure, Field>> tempList = new LinkedList<Tuple<Figure, Field>>(
                                                tuple.right);
                                        for (Tuple<Figure, Field> elem: partnerTuple.right)
                                            add(tempList, elem);
                                        addValidMove(res, tempList);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return res;
        }

        /**
         * Calculate all possible permutations for the stepsCombination list.Add
         * to each permutation of stepsCombination its own new copy of virtual
         * Board.
         * 
         * @param stepCombination
         *        the Combination of movements of Figures
         * @param board
         *        the Board belonging to the stepCombinations
         * @return a list of Tuples. Tuples contain a permutation and its copy
         *         of the Board.
         */
        List<Tuple<Board, List<Tuple<Figure, Field>>>> possiblePermutations(
                List<Tuple<Figure, Integer>> stepCombination, Board board)
        {
            List<Tuple<Board, List<Tuple<Figure, Field>>>> res = new LinkedList<Tuple<Board, List<Tuple<Figure, Field>>>>();
            List<List<Tuple<Figure, Integer>>> permutations = permutations(stepCombination);

            for (List<Tuple<Figure, Integer>> order: permutations)
            {
                Board virtualBoard = board.copy();
                res.addAll(possiblePermutationsExecute(order, virtualBoard));
            }
            return res;

        }

        /**
         * Try to execute stepCombination on the virtual Board in order to
         * verify if it represents a valid move. All permutations of the order
         * in which the figures can move the stepCombination are tested.
         * 
         * @param stepCombination
         *        the stepCombination that is to be verified
         * @param virtualBoard
         *        the Board on which the stepCombination is executed
         * @return return a List of Tuples. The Tuples contain a Board and the
         *         matching List of the remaining moves of stepCombintation
         * 
         */
        private List<Tuple<Board, List<Tuple<Figure, Field>>>> possiblePermutationsExecute(
                List<Tuple<Figure, Integer>> stepCombination, Board virtualBoard)
        {
            List<Tuple<Board, List<Tuple<Figure, Field>>>> res = new LinkedList<Tuple<Board, List<Tuple<Figure, Field>>>>();
            if (!stepCombination.isEmpty())
            {
                List<List<Tuple<Figure, Field>>> temp = calculateMovesForFigureAndSteps(stepCombination.get(0).left,
                        stepCombination.get(0).right, virtualBoard);
                for (List<Tuple<Figure, Field>> single: temp)
                {
                    Board tempBoard = virtualBoard.copy();

                    for (Tuple<Figure, Field> tuple: single)
                    {
                        Field oldField = tempBoard.positions.get(tuple.left);
                        Field newField = tempBoard.accordingFieldOnBoard(tuple.right);

                        // moving of burned figure is not allowed
                        if (oldField != null)
                        {
                            if (oldField.isOccupiedBy() != null && oldField.isOccupiedBy().equals(tuple.left))
                                oldField.setOccupant(null);
                        }
                        if (newField instanceof HomeField)
                            ((HomeField)newField).comeHome(tuple.left);

                        newField.setOccupant(tuple.left);
                        tempBoard.positions.put(tuple.left, newField);
                    }

                    // recursion
                    if (stepCombination.size() > 1)
                    {
                        List<Tuple<Board, List<Tuple<Figure, Field>>>> recursive = possiblePermutationsExecute(
                                stepCombination.subList(1, stepCombination.size()), tempBoard);
                        for (Tuple<Board, List<Tuple<Figure, Field>>> insert: recursive)
                        {
                            List<Tuple<Figure, Field>> insertlist = new LinkedList<Tuple<Figure, Field>>(insert.right);
                            insertlist.add(0, single.get(0));
                            insertlist.addAll(single.subList(1, single.size()));
                            res.add(new Tuple<Board, List<Tuple<Figure, Field>>>(insert.left, insertlist));
                        }
                    }
                    else
                    {
                        res.add(new Tuple<Board, List<Tuple<Figure, Field>>>(tempBoard, single));
                    }
                }
            }
            return res;
        }

        /**
         * Calculate all permutations of the elements in a given list.
         * 
         * @param list
         *        list whose elements permutations are calculated
         * @return a list containing the permutated lists of the input list.
         */
        private List<List<Tuple<Figure, Integer>>> permutations(List<Tuple<Figure, Integer>> list)
        {
            List<List<Tuple<Figure, Integer>>> res = new LinkedList<List<Tuple<Figure, Integer>>>();
            if (list.size() == 0)
            {
                List<Tuple<Figure, Integer>> temp = new LinkedList<Tuple<Figure, Integer>>();
                res.add(temp);
            }
            else
            {
                List<List<Tuple<Figure, Integer>>> subList = permutations(list.subList(1, list.size()));
                for (List<Tuple<Figure, Integer>> elem: subList)
                {
                    for (int i = 0; i <= elem.size(); i++)
                    {
                        List<Tuple<Figure, Integer>> temp = new LinkedList<Tuple<Figure, Integer>>(elem);
                        temp.add(i, list.get(0));
                        res.add(temp);
                    }
                }
            }
            return res;
        }

        /**
         * Add the element to the list, if the list does not contain the element
         * yet.
         * 
         * @param list
         *        list to which the element should be added
         * @param element
         *        element that should be added to the list
         * 
         */
        private void add(List<Tuple<Figure, Field>> list, Tuple<Figure, Field> element)
        {
            if (!list.contains(element))
                list.add(element);
        }

        /**
         * Create a move from a list. Check if the movements are valid and add
         * the move to an already existing list of moves.
         * 
         * @param moves
         *        preexisting list of Moves
         * @param list
         *        list that contains the movements of figures that are made into
         *        a new move if the movements are valid
         */
        private void addValidMove(List<Move> moves, List<Tuple<Figure, Field>> list)
        {
            boolean valid = true;
            for (Tuple<Figure, Field> elem: list)
            {
                if (elem.right instanceof HomeField)
                {
                    for (Tuple<Figure, Field> subelem: list.subList(list.indexOf(elem) + 1, list.size()))
                    {
                        if (elem.left.equals(subelem.left))
                        {
                            valid = false;
                            break;
                        }
                    }
                }
                if (!valid)
                    break;

            }
            if (valid)
            {
                List<Tuple<Figure, Field>> validlist = new LinkedList<Tuple<Figure, Field>>();
                for (Tuple<Figure, Field> elem: list)
                    validlist.add(new Tuple<Figure, Field>(elem.left, elem.left.owner.getGame().getBoard()
                            .accordingFieldOnBoard(elem.right)));
                moves.add(new Move(validlist));
            }
        }

        /**
         * Remove all elements from a List of Tuples representing movements, if
         * the Figure in the Tuple belongs to the player.
         * 
         * @param list
         *        a list of Tuples, which has a Figure and the Field it moves to
         * @param player
         *        The Player whose Figures movements should be removed from the
         *        list.
         */
        private void filter(List<Tuple<Figure, Field>> list, Player player)
        {
            for (Tuple<Figure, Field> elem: list)
            {
                if (elem.left.owner.equals(player))
                    list.remove(elem);
            }
        }

    }

    /**
     * Class that represents a Card of type ThirteenCard. This Card enables to
     * exit the home field or move 13 steps forward
     * 
     * @author Paige and Sphinx
     * 
     */
    public static class ThirteenCard extends Card
    {
        /**
         * Constructor
         * 
         * @param id
         *        Unique ID of this Card
         */
        public ThirteenCard(int id)
        {
            super(id);
            cardType = Util.toCardType(13);
            // TODO DONE may do more in ThirteenCard constructor
        }

        @Override public List<Move> play(Player player)
        {
            List<Move> result = calculateSimplePlays(player, 13, false);
            return result;

            // TODO DONE implement play for ThirteenCard

        }

    }

    /**
     * Class that represents a Card of type SwitchCard. This Card enables switch
     * one of the own Figures that is active and not home with an arbitrary
     * Figure that is also active and not home
     * 
     * @author Paige and Sphinx
     * 
     */
    public static class SwitchCard extends Card
    {
        /**
         * Constructor
         * 
         * @param id
         *        Unique ID of this Card
         */
        public SwitchCard(int id)
        {
            super(id);
            cardType = Util.toCardType(14);
            // TODO DONE may do more in SwitchCard constructor
        }

        @Override public List<Move> play(Player player)
        {
            List<Figure> ownFigures = new ArrayList<Figure>();
            List<Figure> otherFigures = new ArrayList<Figure>();
            List<Tuple<Figure, Field>> list = new LinkedList<Tuple<Figure, Field>>();
            List<Move> result = new LinkedList<Move>();

            // get all switchable figures
            addSwitchableFiguresOfPlayer(player, ownFigures);
            for (Player p: player.getGame().getPlayers())
            {
                if (!p.equals(player))
                    addSwitchableFiguresOfPlayer(p, otherFigures);
            }

            for (Figure f: otherFigures)
                Log.i("SwitchCard", "Switchable Figure: Figure " + f.ID + " of Player " + f.owner.name);

            for (Figure ownFigure: ownFigures)
                for (Figure otherFigure: otherFigures)
                {
                    list.clear();
                    list.add(new Tuple<Figure, Field>(ownFigure, player.getGame().getBoard().positions.get(otherFigure)));
                    list.add(new Tuple<Figure, Field>(otherFigure, player.getGame().getBoard().positions.get(ownFigure)));
                    result.add(new Move(list));
                }

            // TODO DONE implement play for SwitchCard
            return result;

        }

        private void addSwitchableFiguresOfPlayer(Player p, List<Figure> figures)
        {
            for (Figure f: p.getFigures())
            {
                if (!(f.atHome() || (p.getGame().getBoard().positions.get(f) instanceof ParkingLotField)))
                {
                    if (p.getGame().getBoard().positions.get(f) instanceof StartField)
                    {
                        if (!((OwnedField)p.getGame().getBoard().positions.get(f)).getOwner().equals(f.owner))
                        {
                            figures.add(f);
                        }

                    }
                    else
                    {
                        figures.add(f);
                    }
                }
            }
        }

    }

}
