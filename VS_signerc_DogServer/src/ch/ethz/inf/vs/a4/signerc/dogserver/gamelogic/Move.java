package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import java.util.LinkedList;
import java.util.List;

import ch.ethz.inf.vs.a4.signerc.dogserver.util.Tuple;


/**
 * Class that represents a Move on the Board, it assigns each Figure that
 * changes position the new Field
 * 
 * @author Paige and Sphinx
 *
 */
public class Move
{
    /**
     * Represents all Figures that are moving and their new positions
     */
    private List<Tuple<Figure, Field>> moves;

    /**
     * Constructor
     * 
     * @param m
     *        List of Tuples of moving Figures and their new Field positions
     */
    public Move(List<Tuple<Figure, Field>> m)
    {
        moves = new LinkedList<Tuple<Figure, Field>>(m);
    }

    /**
     * Determines the Field that the given Figure will occupy after this Move
     * 
     * @param figure
     *        Figure whose position is wanted
     * @return Field that the given Figure will be on after this Move
     */
    public Field move(Figure figure)
    {
        // implement move

        // return new field, if figure is affected by this move
        for (Tuple<Figure, Field> t: moves)
        {
            if (t.left.equals(figure))
            {
                return t.right;
            }
        }

        // return current field, if this move does not affect this figure
        return figure.owner.getGame().getBoard().positions.get(figure);
    }

    public String displayMove()
    {
        String res = "Move: ";
        for (Tuple<Figure, Field> t: moves)
        {
            if (t.left == null || t.right == null)
                res = res + ", Empty Entry";
            else
                res = res + ", " + "Figure " + t.left.ID + " of Player " + t.left.owner.ID + " to Field "
                        + t.right.fieldID + " of Player " + t.right.playerID;
        }
        return res;

    }

    /**
     * Gives all Figures that will move in this Move
     * 
     * @return List of moving Figures
     */
    public List<Figure> movedFigures()
    {
        if (moves.isEmpty())
            return null;
        else
            return moves.get(0).returnListLeft(moves);
    }

}
