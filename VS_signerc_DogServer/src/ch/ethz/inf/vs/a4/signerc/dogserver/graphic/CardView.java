package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.CardType;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;


public class CardView implements Animation
{

    private static final String TAG = CardView.class.getSimpleName();

    private static final Paint paint = new Paint();

    private final Bitmap bitmap;
    private final float scale;
    private final Matrix matrix = new Matrix();
    private final float halfHeight;
    private final float halfWidth;
    private float x = 0f;
    private float y = 0f;
    private float angle = 0f;

    public CardView(Context context, CardCache cardCache, CardType type, int height, int width)
    {
        bitmap = cardCache.get(type);
        halfHeight = height / 2f;
        halfWidth = width / 2f;
        scale = (float)height / bitmap.getHeight();
    }

    private float translateX(float x)
    {
        return x - halfWidth;
    }

    private float translateY(float y)
    {
        return y - halfHeight;
    }

    public void setX(float x)
    {
        this.x = translateX(x);
    }

    public void setY(float y)
    {
        this.y = translateY(y);
    }

    public void setAngle(float angle)
    {
        this.angle = angle;
    }

    public void draw(Canvas canvas)
    {
        matrix.setScale(scale, scale);
        matrix.postRotate(angle, halfWidth, halfHeight);
        matrix.postTranslate(x, y);
        canvas.drawBitmap(bitmap, matrix, paint);
    }

    //=========================================================================

    @Override public boolean update(long diff)
    {
        boolean result = false;
        if (updateMove(diff))
            result = true;
        if (updateRotate(diff))
            result = true;
        return result;
    }

    //-------------------------------------------------------------------------

    // Move a card to another position
    private static final long moveTime = 500;
    private boolean moveRunning = false;
    private float startX;
    private float startY;
    private float destX;
    private float destY;
    private float stepX;
    private float stepY;

    public void startMove(float destX, float destY)
    {
        startX = x;
        startY = y;
        this.destX = translateX(destX);
        this.destY = translateY(destY);
        stepX = (this.destX - x) / moveTime;
        stepY = (this.destY - y) / moveTime;
        moveRunning = true;
    }

    private boolean updateMove(long diff)
    {
        if (moveRunning)
        {
            x += stepX * diff;
            y += stepY * diff;
            if ((startX <= destX && destX <= x || x <= destX && destX <= startX)
                    && (startY <= destY && destY <= y || y <= destY && destY <= startY))
            {
                x = destX;
                y = destY;
                moveRunning = false;
            }
            return true;
        }

        return false;
    }

    //-------------------------------------------------------------------------

    // Rotate the card to a specific angle
    private static final long rotateTime = 500;
    private boolean rotateRunning = false;
    private float destAngle;
    private float stepAngle;
    private boolean clockwise;

    public void startRotate(float destAngle)
    {
        this.destAngle = match(destAngle);
        clockwise = match(destAngle - angle) <= match(angle - destAngle);
        stepAngle = Math.min(match(destAngle - angle), match(angle - destAngle)) / rotateTime;
        rotateRunning = true;
    }

    private float match(float a)
    {
        return ((a % 360f) + 360f) % 360f;
    }

    private boolean updateRotate(long diff)
    {
        if (rotateRunning)
        {
            if (angle < 0.0 || angle >= 360.0)
                angle = match(angle);
            float oldAngle = angle;
            angle += (clockwise? 1: -1) * stepAngle * diff;
            //if (clockwise && (destAngle - angle) <= 0.0 || !clockwise && (angle - destAngle) <= 0.0)
            if (oldAngle <= destAngle && destAngle <= angle || angle <= destAngle && destAngle <= oldAngle)
            {
                angle = destAngle;
                rotateRunning = false;
            }
            return true;
        }

        return false;
    }

}
