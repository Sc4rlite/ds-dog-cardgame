package ch.ethz.inf.vs.a4.signerc.dogserver.communication;

import java.io.IOException;

import org.json.JSONObject;

import android.bluetooth.BluetoothSocket;


public class DummyConnection extends Connection
{

    public DummyConnection(BluetoothSocket s, ConnectionManager m, int p)
    {
        number = p;
        //	m.ConnectionEvent(p, "Player " + p + " joined");
    }

    @Override public boolean discover()
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override public void send(JSONObject msg)
    {
        // TODO Auto-generated method stub

    }

    @Override public void broadcast(String msg)
    {
        // TODO Auto-generated method stub

    }

    @Override public void disconnect()
    {
        // TODO Auto-generated method stub

    }

}
