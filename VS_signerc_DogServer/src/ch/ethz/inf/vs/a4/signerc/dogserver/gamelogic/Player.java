package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.ethz.inf.vs.a4.signerc.dogserver.util.Tuple;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.Color;


/**
 * Class that represents a Player
 * 
 * @author Paige and Sphinx
 *
 */

public class Player
{
    /**
     * name of the player
     */
    public final String name;

    /**
     * ID of the player
     */
    public final int ID;

    /**
     * Object of player that this Player builds a team with
     */
    protected Player teamMember;

    /**
     * Cards in hand of player
     */
    protected final List<Card> hand;

    /**
     * Colour of own field and figures
     */
    protected Color colour;

    /**
     * Figures that are owned by this Player
     */
    private Figure[] figures;

    /**
     * Game that the Player is participating in
     */
    private Game game;

    /**
     * Constructor with name
     * 
     * @param n
     *        Name of player
     * @param g
     *        Game that this Player is participating in
     * @param id
     *        Unique ID of player
     */
    public Player(String n, int id, Game g)
    {
        name = n;
        ID = id;
        hand = new LinkedList<Card>();
        figures = new Figure[4];
        for (int i = 0; i < 4; i++)
            figures[i] = new Figure(this, i);
        game = g;
    }

    /**
     * Constructor without name
     * 
     * @param id
     *        Unique ID of player
     * @param g
     *        Game that this Player is participating in
     */
    public Player(int id, Game g)
    {
        this("", id, g);
    }

    /**
     * Getter method for game
     * 
     * @return Game of this Player
     */
    public Game getGame()
    {
        return game;
    }

    /**
     * set the colour belonging to this player
     * 
     * @param c
     *        Colour that will be set
     */
    public void setColour(Color c)
    {
        colour = c;
    }

    /**
     * Get colour belonging to this player
     * 
     * @return Colour belonging to player
     */
    public Color getColour()
    {
        return colour;
    }

    /**
     * Sets partner and assures that this Player is the partner's teamMember
     * 
     * @param partner
     *        Player that this Player builds a team with
     */
    public void setTeam(Player partner)
    {
        assert (partner != null);
        teamMember = partner;
        if (partner.teamMember == null)
            partner.setTeam(this);
        //May be better don with a customised exception
        assert (partner.teamMember.equals(this));
        //TODO maybe add code to setTeam
    }

    public String displayHand()
    {
        String res = "Player " + ID + " 's hand: ";
        for (Card c: hand)
            res = res + ", " + c.ID;
        return res;
    }

    /**
     * Getter method for teamMember
     * 
     * @return Player that is partner
     */
    public Player getTeamMember()
    {
        return teamMember;
    }

    /**
     * Getter function for Figures
     * 
     * @return Figures owned by this Player
     */
    public Figure[] getFigures()
    {
        return figures;
    }

    /**
     * Play out the specified Card
     * 
     * @param card
     *        Card that is played
     * @return boolean whether playing this Card was successful
     */
    public boolean playCard(Card card)
    {
        //TODO maybe more has to be done in playCard
        return hand.remove(card);
    }

    /**
     * Setting Cards in hand when receiving new Cards at the start of turn
     * 
     * @param cards
     *        List of Cards that are received
     */
    public void receiveHand(List<Card> cards)
    {
        //not just copying of reference to avoid coupling
        hand.clear();
        for (Card c: cards)
            hand.add(c);
        //TODO add functionality to receiveHand
    }

    /**
     * Removing Card from hand when sending it to partner
     * 
     * @param card
     *        Card that was sent to partner
     * @return boolean whether Card was actually in hand
     */
    public boolean sendCard(Card card)
    {
        //TODO extend sendCard
        return hand.remove(card);
    }

    /**
     * Adds card to hand when receiving it from partner
     * 
     * @param card
     *        Card that is received by partner
     */
    public void receiveCard(Card card)
    {
        //TODO extend receiveCard
        hand.add(card);
    }

    /**
     * dumps all cards, hand is empty afterwards
     */
    public void dumpCards()
    {
        hand.clear();
    }

    /**
     * Lists all possible plays for this player with the current status of hand
     * and board
     * 
     * @return List of Tuples of Cards in hand with their according List of
     *         possible moves
     */
    protected Map<Card, List<Move>> possiblePlays()
    {
        Map<Card, List<Move>> result = new HashMap<Card, List<Move>>();
        List<Move> moves = new LinkedList<Move>();

        Player player = this;
        //play for teamMember if finished already
        if (allFiguresParked())
            player = teamMember;

        for (Card c: hand)
        {
            moves = c.play(player);
            //this card is playable if there are possible moves
            if (moves != null && !moves.isEmpty())
            {
                result.put(c, moves);
            }
        }

        //TODO DONE find all possible movement for player
        return result;
    }

    /**
     * Get Card in hand of this Player
     * 
     * @param id
     *        ID of requested Card
     * @return Card that was requested or null if not found
     */
    public Card getCardWithID(int id)
    {
        for (Card card: hand)
        {
            if (card.ID == id)
                return card;
        }
        return null;
    }

    public boolean allFiguresParked()
    {
        for (int i = 0; i < figures.length; i++)
        {
            if (figures[i].active())
                return false;
        }
        return true;
    }
}
