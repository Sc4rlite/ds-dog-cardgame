package ch.ethz.inf.vs.a4.signerc.dogserver;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Game;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Player;
import ch.ethz.inf.vs.a4.signerc.dogserver.graphic.GamePanel;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util;


public class MainActivity extends Activity implements OnClickListener
{

    private Game game;
    private GamePanel gamepanel;
    private static final int DISCOVERABLE_REQUEST_CODE = 0x1;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // input dialog for game name
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this).setTitle("NEW GAME").setMessage(
                "Please enter the name of the game!");
        final EditText gameName = new EditText(MainActivity.this);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                if (gameName.getText().equals(""))
                {
                    submitName("DOG GAME");
                }
                else
                {
                    submitName(gameName.getText().toString());
                }
                dialog.cancel();
            }
        });
        // Setting Negative Button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                submitName("DOG GAME");
                dialog.cancel();
            }
        });

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        gameName.setLayoutParams(lp);
        alertDialog.setView(gameName);

        alertDialog.show();

        // Hide title and make it fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Set game panel as view
        gamepanel = new GamePanel(this);
        setContentView(gamepanel);
    }

    String name;

    private void submitName(String name)
    {
        this.name = name;
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(discoverableIntent, DISCOVERABLE_REQUEST_CODE);
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        game = new Game(4, name, gamepanel);
    }

    @Override public void onBackPressed()
    {
        game.connection.disconnect();
        super.onBackPressed();
    }

    public void showRestart(String message)
    {
        // buttons for the popup
        Button restart = new Button(this);
        restart.setTag("restartButton");
        restart.setTextColor(Color.WHITE);
        restart.setText("Restart");
        restart.setOnClickListener(this);

        // text for the popup
        TextView popupText = new TextView(this);
        popupText.setGravity(Gravity.CENTER_HORIZONTAL);
        popupText.setTextSize(20);
        popupText.setText(message);
        popupText.setPadding(0, 0, 0, 0);
        popupText.setTextColor(Color.WHITE);

        // layout of the popup
        final LinearLayout popupLayout = new LinearLayout(this);
        popupLayout.setOrientation(1);
        popupLayout.addView(popupText);
        popupLayout.addView(restart);
        popupLayout.setBackgroundColor(Color.BLACK);
        popupLayout.setBackground(getResources().getDrawable(R.drawable.popupborder));

        final PopupWindow popup = new PopupWindow(popupLayout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
                true);
        runOnUiThread(new Runnable()
        {
            @Override public void run()
            {
                popup.showAtLocation(popupLayout, Gravity.CENTER, 0, 0);
            }
        });
    }

    @Override public void onClick(View view)
    {
        if (view.getTag() instanceof String)
        {
            String tag = (String)view.getTag();
            if (tag.equals("restartButton"))
            {
                // Restart application: Pure magic, no one knows how this works
                Intent mStartActivity = new Intent(this, MainActivity.class);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, mStartActivity,
                        PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                finish();
            }
        }
    }
}
