package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

import android.view.MotionEvent;


public interface Touchable extends Animation
{

    public float calcDistance(MotionEvent event);

    public void startFlash();

    public void stopFlash();

}
