package ch.ethz.inf.vs.a4.signerc.dogserver.communication;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util;


public class ConnectionManager
{

    private String gameName;
    private int i = 0;
    private int numberOfPlayers;
    private Connection[] players;
    private BluetoothServerSocket serverSocket;
    private ConnectionManager meFromThread;
    private List<ConnectionListener> listener;
    private Queue<Integer> available;
    private boolean scanning = false;
    boolean mainGameStarted = false;
    String TAG = "ConnectionManager";
    BluetoothAdapter adapter;

    /**
     * The connection manager handles the connections to all devices. It also
     * handles the setup of the communication and teambuilding.
     */
    public ConnectionManager()
    {
        meFromThread = this;
        listener = new LinkedList<ConnectionListener>();
        available = new ConcurrentLinkedQueue<Integer>();
        adapter = BluetoothAdapter.getDefaultAdapter();
    }

    /**
     * make the server discoverable to the devices
     * 
     * @param gameName
     *        The name of the game (if null or "" the name is "DOG GAME")
     * @param numberOfPlayers
     *        Amount of players in the game. Must be 4 or 6
     */
    public void startServer(String name, int numberOfPlayers)
    {
        assert (numberOfPlayers == 4 || numberOfPlayers == 6);
        players = new Connection[4];
        scanning = false;
        mainGameStarted = false;
        temp = null;
        lowestTeamPos = 0;
        if (gameName == null || gameName.equals(""))
            gameName = "DOG GAME";
        this.gameName = name;
        this.numberOfPlayers = numberOfPlayers;
        for (int i = 0; i < numberOfPlayers; i++)
        {
            available.add(i);
        }
        // for(int i = 0; i < 2; i++){
        // int q = available.poll();
        // players[q] = new DummyConnection(null, this, q);
        // players[q].setPlayer("Dummy " + q, q + "");
        // setPreferedTeam(q, 3);
        // }

        scanning = true;
        new Thread(btScan).start();
        // new Thread(infoThread).start();
    }

    /**
     * send a json packet to the receiver
     * 
     * @param receiver
     * @param json
     */
    public void send(int receiver, JSONObject json)
    {
        synchronized (players[receiver])
        {
            if (players[receiver] != null)
            {
                Log.i(TAG, "sending " + json.toString() + " to " + receiver);
                players[receiver].send(json);
            }
        }
    }

    /**
     * send a json packet to every connected device
     * 
     * @param json
     */
    public void broadcast(JSONObject json)
    {
        for (Connection c: players)
        {
            if (c != null)
            {
                c.send(json);
            }
        }
    }

    /**
     * send a json packet to every connected device except the one specified
     * with exception
     * 
     * @param json
     *        the object
     * @param exception
     *        the receiver that is excluded
     */
    public void broadcastExcept(JSONObject json, int exception)
    {
        for (Connection c: players)
        {
            if (c != null && !c.equals(players[exception]))
            {
                c.send(json);
            }
        }
    }

    /**
     * This thread offers information about the server to the clients
     */
    BluetoothSocket infoSocket = null;
    private Runnable infoThread = new Runnable()
    {
        public void run()
        {
            UUID uuid = ConnectionUtils.scan_uuid;
            JSONObject json;
            BluetoothServerSocket infoSocket = null;
            BluetoothSocket socket = null;
            OutputStream out = null;
            // InputStream in = null;
            while (scanning)
            {
                try
                {
                    infoSocket = adapter.listenUsingInsecureRfcommWithServiceRecord(uuid.toString(), uuid);
                    socket = infoSocket.accept();
                    out = socket.getOutputStream();
                    // in = infoSocket.getInputStream();
                    // in.read();
                    json = new JSONObject();
                    json.put(ConnectionUtils.CMD, ConnectionUtils.GET_NAME);
                    json.put(ConnectionUtils.VALUE, gameName);
                    json.put(ConnectionUtils.SIZE, numberOfPlayers);
                    json.put(ConnectionUtils.HAS, countPlayers());
                    Log.i(TAG, new String(json.toString().getBytes()));
                    out.write(json.toString().getBytes());
                    out.flush();
                    Thread.sleep(1000);
                    out.close();
                    // in.read();
                    // in.close();
                    socket.close();
                    infoSocket.close();
                }
                catch (Exception e)
                {
                    try
                    {
                        if (infoSocket != null)
                        {
                            infoSocket.close();
                        }
                        if (socket != null)
                        {
                            socket.close();
                        }
                        if (out != null)
                        {
                            out.close();
                        }
                        infoSocket = null;
                        socket = null;
                        out = null;
                    }
                    catch (Exception ex)
                    {
                        // ignore
                    }
                }
            }
        }

        private int countPlayers()
        {
            int res = 0;
            for (Connection c: players)
            {
                if (c != null)
                {
                    res++;
                }
            }
            return res;
        }
    };

    String oldName;

    /**
     * Listen for registering clients
     */
    private Runnable btScan = new Runnable()
    {
        public void run()
        {
            oldName = adapter.getName();
            adapter.setName(gameName);
            UUID uuid;
            int myInt = 0;
            while (!available.isEmpty())
            {
                synchronized (available)
                {
                    i = available.poll();
                    uuid = ConnectionUtils.uuid;
                    myInt = i;
                    i++;
                }
                try
                {
                    Log.i(TAG, "waiting for connection with " + myInt);
                    serverSocket = adapter.listenUsingRfcommWithServiceRecord(uuid.toString(), uuid);
                    synchronized (players)
                    {
                        players[myInt] = new BluetoothConnection(serverSocket.accept(), meFromThread, myInt);// ConnectionUtils.SERVERSOCKET_TIMEOUT));
                    }
                    serverSocket.close();
                    sendPlayerNames(myInt);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            scanning = false;
        }
    };

    /**
     * send the names / colors of all registered players
     * 
     * @param p
     *        the player to inform
     */
    private void sendPlayerNames(int p)
    {
        synchronized (players[p])
        {
            for (Connection s: players)
            {
                if (s != null && !s.equals(players[p]))
                {
                    Log.i(TAG, s.getPlayer());
                    send(p, makePlayerJSON(s.getPlayer(), s.getColor(), s.number, true));
                }
            }
        }
    }

    /**
     * make a json representing a registered player
     * 
     * @param name
     *        the name of the player
     * @param color
     *        his color choice
     * @param index
     *        the index of the player (i.e. the start position)
     * @param add
     *        boolean indicating whether the player joins or leaves
     * @return json representation of the player
     */
    private JSONObject makePlayerJSON(String name, String color, int index, boolean add)
    {
        JSONObject json = new JSONObject();
        Log.i(TAG, "makePlayerJSON " + name + " with color " + color);
        try
        {
            json.put(ConnectionUtils.CMD, ConnectionUtils.PLAYER);
            json.put(ConnectionUtils.ACTION, (add? ConnectionUtils.ADD: ConnectionUtils.REMOVE));
            json.put(ConnectionUtils.VALUE, name);
            json.put(ConnectionUtils.INDEX, index);
            json.put(ConnectionUtils.COLOR, color);
        }
        catch (Exception e)
        {
            Log.e("makePlayerJSON", "error parsing json");
        }
        return json;
    }

    /**
     * register a listener
     * 
     * @param l
     */
    public void registerListener(ConnectionListener l)
    {
        listener.add(l);
    }

    /**
     * Event from the connection
     * 
     * @param who
     *        sender
     * @param what
     *        Object
     */
    public void ConnectionReceiveEvent(final int who, final JSONObject what)
    {
        try
        {
            Log.i(TAG, who + " sent " + what.toString());
            if (what.has(ConnectionUtils.CONNECTION)
                    && what.getString(ConnectionUtils.CONNECTION).equals(ConnectionUtils.INTERN))
            {
                if (mainGameStarted)
                {
                    startRegainedConnection(who);
                    return;
                }
                String cmd = what.getString(ConnectionUtils.CMD);
                if (cmd.equals(ConnectionUtils.PLAYER))
                {
                    Log.i(TAG, "cmd was player");
                    if (what.getString(ConnectionUtils.ACTION).equals(ConnectionUtils.VALUE))
                    {
                        Log.i(TAG, "add player " + who);
                        String name = what.getString(ConnectionUtils.VALUE);
                        String color = what.getString(ConnectionUtils.COLOR);
                        boolean color_ok = sendColorRequest(who, color);
                        if (color_ok)
                        {
                            Log.i(TAG, name + " joined");
                            players[who].setPlayer(name, color);
                            // tell everyone about new player
                            broadcastExcept(makePlayerJSON(name, color, who, true), who);
                        }
                    }
                    else
                    { // action == "choice"
                        Log.i(TAG, "action was choice from " + players[who].getPlayer());
                        int index = what.getInt(ConnectionUtils.INDEX);
                        setPreferedTeam(who, index);
                    }
                }
                else if (cmd.equals(ConnectionUtils.LEAVE))
                {
                    removePlayer(who);
                }
            }
            else
            {
                for (final ConnectionListener l: listener)
                {
                    new Thread(new Runnable()
                    {
                        public void run()
                        {
                            l.ReceiveEvent(who, what);
                        }
                    }).start();
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * JSONObject to inform about validity of user's color choice
     * 
     * @param who
     *        player
     * @param color
     *        his chosen color
     * @return
     */
    private boolean sendColorRequest(int who, String color)
    {
        JSONObject result = new JSONObject();
        JSONArray colors = new JSONArray();
        boolean colorExists = false;
        for (int i = 0; i < players.length; i++)
        {
            if (i != who && players[i] != null)
            {
                colors.put(players[i].getColor());
                if (players[i].getColor().equals(color))
                {
                    colorExists = true;
                }
            }
        }

        try
        {
            if (!colorExists)
            {
                colors = new JSONArray();
                colors.put(color);
                result.put("color", "ok");
            }
            result.put(ConnectionUtils.CMD, ConnectionUtils.COLOR);
            result.put("value", colors);
            send(who, result);
        }
        catch (Exception e)
        {}
        return !colorExists;
    }

    /**
     * clean up when player leaves
     * 
     * @param who
     *        the player who leaves
     */
    private void removePlayer(int who)
    {
        players[who].disconnect();
        broadcastExcept(makePlayerJSON(players[who].getPlayer(), players[who].getColor(), who, false), who);
        players[who] = null;
        synchronized (available)
        {
            available.add(who);
        }
        if (!scanning)
        {
            scanning = true;
            new Thread(btScan).start();
            new Thread(infoThread).start();
        }
        // if choicesMade == 0 then prefTeam is null
        if (choicesMade != 0 && prefTeam[who] != -1)
        {
            choicesMade--;
            prefTeam[who] = -1;
            for (int c = 0; c < prefTeam.length; c++)
            {
                if (prefTeam[c] == who)
                {
                    prefTeam[c] = -1;
                    choicesMade--;
                }
            }
        }

    }

    public void ConnectionEvent(int who, final String what)
    {
        try
        {
            Log.i(TAG, "raw" + what);
            ConnectionReceiveEvent(who, new JSONObject(new JSONTokener(what)));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * here the team building happens
     */
    int[] prefTeam;
    int choicesMade = 0;

    private void setPreferedTeam(int who, int index)
    {
        if (choicesMade == 0)
        {
            prefTeam = new int[numberOfPlayers];
            for (int k = 0; k < numberOfPlayers; k++)
            {
                prefTeam[k] = -1;
            }
        }
        prefTeam[who] = index;
        choicesMade++;
        Log.i(TAG, choicesMade + " choices made");
        if (choicesMade == numberOfPlayers)
        {
            makeTeams();
        }
    }

    private void makeTeams()
    {
        // 1. both decide for same team
        boolean[] teamed = new boolean[numberOfPlayers];
        for (int t = 0; t < numberOfPlayers; t++)
            teamed[t] = false;
        for (int t = 0; t < numberOfPlayers; t++)
        {
            if (prefTeam[prefTeam[t]] == t && !teamed[t] && !teamed[prefTeam[t]])
            {
                teamed[t] = true;
                teamed[prefTeam[t]] = true;
                putInTeam(t, prefTeam[t]);
            }
        }

        // 2. one decides for the team
        for (int t = 0; t < numberOfPlayers; t++)
        {
            if (!teamed[prefTeam[t]] && !teamed[t])
            {
                teamed[t] = true;
                teamed[prefTeam[t]] = true;
                putInTeam(t, prefTeam[t]);
            }
        }

        // 3. rest of players
        for (int t = 0; t < numberOfPlayers; t++)
        {
            for (int tt = 0; tt < numberOfPlayers; tt++)
            {
                if (!teamed[tt] && !teamed[t] && t != tt)
                {
                    teamed[t] = true;
                    teamed[tt] = true;
                    putInTeam(t, tt);
                    // break;
                }
            }
        }
        players = temp;
        for (int i = 0; i < players.length; i++)
        {
            players[i].setNumber(i);
        }
        JSONObject start = new JSONObject();
        try
        {
            start.put(ConnectionUtils.CMD, ConnectionUtils.START);
            start.put(ConnectionUtils.CONNECTION, ConnectionUtils.EXTERN);
            JSONArray jPlayers = new JSONArray();
            JSONArray jColors = new JSONArray();
            for (Connection conn: players)
            {
                jPlayers.put(conn.getPlayer());
                jColors.put(conn.getColor());
            }
            start.put(ConnectionUtils.NAMES, jPlayers);
            start.put(ConnectionUtils.COLORS, jColors);
            mainGameStarted = true;
            ConnectionReceiveEvent(-1, start);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    Connection[] temp = null;
    int lowestTeamPos = 0;

    private void putInTeam(int a, int b)
    {
        if (temp == null)
            temp = new Connection[numberOfPlayers];
        Log.d(TAG, "player " + a + ": " + players[a].getPlayer() + " player " + b + ": " + players[b].getPlayer());
        int otherPos = (lowestTeamPos + numberOfPlayers / 2) % numberOfPlayers;
        Log.i(TAG, lowestTeamPos + " in " + temp.length);
        temp[lowestTeamPos] = players[a];
        temp[otherPos] = players[b];
        send(a, teamJSON(lowestTeamPos, players[a].getColor(), players[b].getPlayer(), players[b].getColor(), otherPos));
        send(b, teamJSON(otherPos, players[a].getColor(), players[b].getPlayer(), players[b].getColor(), lowestTeamPos));
        lowestTeamPos++;
    }

    private JSONObject teamJSON(int player_id, String myColor, String otherPlayerName, String otherPlayerColor,
            int otherPos)
    {
        Log.i(TAG, "sending TeamJSON");
        JSONObject json = new JSONObject();
        try
        {
            json.put(ConnectionUtils.CMD, ConnectionUtils.START);
            json.put(ConnectionUtils.VALUE, otherPlayerName);
            json.put(ConnectionUtils.MY_COLOR, myColor);
            json.put(ConnectionUtils.OTHER_COLOR, otherPlayerColor);
            json.put(ConnectionUtils.MY_INDEX, player_id);
            json.put(ConnectionUtils.OTHER_INDEX, otherPos);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return json;
    }

    public void disconnect()
    {
        adapter.setName(oldName);
        try
        {
            if (serverSocket != null)
            {
                serverSocket.close();
            }
            if (infoSocket != null)
            {
                infoSocket.close();
            }
            for (Connection c: players)
            {
                if (c != null)
                {
                    c.disconnect();
                }
            }
        }
        catch (Exception e)
        {
            // empty
        }

    }

    public void restartLostConnection(int id)
    {
        // 1. close connection
        String color = players[id].getColor();
        String name = players[id].getPlayer();
        players[id].disconnect();
        try
        {
            // 2. start new connection
            synchronized (players)
            {
                serverSocket = adapter.listenUsingRfcommWithServiceRecord(ConnectionUtils.uuid.toString(),
                        ConnectionUtils.uuid);
                players[id] = new BluetoothConnection(serverSocket.accept(), meFromThread, id);
                players[id].setPlayer(name, color);
            }
            serverSocket.close();
            startRegainedConnection(id);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void startRegainedConnection(int id)
    {
        // 3. send start packet
        try
        {
            int otherID = (id + 2) % 4;
            JSONObject json = new JSONObject();
            json.put(ConnectionUtils.CMD, ConnectionUtils.START);
            json.put(ConnectionUtils.VALUE, players[otherID].playerName);
            json.put(ConnectionUtils.MY_COLOR, players[id].getColor());
            json.put(ConnectionUtils.OTHER_COLOR, players[otherID].getColor());
            json.put(ConnectionUtils.MY_INDEX, id);
            json.put(ConnectionUtils.OTHER_INDEX, otherID);
            send(id, json);
            // 4. send all packets
            Thread.sleep(4000); // the MainActivity of the client must be
                                // started
            JSONObject notifyLogic = new JSONObject();
            notifyLogic.put("cmd", "rejoined");
            notifyLogic.put("id", id);
            ConnectionReceiveEvent(id, notifyLogic);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
