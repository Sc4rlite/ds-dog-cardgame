package ch.ethz.inf.vs.a4.signerc.dogserver.communication;

import java.util.UUID;


public class ConnectionUtils
{

    public static UUID uuid = UUID.fromString("4e5d48e0-75df-11e3-981f-0800200c9a66");
    public static UUID scan_uuid = UUID.fromString("dbfab162-68ee-11e4-b116-123b93f75cba");
    public static UUID reconnect_uuid = UUID.fromString("7414d85b-1fdb-4052-95c5-8f24a62a7c19");
    public static String SPLIT_PATTERN = "xxxxxxxxx";

    public static final int SERVERSOCKET_TIMEOUT = 120000;	// wait 2min
    public static final int RECONNECTION_TIMEOUT = 30000;	// client has 3x 0.5min
    protected static final long KEEP_ALIVE = 20000;
    public static final int ACK_TIMEOUT = 3000;

    public static final String CMD = "cmd";
    public static final String CONNECTION = "connection";
    public static final String INTERN = "intern";
    public static final String EXTERN = "extern";
    public static final String GET_NAME = "getName";
    public static final String PLAYER = "player";
    public static final String VALUE = "name";
    public static final String GAME_SIZE = "size";
    public static final String LEAVE = "leave";
    public static final String ACTION = "action";
    public static final String EXCEPTION = "exception";
    public static final String ADD = "add";
    public static final String REMOVE = "remove";
    public static final String INDEX = "index";
    public static final String START = "start";
    public static final String NAMES = "names";
    public static final String SIZE = "size";
    public static final String HAS = "has";
    public static final String COLOR = "color";
    public static final String COLORS = "colors";
    public static final String MY_COLOR = "my_color";
    public static final String OTHER_COLOR = "other_color";
    public static final String MY_INDEX = "my_index";
    public static final String OTHER_INDEX = "other_index";
    public static final String CHOICE = "choice";

}
