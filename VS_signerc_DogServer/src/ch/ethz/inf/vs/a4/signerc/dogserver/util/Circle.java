package ch.ethz.inf.vs.a4.signerc.dogserver.util;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;



/**
 * Class that represents a Circle of Objects connected by double links
 * 
 * @author durrera and Sphinx
 *
 * @param <T>
 *            Generic type of this Circle
 */
public class Circle<T> implements Iterable<T>
{
    /**
     * Start of Circle.
     */
    private CircleElement<T> start;
    
    /**
     * Current Element in internal iterator
     */
    private CircleElement<T> current;
    
    /**
     * Size of Circle, number of elements.
     */
    public int size;

    /**
     * Constructor
     */
    public Circle()
    {
        size = 0;
        start = null;
        current = null;
    }

    /**
     * Adds given object to the end of the Circle
     * @param object
     * Object that is to be added to this Circle
     * @return Boolean whether adding was successful
     */
    public boolean add(T object)
    {
        if (isEmpty())
        {
            start = new CircleElement<T>(object, null, null);
            start.next = start;
            start.previous = start;
        }
        else
        {
            start.previous.next = new CircleElement<T>(object, start, start.previous);
            start.previous = start.previous.next;
        }
        current = start;
        size++;
        return true;
    }

    /**
     * Adds all Elements in Collection to Circle
     * @param arg0
     * Collection of Elements that are to be added
     * @return Boolean whether adding was successful
     */
    public boolean addAll(Collection<? extends T> arg0)
    {
        for (T obj: arg0)
            add(obj);
        return true;
    }

    /**
     * Clears Circle and removes all Elements
     */
    public void clear()
    {
        start = null;
        size = 0;
    }

    /**
     * Determines whether this Circle contains given object
     * @param object
     * Object that is checked
     * @return Boolean whether object is contained
     */
    public boolean contains(Object object)
    {
        if (isEmpty())
            return false;
        else
        {
            CircleElement<T> recent = start;
            do
            {
                if (recent.equals(object))
                    return true;
                recent = recent.next;
            } while (!recent.next.equals(start));
        }
        return false;
    }

    /**
     * Determines whether all Elements in this Collection are present in this
     * Circle
     * 
     * @param arg0
     *            Collection that is checked
     * @return Boolean whether all Elements in Collection are contained in this Circle
     */
    public boolean containsAll(Collection<?> arg0)
    {
        boolean res = true;
        for (Object o: arg0)
            res &= contains(o);
        return res;
    }

    /**
     * Determines whether this Circle has no Elements
     * 
     * @return
     */
    public boolean isEmpty()
    {
        return start == null;
    }

    /**
     * Removes the given Object from the Circle
     * 
     * @param object
     *            That is removed
     * @return Boolean whether removing was successful.
     */
    public boolean remove(Object object)
    {
        if (isEmpty())
            return false;
        else
        {
            CircleElement<T> recent = start;
            do
            {
                if (recent.equals(object))
                {
                    recent.previous.next = recent.next;
                    recent.next.previous = recent.previous;
                    size--;
                    return true;
                }
                recent = recent.next;
            } while (!recent.next.equals(start));
        }
        return false;
    }

    /**
     * Removes all Elements from given Collection from the Circle
     * 
     * @param arg0
     *            Collection that will be removed
     * @return whether removing was successful
     */
    public boolean removeAll(Collection<?> arg0)
    {
        boolean res = true;
        for (Object o: arg0)
            res &= remove(o);
        return res;
    }

    @Override public Iterator<T> iterator()
    {
        return new CircleIterator<T>(this);
    }

    /**
     * Start of Circle is advanced for one step (second Object is now the first
     * one)
     */
    public void advance()
    {
        start = start.next;
    }

    /**
     * Determines whether the internal iterator has more elements
     * 
     * @return Boolean whether there are more Elements in internal iterator.
     *         True means there are.
     */
    public boolean hasMoreElements()
    {
        return !current.equals(start.previous);
    }

    /**
     * get next Element of internal iterator
     * 
     * @return next Element in internal iterator
     */
    public T nextElement()
    {
        current = current.next;
        return current.element;
    }

    /**
     * set current Element to start
     */
    public void gotoStart()
    {
        current = start;
    }

    /**
     * Getter for current CircleElement
     * 
     * @return the current element in Circle
     */
    public T getCurrentElement()
    {
        if (current != null)
            return current.element;
        else
            return null;
    }

    /**
     * Class that represents one Element of a Circle
     * 
     * @author Paige and Sphinx
     *
     * @param <T>
     *            Type of CircleElement
     */
    private class CircleElement<T>
    {
        public T element;
        public CircleElement<T> next;
        public CircleElement<T> previous;

        /**
         * Constructor
         * 
         * @param elem
         *            Element of type T
         * @param n
         *            next element in Circle
         * @param prev
         *            previous element in Circle
         */
        public CircleElement(T elem, CircleElement<T> n, CircleElement<T> prev)
        {
            element = elem;
            next = n;
            previous = prev;
        }
    }

}
