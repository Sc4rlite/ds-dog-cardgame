package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Tuple;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.Color;


public class PlayerEdge
{

    private static final String TAG = PlayerEdge.class.getSimpleName();

    private static final float radius = 2.3f;
    private static final float diag = (float)Math.sqrt(4.0 * 4.0 / 2.0);
    private static final float scale = 45f / (6f + diag);
    private static final Paint black = new Paint();
    private static final Paint white = new Paint();
    private static final Tuple<Float, Float>[] coords = (Tuple<Float, Float>[])new Tuple[10];

    private static final int PANELS_PER_EDGE = 4;
    private static final int INDEX_OF_FIRST_HOME_PANEL = 0;
    private static final int INDEX_OF_LAST_HOME_PANEL = 1;
    private static final int INDEX_OF_START_PANEL = 2;
    private static final int INDEX_OF_FIRST_COMMON_CORNER = 3; // Must follow start panel for inference
    private static final int INDEX_OF_LAST_COMMON_CORNER = 6;
    private static final int INDEX_OF_FIRST_PARKING_CORNER = 7;
    private static final int INDEX_OF_LAST_PARKING_CORNER = 9;

    static
    {
        black.setColor(android.graphics.Color.BLACK);
        black.setStrokeWidth(3f);
        white.setColor(android.graphics.Color.WHITE);
        // Home fields
        coords[0] = new Tuple<Float, Float>(scale * -4f, scale * -(6f + diag));
        coords[1] = new Tuple<Float, Float>(scale * -(4f + diag), scale * -(6f));
        // Start field
        coords[2] = new Tuple<Float, Float>(scale * -2f, scale * -(6f + diag));
        // Normal fields (just corners)
        coords[3] = new Tuple<Float, Float>(scale * 2f, scale * -(6f + diag));
        coords[4] = new Tuple<Float, Float>(scale * 2f, scale * -(2f + diag));
        coords[5] = new Tuple<Float, Float>(scale * (2f + diag), scale * -2f);
        coords[6] = new Tuple<Float, Float>(scale * (6f + diag), scale * -2f);
        // Parking fields (3rd one implicit)
        coords[7] = new Tuple<Float, Float>(scale * -diag / 4f, scale * -(4f + diag + diag / 4f));
        coords[8] = new Tuple<Float, Float>(scale * 0f, scale * -(4f + diag));
        coords[9] = new Tuple<Float, Float>(scale * 0f, scale * -(2f + diag));
    }

    private final String name;
    private final Panel[] panels = new Panel[Util.NOF_FIELDS_PER_PLAYER + 3];
    private final Token[] tokens = new Token[4];
    private final Paint paint;
    private final Transform transform;

    public PlayerEdge(String name, Color color, Transform transform)
    {
        this.name = name;
        this.transform = transform;
        paint = new Paint();
        paint.setColor(color.hexLight);
        paint.setTextSize(transform.scale(4f));
        paint.setTextAlign(Align.RIGHT);

        // Calculate radius
        float scaledRadius = transform.scale(radius);

        // Set home panel
        // First parking field is special because of its index
        panels[Util.INDEX_OF_HOME_FIELD] = new Panel(paint, transform.transform(coords[INDEX_OF_FIRST_HOME_PANEL]),
                scaledRadius);
        int p = Util.NOF_FIELDS_PER_PLAYER;
        float n = (coords[INDEX_OF_LAST_HOME_PANEL].left - coords[INDEX_OF_FIRST_HOME_PANEL].left) / 4f;
        float m = (coords[INDEX_OF_LAST_HOME_PANEL].right - coords[INDEX_OF_FIRST_HOME_PANEL].right) / 4f;
        float x = coords[INDEX_OF_FIRST_HOME_PANEL].left + n;
        float y = coords[INDEX_OF_FIRST_HOME_PANEL].right + m;
        for (int j = 0; j < 3; j++)
        {
            panels[p++] = new Panel(paint, transform.transform(new Tuple<Float, Float>(x, y)), scaledRadius);
            x += n;
            y += m;
        }
        // Set home panel
        panels[Util.INDEX_OF_HOME_FIELD] = new Panel(paint, transform.transform(coords[INDEX_OF_FIRST_HOME_PANEL]),
                scaledRadius);
        // Set start panel
        panels[Util.INDEX_OF_START_FIELD] = new Panel(paint, transform.transform(coords[INDEX_OF_START_PANEL]),
                scaledRadius);

        // Set normal panels
        p = Util.INDEX_OF_FIRST_COMMON_FIELD;
        for (int i = INDEX_OF_START_PANEL; i < INDEX_OF_LAST_COMMON_CORNER; i++)
        {
            // Calculate transitions
            n = (coords[i + 1].left - coords[i].left) / PANELS_PER_EDGE;
            m = (coords[i + 1].right - coords[i].right) / PANELS_PER_EDGE;
            x = coords[i].left;
            y = coords[i].right;
            for (int j = 0; j < PANELS_PER_EDGE; j++)
            {
                // Start panel already set - skip it
                if (i != INDEX_OF_START_PANEL || j != 0)
                    panels[p++] = new Panel(white, transform.transform(new Tuple<Float, Float>(x, y)), scaledRadius);
                x += n;
                y += m;
            }
        }

        // Set parking fields
        // First parking field is special because of its position
        p = Util.INDEX_OF_FIRST_PARKING_FIELD;
        panels[p++] = new Panel(paint, transform.transform(coords[INDEX_OF_FIRST_PARKING_CORNER]), scaledRadius);
        n = (coords[INDEX_OF_LAST_PARKING_CORNER].left - coords[INDEX_OF_FIRST_PARKING_CORNER + 1].left) / 2f;
        m = (coords[INDEX_OF_LAST_PARKING_CORNER].right - coords[INDEX_OF_FIRST_PARKING_CORNER + 1].right) / 2f;
        x = coords[INDEX_OF_FIRST_PARKING_CORNER + 1].left;
        y = coords[INDEX_OF_FIRST_PARKING_CORNER + 1].right;
        for (int j = 0; j < 3; j++)
        {
            panels[p++] = new Panel(paint, transform.transform(new Tuple<Float, Float>(x, y)), scaledRadius);
            x += n;
            y += m;
        }

        // Set tokens
        tokens[0] = new Token(color, panels[Util.INDEX_OF_HOME_FIELD], panels[Util.INDEX_OF_HOME_FIELD], transform);
        for (int i = 1; i < 4; i++)
        {
            tokens[i] = new Token(color, panels[Util.NOF_FIELDS_PER_PLAYER + i - 1], panels[Util.INDEX_OF_HOME_FIELD], transform);
        }
    }

    public Panel getPanel(int i)
    {
        return panels[i];
    }

    public Token getToken(int i)
    {
        return tokens[i];
    }

    public void draw(Canvas canvas)
    {
        // Display name
        Tuple<Float, Float> namePosition = new Tuple<Float, Float>(0f, 0f);
        Tuple<Float, Float> center = transform.getCenter();
        namePosition.left = transform.scale(coords[INDEX_OF_FIRST_HOME_PANEL].left - 7f) + center.left;
        namePosition.right = transform.scale(coords[INDEX_OF_FIRST_HOME_PANEL].right + 1f) + center.right;
        canvas.save();
        canvas.rotate((float)transform.rotateAngleDegrees(0.0), center.left, center.right);
        canvas.drawText(name, namePosition.left, namePosition.right, paint);
        canvas.restore();

        // Draw lines
        // First home to second home
        drawLineBetween(canvas, panels[Util.INDEX_OF_HOME_FIELD], panels[Util.NOF_FIELDS_PER_PLAYER]);
        // Between other homes
        for (int i = Util.NOF_FIELDS_PER_PLAYER; i < Util.NOF_FIELDS_PER_PLAYER + 2; i++)
            drawLineBetween(canvas, panels[i], panels[i + 1]);
        // Home to start
        drawLineBetween(canvas, panels[Util.INDEX_OF_HOME_FIELD], panels[Util.INDEX_OF_START_FIELD]);
        // Start to parking
        drawLineBetween(canvas, panels[Util.INDEX_OF_START_FIELD], panels[Util.INDEX_OF_FIRST_PARKING_FIELD]);
        // Between parking
        for (int i = Util.INDEX_OF_FIRST_PARKING_FIELD; i < Util.INDEX_OF_LAST_PARKING_FIELD; i++)
            drawLineBetween(canvas, panels[i], panels[i + 1]);
        // Start to common
        drawLineBetween(canvas, panels[Util.INDEX_OF_START_FIELD], panels[Util.INDEX_OF_FIRST_COMMON_FIELD]);
        // Between common
        for (int i = Util.INDEX_OF_FIRST_COMMON_FIELD; i < Util.INDEX_OF_LAST_COMMON_FIELD; i++)
            drawLineBetween(canvas, panels[i], panels[i + 1]);
        // Infer last common field to next players start field
        float fromX = panels[Util.INDEX_OF_LAST_COMMON_FIELD].getCoords().left;
        float fromY = panels[Util.INDEX_OF_LAST_COMMON_FIELD].getCoords().right;
        float toX = panels[Util.INDEX_OF_LAST_COMMON_FIELD - 1].getCoords().left; // previous
        float toY = panels[Util.INDEX_OF_LAST_COMMON_FIELD - 1].getCoords().right;
        toX = fromX + (fromX - toX); // Walk difference
        toY = fromY + (fromY - toY);
        canvas.drawLine(fromX, fromY, toX, toY, black);

        // Draw panels
        for (Panel panel: panels)
            panel.draw(canvas);
    }

    public void drawStartField(Canvas canvas)
    {
        panels[Util.INDEX_OF_START_FIELD].draw(canvas);
    }

    public void drawTokens(Canvas canvas)
    {
        // Draw tokens
        for (Token token: tokens)
            token.draw(canvas);
    }

    private void drawLineBetween(Canvas canvas, Panel from, Panel to)
    {
        float fromX = from.getCoords().left;
        float fromY = from.getCoords().right;
        float toX = to.getCoords().left;
        float toY = to.getCoords().right;
        canvas.drawLine(fromX, fromY, toX, toY, black);
    }
}
