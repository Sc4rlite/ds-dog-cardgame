package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


public abstract class GameSendEvent
{
    /**
     * ID of recipient of message
     */
    protected int receiver;

    /**
     * JSONObject of this message
     */
    protected JSONObject json;

    /**
     * Getter for JSONObject associated with this GameSendEvent
     * 
     * @return Associated JSONObject
     */
    public JSONObject getJson()
    {
        return json;
    }

    /**
     * Getter for receiver of the message that is contained in this
     * GameSendEvent
     * 
     * @return Integer representing the receiver of this message
     */
    public int getReceiver()
    {
        return receiver;
    }

    /**
     * Constructor that builds JSONObject
     */
    public GameSendEvent()
    {}

    /**
     * Constructor
     * 
     * @param recv
     *        Receiver of this GameSendEvent
     */
    public GameSendEvent(int recv)
    {
        receiver = recv;
        builtJson();
    }

    /**
     * Builds JSON object from the current state of message
     */
    protected abstract void builtJson();

    //_______________________________________________________________________

    public static class TurnSendEvent extends GameSendEvent
    {

        /**
         * Determines whether a turn is accepted or not, so whether the Player
         * has play options or not. True means turn is valid and Player has
         * options.
         */
        private boolean accept;

        /**
         * Constructor
         * 
         * @param recv
         *        Receiver of this SendEvent
         * @param acc
         *        Boolean that determines whether a Player has valid play
         *        options or not. True means the Player has options.
         */
        public TurnSendEvent(int recv, boolean acc)
        {
            receiver = recv;
            accept = acc;
            builtJson();
        }

        @Override protected void builtJson()
        {
            json = new JSONObject();
            try
            {
                json.put("cmd", "turn");
                json.put("accept", accept);
            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

    public static class DistributeCardsSendEvent extends GameSendEvent
    {

        /**
         * List of Cards that are distributed to receiver bin this Event
         */
        private List<Card> cards;

        private boolean swap;

        /**
         * Constructor
         * 
         * @param recv
         *        Receiver of this SendEvent
         * @param myCards
         *        List of Cards that are distributed to receiver
         */
        public DistributeCardsSendEvent(int recv, List<Card> myCards, boolean swapping)
        {
            assert (myCards != null);
            cards = myCards;
            receiver = recv;
            swap = swapping;
            builtJson();
        }

        @Override protected void builtJson()
        {
            json = new JSONObject();
            try
            {
                json.put("cmd", "distributeCards");
                JSONObject jsonHash = new JSONObject();
                for (Card card: cards)
                    jsonHash.put(String.valueOf(card.ID), card.cardType.toInt());
                json.put("cards", jsonHash);
                json.put("swap", swap);
            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static class RejectCardSendEvent extends GameSendEvent
    {
        //        public RejectCardSendEvent()
        //        {
        //            builtJson();
        //        }

        public RejectCardSendEvent(int iD)
        {
            super(iD);
        }

        @Override protected void builtJson()
        {
            json = new JSONObject();
            try
            {
                json.put("cmd", "rejectCard");
            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static class FinishedSendEvent extends GameSendEvent
    {
        /**
         * Player that has won the Game
         */
        private Player winner;

        /**
         * Constructor
         * 
         * @param recv
         *        Receiver of this SendEvent
         * @param winningPlayer
         *        Player that has won the game
         */
        public FinishedSendEvent(int recv, Player winningPlayer)
        {
            receiver = recv;
            winner = winningPlayer;
            builtJson();
        }

        @Override protected void builtJson()
        {
            json = new JSONObject();
            try
            {
                json.put("cmd", "finished");
                JSONObject jsonHash = new JSONObject();
                jsonHash.put("player1", winner.name);
                jsonHash.put("player2", winner.teamMember.name);
                json.put("players", jsonHash);
            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
