package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import ch.ethz.inf.vs.a4.signerc.dogserver.MainActivity;
import ch.ethz.inf.vs.a4.signerc.dogserver.R;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Board;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Figure;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Move;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Player;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Tuple;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.CardType;


public class GamePanel extends SurfaceView
{

    private static final String TAG = GamePanel.class.getSimpleName();

    private static final long framerate = 60;
    private static final long delay = 1000 / framerate;
    private static final float relativeCardHeight = 40;
    private static final float cardRatio = 618f / 1000f;

    private final MainActivity context;
    private final CardCache cardCache;
    private final SurfaceHolder holder;
    private final int width;
    private final int height;
    private final int cardWidth;
    private final int cardHeight;
    private final float clickTolerance;
    private Transform[] transforms = null;
    private PlayerEdge[] playerEdges = null;
    private CardView lastCard = null;
    private CardView card = null;
    private Bitmap background;
    private Matrix matrix = new Matrix();
    private Paint paint = new Paint();

    private final Set<Animation> animations = new LinkedHashSet<Animation>();
    private final Thread animationThread;
    private boolean running = true;

    public GamePanel(MainActivity context)
    {
        super(context);
        this.context = context;

        // VIP: Very important program-line
        setWillNotDraw(false);

        // Get screen size
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        windowManager.getDefaultDisplay().getSize(size);

        // Set width as longer edge
        if (size.x > size.y)
        {
            width = size.x;
            height = size.y;
        }
        else
        {
            width = size.y;
            height = size.x;
        }

        Transform transform = new Transform(height, (float)width / 2f, (float)height / 2f, 0.0);
        clickTolerance = transform.scale(10f);

        // Load background
        background = BitmapFactory.decodeResource(context.getResources(), R.drawable.wood);

        // Load cards
        cardHeight = (int)transform.scale(relativeCardHeight);
        cardWidth = (int)(cardRatio * cardHeight);
        cardCache = new CardCache(context, 1000, 1000);

        // Set holder and callback
        holder = getHolder();
        holder.addCallback(new SurfaceHolder.Callback()
        {
            @Override public void surfaceDestroyed(SurfaceHolder holder)
            {
                // Do nothing
            }

            @Override public void surfaceCreated(SurfaceHolder holder)
            {
                // Not exactly sure if needed
                postInvalidate();
            }

            @Override public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
            {
                // Do nothing
            }
        });

        // Define animation thread
        animationThread = new Thread(new Runnable()
        {
            @Override public void run()
            {
                long before, diff, sleep;
                before = System.currentTimeMillis();

                while (running)
                {
                    diff = System.currentTimeMillis() - before;
                    before = System.currentTimeMillis();

                    boolean change = false;
                    synchronized (animations)
                    {
                        for (Animation animation: animations)
                            if (animation.update(diff))
                                change = true;
                    }
                    if (change)
                        postInvalidate();

                    sleep = System.currentTimeMillis() + delay - before;
                    if (sleep < 0)
                        sleep = 0;

                    // Wait remaining time
                    try
                    {
                        Thread.sleep(sleep);
                    }
                    catch (InterruptedException e)
                    {
                        Log.i(TAG, "Interrupted: " + e.getMessage());
                    }
                }
            }
        });
        animationThread.start();
    }

    public void setPlayers(Player[] players, Board board)
    {
        // Create players
        playerEdges = new PlayerEdge[players.length];
        transforms = new Transform[players.length];
        for (Player p: players)
        {
            transforms[p.ID] = new Transform(height, (float)width / 2f, (float)height / 2f, p.ID * Math.PI / 2.0);
            playerEdges[p.ID] = new PlayerEdge(p.name, p.getColour(), transforms[p.ID]);
        }

        synchronized (animations)
        {
            // Add panels to animation list
            for (int i = 0; i < playerEdges.length; i++)
                for (int j = 0; j < Util.NOF_FIELDS_PER_PLAYER + 3; j++)
                    animations.add(playerEdges[i].getPanel(j));

            // Add tokens to animation list
            for (int i = 0; i < playerEdges.length; i++)
                for (int j = 0; j < 4; j++)
                    animations.add(playerEdges[i].getToken(j));
        }

        //------
        for (Entry<Figure, Field> entry: board.positions.entrySet())
            castFigure(entry.getKey()).startMove(castField(entry.getValue()));
        //------

        postInvalidate();
    }

    //-------------------------------------------------------------------------

    private Token castFigure(Figure figure)
    {
        return playerEdges[figure.owner.ID].getToken(figure.ID);
    }

    private Panel castField(Field field)
    {
        return playerEdges[field.playerID].getPanel(field.fieldID);
    }

    private Set<Touchable> touchables = new LinkedHashSet<Touchable>();
    private boolean waitForClick = false;
    private Touchable lastClick;

    public void fakePlay(Player active, CardType type)
    {
        synchronized (touchables)
        {
            if (card != null)
            {
                synchronized (animations)
                {
                    if (lastCard != null)
                        animations.remove(lastCard);
                }
                lastCard = card;
            }
            card = new CardView(context, cardCache, type, cardHeight, cardWidth);
            Tuple<Float, Float> startPoint = transforms[active.ID].transform(new Tuple<Float, Float>(-50f, -50f));
            card.setX(startPoint.left);
            card.setY(startPoint.right);
            card.setAngle(135f);
            synchronized (animations)
            {
                animations.add(card);
            }

            // Move to center
            card.startMove(width / 2f, height / 2f);
            card.startRotate(45f);

            // Wait
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            // Move back
            card.startMove(startPoint.left, startPoint.right);
            card.startRotate(135f);

            // Wait again
            try
            {
                Thread.sleep(500);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            // Disappear
            card = null;
            postInvalidate();
        }
    }

    public Move chooseMove(Player active, CardType type, List<Move> moves)
    {
        Move chosenMove = null;

        synchronized (touchables)
        {
            if (card != null)
            {
                if (lastCard != null)
                    synchronized (animations)
                    {
                        animations.remove(lastCard);
                    }
                lastCard = card;
            }
            card = new CardView(context, cardCache, type, cardHeight, cardWidth);
            Tuple<Float, Float> startPoint = transforms[active.ID].transform(new Tuple<Float, Float>(-50f, -50f));
            card.setX(startPoint.left);
            card.setY(startPoint.right);
            card.setAngle(135f);
            synchronized (animations)
            {
                animations.add(card);
            }
            card.startMove(width / 2f, height / 2f);
            card.startRotate(45f);

            int i = 0;

            // Let the player chose tokens and fields until move is distinct
            do
            {

                // Filter all tokens of ith position
                touchables.clear();
                for (Move move: moves)
                    touchables.add(castFigure(move.movedFigures().get(i)));

                // Wait for input
                waitForInput(); //---------------------------------------------

                Token chosenToken = (Token)lastClick;
                // Filter all possible fields
                touchables.clear();
                // Get own figure
                Figure chosenFigure = null;
                for (Move move: moves)
                {
                    for (Figure figure: move.movedFigures())
                        if (castFigure(figure).equals(chosenToken))
                        {
                            chosenFigure = figure;
                            break;
                        }
                    if (chosenFigure != null)
                        break;
                }
                Iterator<Move> iterator = moves.iterator();
                while (iterator.hasNext())
                {
                    // Test move
                    Move move = iterator.next();

                    // Check if chosen token is included
                    if (move.movedFigures().get(i).equals(chosenFigure))
                        // Add the field on which the figure would land
                        touchables.add(castField(move.move(chosenFigure)));
                    else
                        // Remove move from copy to reduce iteration
                        iterator.remove();
                }

                // Wait for input
                waitForInput(); //---------------------------------------------

                Panel chosenPanel = (Panel)lastClick;
                // Perform action of chosen token (others will follow)
                chosenToken.startMove(chosenPanel);
                // Remove all moves that don't match
                iterator = moves.iterator();
                while (iterator.hasNext())
                {
                    Move move = iterator.next();
                    if (!castField(move.move(chosenFigure)).equals(chosenPanel))
                        iterator.remove();
                }
                i++;
            } while (moves.size() > 1);

            // Perform all remaining actions
            chosenMove = moves.get(0);
            for (Figure figure: chosenMove.movedFigures())
                castFigure(figure).startMove(castField(chosenMove.move(figure)));
        }

        // Sleep 1 second for animation
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return chosenMove;
    }

    private void waitForInput()
    {
        // Flash all tokens
        for (Touchable touchable: touchables)
            touchable.startFlash();

        // Wait for user input
        waitForClick = true;
        try
        {
            touchables.wait();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        waitForClick = false;

        // Unflash all tokens
        for (Touchable touchable: touchables)
            touchable.stopFlash();
    }

    @Override public boolean onTouchEvent(MotionEvent event)
    {
        if (waitForClick && event.getAction() == MotionEvent.ACTION_UP)
            synchronized (touchables)
            {
                // Go through all given touchables and determine the closest
                lastClick = null;
                float minDist = Float.MAX_VALUE;
                for (Touchable touchable: touchables)
                {
                    float dist = touchable.calcDistance(event);
                    if (dist < clickTolerance && dist < minDist)
                    {
                        lastClick = touchable;
                        minDist = dist;
                    }
                }

                // If found a matching touchable, inform move method
                if (lastClick != null)
                    touchables.notify();
            }
        return true;
    }

    //-------------------------------------------------------------------------

    @Override protected void onDraw(Canvas canvas)
    {
        // Draw background
        canvas.drawColor(0xffa52a2a);
        matrix.setScale((float)width / (float)background.getWidth(), (float)height / (float)background.getHeight());
        canvas.drawBitmap(background, matrix, paint);

        if (playerEdges != null)
            for (PlayerEdge playerEdge: playerEdges)
                playerEdge.draw(canvas);

        // Draw first players start panel again to overpaint last edge
        if (playerEdges != null && playerEdges[0] != null)
            playerEdges[0].drawStartField(canvas);

        // Draw tokens
        if (playerEdges != null)
            for (PlayerEdge playerEdge: playerEdges)
                playerEdge.drawTokens(canvas);

        // Draw cards
        if (lastCard != null)
            lastCard.draw(canvas);
        if (card != null)
            card.draw(canvas);
    }

    public void showRestart(String message)
    {
        context.showRestart(message);
    }
}
