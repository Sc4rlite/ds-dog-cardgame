package ch.ethz.inf.vs.a4.signerc.dogserver.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import org.json.JSONObject;
import org.json.JSONTokener;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;


public class BluetoothConnection extends Connection
{
    BluetoothSocket socket;
    OutputStream os;
    InputStream is;
    ConnectionManager manager;
    Thread t;
    String TAG = "BluetoothConnection";
    AckManager ackMan;
    BluetoothDevice device;
    boolean running;
    Object lock = new Object();

    public BluetoothConnection(BluetoothSocket s, ConnectionManager m, int p)
    {
        socket = s;
        device = socket.getRemoteDevice();
        manager = m;
        number = p;
        // manager.ConnectionEvent(p, "Player " + p + " joined");
        running = true;
        try
        {
            if (socket != null)
            {
                is = socket.getInputStream();
                os = socket.getOutputStream();
                t = new Thread(receive);
                t.start();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        ackMan = new AckManager();
        TAG = TAG + number;
    }

    public void send(JSONObject json)
    {
        sendRaw(ackMan.add(json).toString().getBytes());
    }

    public void sendRaw(byte[] msg)
    {
        try
        {
            Log.i(TAG, "send " + new String(msg));
            if (ackMan.connected)
            {
                synchronized (lock)
                {
                    os.write(msg);
                    os.write(ConnectionUtils.SPLIT_PATTERN.getBytes());
                    os.flush();
                }
            }
            Thread.yield();
        }
        catch (Exception e)
        {
            ackMan.initReconnect();
            e.printStackTrace();
        }
    }

    Runnable receive = new Runnable()
    {
        public void run()
        {
            int bufferSize = 1024;
            int bytesRead = -1;
            byte[] buffer = new byte[bufferSize];
            // Keep reading the messages while connection is open...
            while (running)
            {
                try
                {
                    bytesRead = is.read(buffer);
                    if (bytesRead != -1)
                    {
                        /*
                         * Possible packets: ACK... (Hashcode) live JSON Object
                         */
                        String result = new String(buffer, 0, bytesRead);
                        String[] res = result.split(ConnectionUtils.SPLIT_PATTERN);
                        for (String r: res)
                        {
                            while (r.contains(ConnectionUtils.SPLIT_PATTERN))
                            {
                                r = r.replace(ConnectionUtils.SPLIT_PATTERN, "");
                                Log.i(TAG, "removed pattern in " + r);
                            }
                            Log.i(TAG, "cleaned up: " + r);
                            if (r.equals(""))
                                break; // last one might be empty
                            if (r.startsWith("ACK"))
                            {
                                ackMan.remove(r.substring(3));
                            }
                            else if (r.equals("live"))
                            {
                                ackMan.live();
                            }
                            else if (ackMan.ack(r))
                            {
                                manager.ConnectionEvent(number, r);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    break;
                }
            }
        }
    };

    boolean initiatedReconnect = false;

    boolean reconnect()
    {
        if (initiatedReconnect || !running)
        {
            return false;
        }
        initiatedReconnect = true;
        // the connection was lost; try to reestablish the connection
        Log.i(TAG, "reconnect with " + number);
        try
        {
            socket.close();
        }
        catch (Exception e)
        { /* probably will throw an exception! */
        }
        for (int i = 0; i < 3 && running; i++)
        {
            try
            {
                UUID uuid = ConnectionUtils.uuid;
                BluetoothServerSocket s = manager.adapter.listenUsingRfcommWithServiceRecord(uuid.toString(), uuid);
                socket = s.accept(ConnectionUtils.RECONNECTION_TIMEOUT);
                initiatedReconnect = false;
                Log.i(TAG, "reconnected successful? " + socket.isConnected());
                if (!socket.getRemoteDevice().getAddress().equals(device.getAddress()))
                {
                    socket.close();
                }
                else
                {
                    is.close();
                    os.close();
                    is = socket.getInputStream();
                    os = socket.getOutputStream();
                    return true;
                }
            }
            catch (Exception e)
            {
                // might throw exception if client not ready
            }
        }
        try
        {
            // Client lost
            JSONObject exception = new JSONObject();
            exception.put(ConnectionUtils.CMD, ConnectionUtils.EXCEPTION);
            manager.ConnectionReceiveEvent(number, exception);
            manager.restartLostConnection(number);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override public void disconnect()
    {
        running = false;
        ackMan.notifyExit();
        t.interrupt();
        if (socket != null)
        {
            try
            {
                is.close(); // this will kill receive
                os.close();
                socket.close();
            }
            catch (Exception e)
            {}
        }
    }

    @Override public boolean discover()
    {
        // unused
        return false;
    }

    @Override public void broadcast(String msg)
    {
        // unused

    }

    @SuppressLint("UseSparseArrays")
    class AckManager
    {

        HashMap<Integer, JSONObject> messages;
        HashMap<Integer, Timeout> timeout;
        HashMap<Integer, JSONObject> acked;
        int counter;
        Thread worker;
        boolean connected;
        long lastLive;

        public AckManager()
        {
            messages = new HashMap<Integer, JSONObject>();
            timeout = new HashMap<Integer, Timeout>();
            acked = new HashMap<Integer, JSONObject>();
            counter = 0;
            connected = true;
            worker = new Thread(ackHandler);
            worker.start();
        }

        public void live()
        {
            lastLive = System.currentTimeMillis();
        }

        public JSONObject add(JSONObject message)
        {
            try
            {
                int id = uniqueInt();
                message.put("uniqueInt", id);
                synchronized (messages)
                {
                    messages.put(id, message);
                    timeout.put(id, new Timeout(System.currentTimeMillis(), 0));
                    messages.notify();
                }
                return message;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void remove(String rawID)
        {
            int id = Integer.parseInt(rawID);
            synchronized (messages)
            {
                messages.remove(id);
                timeout.remove(id);
                messages.notify();
            }
        }

        /**
         * ack the received message
         * 
         * @param message
         *        incoming JSON Object
         * @return returns true if the message was not acked before, false
         *         otherwise
         */
        public boolean ack(String message)
        {
            try
            {
                JSONObject ackable = new JSONObject(new JSONTokener(message));
                if (!ackable.has("uniqueInt"))
                    return true;
                int key = ackable.getInt("uniqueInt");
                sendRaw(("ACK" + key).getBytes());
                if (acked.put(key, ackable) != null)
                {
                    // Already acked
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                synchronized (messages)
                {
                    messages.notify();
                }
            }
        }

        Runnable ackHandler = new Runnable()
        {
            public void run()
            {
                synchronized (messages)
                {
                    try
                    {
                        messages.wait(ConnectionUtils.KEEP_ALIVE);
                    }
                    catch (InterruptedException e1)
                    {}
                    lastLive = System.currentTimeMillis();
                    while (running)
                    {
                        long sleep = ConnectionUtils.KEEP_ALIVE;
                        long now = System.currentTimeMillis();
                        if (Math.abs(now - lastLive) > 3 * ConnectionUtils.KEEP_ALIVE)
                        {
                            initReconnect();
                        }
                        Set<Integer> keys = timeout.keySet();
                        for (int key: keys)
                        {
                            long waiting = (timeout.get(key).timeout + ConnectionUtils.ACK_TIMEOUT) - now;
                            if (waiting < 100)
                            {
                                resend(key);
                            }
                            else
                            {
                                sleep = Math.min(sleep, waiting);
                            }
                        }
                        if (connected && messages.isEmpty())
                        {
                            sendRaw((ConnectionUtils.SPLIT_PATTERN + "live" + ConnectionUtils.SPLIT_PATTERN).getBytes());
                        }
                        try
                        {
                            messages.wait(sleep);
                        }
                        catch (InterruptedException e)
                        {}
                    }
                }
            }
        };

        void resend(int key)
        {
            // keep old one in the List
            if (connected)
            {
                sendRaw(messages.get(key).toString().getBytes());
            }
            int age = timeout.get(key).age;
            if (age < 3)
            {
                timeout.put(key, new Timeout(System.currentTimeMillis(), age + 1));
            }
            else
            {
                initReconnect();
            }
        }

        void initReconnect()
        {
            connected = false;
            if (reconnect())
            {
                t = new Thread(receive);
                t.start();
                connected = true;
            }
            // otherwise an exception is thrown in "reconnect()"
        }

        private void notifyExit()
        {
            synchronized (messages)
            {
                messages.notify();
            }
        }

        int uniqueInt()
        {
            counter++;
            return counter;
        }

        class Timeout
        {
            public int age;
            public long timeout;

            public Timeout(long timeout, int age)
            {
                this.timeout = timeout;
                this.age = age;
            }
        }
    }
}
