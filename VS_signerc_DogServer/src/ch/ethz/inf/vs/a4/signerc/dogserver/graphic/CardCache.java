package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.CardType;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;


public class CardCache extends LruCache<CardType, Bitmap>
{

    private static final String TAG = CardCache.class.getSimpleName();

    private static final int maxMemory = (int)(Runtime.getRuntime().maxMemory() / 1024);
    private static final int cacheSize = maxMemory / 8;

    public CardCache(Context context, int reqHeight, int reqWidth)
    {
        super(cacheSize);

        // Load images
        for (CardType type: CardType.values())
            put(type, decodeSampledBitmapFromResource(context.getResources(), type.res, reqWidth, reqHeight));
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
    {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth)
        {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
            {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight)
    {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

}
