package ch.ethz.inf.vs.a4.signerc.dogserver.communication;

import org.json.JSONObject;


public abstract class Connection
{
    protected int number;
    protected ConnectionListener listener;
    protected String playerName = "";
    protected String color = "";

    public Connection()
    {
        number = 0;
    }

    public void setNumber(int i)
    {
        this.number = i;
    }

    public abstract boolean discover();

    public abstract void send(JSONObject msg);

    protected Runnable receive;

    public abstract void broadcast(String msg);	// TODO remove

    public abstract void disconnect();

    public String getPlayer()
    {
        return playerName;
    }

    public String getColor()
    {
        return color;
    }

    public void setPlayer(String s, String color)
    {
        playerName = s;
        this.color = color;
    }

}
