package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.CardType;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.Color;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.GameReceiveEventType;


public abstract class GameReceiveEvent
{
    protected int sender;
    protected JSONObject json;
    protected GameReceiveEventType eventType;

    public GameReceiveEvent(JSONObject jsonObject, int senderNr)
    {
        json = jsonObject;
        sender = senderNr;
        parseJson();
    }

    protected abstract void parseJson();

    //________________________________________________________________________________________

    public static class GameReceiveEventStart extends GameReceiveEvent
    {
        public String[] names;
        public Color[] colours;

        public GameReceiveEventStart(JSONObject jsonObject, int senderNr)
        {
            super(jsonObject, senderNr);
            eventType = Util.GameReceiveEventType.GAME_START;
        }

        @Override protected void parseJson()
        {
            // TODO Implement parseJSON       
            try
            {
                assert (json.getString("cmd").equals("start"));
                JSONArray jArray = json.getJSONArray("names");
                names = new String[jArray.length()];
                for (int i = 0; i < jArray.length(); i++)
                    names[i] = jArray.getString(i);
                jArray = json.getJSONArray("colors");
                colours = new Color[jArray.length()];
                for (int i = 0; i < jArray.length(); i++)
                    colours[i] = Color.toColour(jArray.getInt(i));

            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static class GameReceiveEventReceiveCard extends GameReceiveEvent
    {
        private int cardID;
        private int type;

        public GameReceiveEventReceiveCard(JSONObject jsonObject, int senderNr)
        {
            super(jsonObject, senderNr);
            eventType = Util.GameReceiveEventType.RECEIVE_CARD;
        }

        public int getCardID()
        {
            return cardID;
        }

        @Override protected void parseJson()
        {
            // TODO Implement parseJSON 
            try
            {
                assert (json.getString("cmd").equals("playCard"));
                cardID = json.getInt("id");
                type = json.getInt("type");
            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public Card getCardOfType()
        {
            return Util.getCardofType(cardID, type);
        }
    }

    public static class GameReceiveEventReceiveSwitchCard extends GameReceiveEvent
    {
        private int cardID;

        public GameReceiveEventReceiveSwitchCard(JSONObject jsonObject, int senderNr)
        {
            super(jsonObject, senderNr);
            eventType = Util.GameReceiveEventType.RECEIVE_SWITCH_CARD;
        }

        public int getCardID()
        {
            return cardID;
        }

        @Override protected void parseJson()
        {
            // TODO Implement parseJSON     
            try
            {
                assert (json.getString("cmd").equals("switchCard"));
                cardID = json.getInt("id");
            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static class GameReceiveEventReady extends GameReceiveEvent
    {
        public GameReceiveEventReady(JSONObject jsonObject, int senderNr)
        {
            super(jsonObject, senderNr);
            eventType = Util.GameReceiveEventType.READY;
        }

        @Override protected void parseJson()
        {
            // TODO Implement parseJSON     
            try
            {
                assert (json.getString("cmd").equals("ready"));
            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static class GameReceiveEventRejoined extends GameReceiveEvent
    {
        private int ID;

        public GameReceiveEventRejoined(JSONObject jsonObject, int senderNr)
        {
            super(jsonObject, senderNr);
            eventType = Util.GameReceiveEventType.REJOINED;
        }

        @Override protected void parseJson()
        {
            // TODO Implement parseJSON     
            try
            {
                assert (json.getString("cmd").equals("rejoined"));
                ID = json.getInt("id");

            }
            catch (JSONException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public int getID()
        {
            return ID;
        }
    }
}
