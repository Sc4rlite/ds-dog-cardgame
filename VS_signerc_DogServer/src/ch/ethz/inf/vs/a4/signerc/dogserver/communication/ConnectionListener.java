package ch.ethz.inf.vs.a4.signerc.dogserver.communication;

import java.util.EventListener;

import org.json.JSONObject;

import android.os.Handler;


public abstract interface ConnectionListener extends EventListener
{

    public final Handler mCallbackHandler = new Handler();

    public Handler getHandler();

    public void ReceiveEvent(int sender, JSONObject json);

}
