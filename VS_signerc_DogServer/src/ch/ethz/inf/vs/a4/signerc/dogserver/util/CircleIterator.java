package ch.ethz.inf.vs.a4.signerc.dogserver.util;

import java.util.Iterator;

import android.util.Log;


public class CircleIterator<E> implements Iterator<E>
{
    IteratorElement<E> current;

    public CircleIterator(Circle<E> circle)
    {
        if (circle.isEmpty())
            current = null;
        else
        {
            circle.gotoStart();
            current = new IteratorElement<E>(circle.getCurrentElement());
            IteratorElement<E> recent = current;
            while (circle.hasMoreElements())
            {
                circle.nextElement();
                IteratorElement<E> temp = new IteratorElement<E>(circle.getCurrentElement());
                recent.next = temp;
                recent = temp;
            }
        }
    }

    @Override public boolean hasNext()
    {
        return current != null; //&& current.next != null;
    }

    @Override public E next()
    {
        if (current != null)
        {
            E res = current.element;
            current = current.next;
            return res;
        }
        else
            return null;
    }

    @Override public void remove()
    {
        // Do nothing
    }

    private class IteratorElement<E>
    {
        public E element;
        public IteratorElement<E> next;

        public IteratorElement(E elem)
        {
            element = elem;
        }
    }

}
