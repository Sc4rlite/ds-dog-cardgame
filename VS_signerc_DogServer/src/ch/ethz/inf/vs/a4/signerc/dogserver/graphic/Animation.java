package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

public interface Animation
{

    public boolean update(long diff);

}
