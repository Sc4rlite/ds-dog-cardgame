package ch.ethz.inf.vs.a4.signerc.dogserver.graphic;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.Color;


public class Token implements Touchable, Animation
{

    private static final String TAG = Token.class.getSimpleName();

    private static final float radius = 1.5f;
    private static final Paint black = new Paint();

    static
    {
        black.setColor(android.graphics.Color.BLACK);
    }

    private final Panel home;
    private final Panel formalHome; // Game logic treats home different
    private Paint paint;
    private final float scaledRadius;
    private float x;
    private float y;

    public Token(Color color, Panel home, Panel formalHome, Transform transform)
    {
        this.home = home;
        this.formalHome = formalHome;
        paint = new Paint();
        paint.setColor(color.hex);
        x = home.getCoords().left;
        y = home.getCoords().right;
        scaledRadius = transform.scale(radius);
    }

    public void draw(Canvas canvas)
    {
        canvas.drawCircle(x, y, scaledRadius, black);
        canvas.drawCircle(x, y, scaledRadius * 0.8f, paint);
    }

    //=========================================================================

    @Override public float calcDistance(MotionEvent event)
    {
        float dx = event.getRawX() - x;
        float dy = event.getRawY() - y;
        return (float)Math.sqrt(dx * dx + dy * dy);
    }

    //=========================================================================

    @Override public boolean update(long diff)
    {
        boolean result = false;
        if (updateMove(diff))
            result = true;
        if (updateFlash(diff))
            result = true;
        return result;
    }

    //-------------------------------------------------------------------------

    // Move a token to another field
    private static final long moveTime = 500;
    private boolean moveRunning = false;
    private float startX;
    private float startY;
    private float destX;
    private float destY;
    private float stepX;
    private float stepY;

    public void startMove(Panel panel)
    {
        // If sent back to home, choose own home
        if (panel.equals(formalHome))
            panel = home;

        startX = x;
        startY = y;
        destX = panel.getCoords().left;
        destY = panel.getCoords().right;
        stepX = (destX - x) / moveTime;
        stepY = (destY - y) / moveTime;
        moveRunning = true;
    }

    private boolean updateMove(long diff)
    {
        if (moveRunning)
        {
            x += stepX * diff;
            y += stepY * diff;
            if ((startX <= destX && destX <= x || x <= destX && destX <= startX)
                    && (startY <= destY && destY <= y || y <= destY && destY <= startY))
            {
                x = destX;
                y = destY;
                moveRunning = false;
            }
            return true;
        }

        return false;
    }

    //-------------------------------------------------------------------------

    // Flash token
    private static final long flashTime = 500;
    private boolean flashRunning = false;
    private Paint oldPaint;
    private boolean isBlack = false;
    private long flashTimer;

    public void startFlash()
    {
        oldPaint = paint;
        paint = black;
        isBlack = true;
        flashTimer = flashTime;
        flashRunning = true;
    }

    public void stopFlash()
    {
        flashRunning = false;
        paint = oldPaint;
    }

    private boolean updateFlash(long diff)
    {
        if (flashRunning)
        {
            flashTimer -= diff;
            if (flashTimer < 0)
            {
                paint = isBlack? oldPaint: black;
                isBlack = !isBlack;
                flashTimer = flashTime;
            }
            return true;
        }

        return false;
    }

}
