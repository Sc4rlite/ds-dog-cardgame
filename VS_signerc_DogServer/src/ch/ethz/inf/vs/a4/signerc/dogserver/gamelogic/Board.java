package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import java.util.HashMap;
import java.util.Map;

import android.util.Log;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.HomeField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.StartField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.ParkingLotField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.PublicField;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Circle;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util;


/**
 * Class that represents the Board that the Game is played on
 * 
 * @author Paige and Sphinx
 *
 */
public class Board
{
    /**
     * List of all Fields ordered by its Player
     */
    public Field[][] fields;

    /**
     * Number of Players that this Board can accommodate
     */
    protected final int nofPlayer;

    /**
     * Positions of all Figures on this Board
     */
    public Map<Figure, Field> positions;

    /**
     * Game that this Board is used for
     */
    protected final Game game;

    protected boolean isVirtual;

    /**
     * Constructor
     * 
     * @param gameObject
     *        Game that this Board is used for
     * @param Number
     *        of Players on Board
     */
    public Board(Game gameObject, int boardSize)
    {
        game = gameObject;
        nofPlayer = boardSize;
        fields = new Field[nofPlayer][];
        positions = new HashMap<Figure, Field>();
    }

    /**
     * Initialise the Board by creating its Fields and setting Figures in their
     * respective HomeField
     */
    public void initialiseBoard()
    {
        createBoard();
        sendAllFiguresHome();
        //        sendOneToHomeRestParked();
    }

    /**
     * Sends all Figures to their respective HomeField
     */
    protected void sendAllFiguresHome()
    {
        //iterate through all Figures in Game and call comeHome for them
        for (Player player: game.getPlayers())
        {
            HomeField homeField = findHomeField(player);
            for (Figure figure: player.getFigures())
            {
                homeField.comeHome(figure);
                positions.put(figure, homeField);
            }
        }
    }

    protected void sendOneToHomeRestParked()
    {
        //iterate through all Figures in Game and call comeHome for them
        for (Player player: game.getPlayers())
        {
            HomeField homeField = findHomeField(player);
            int i = 0;
            for (Figure figure: player.getFigures())
            {
                if (i == 0)
                {
                    homeField.comeHome(figure);
                    positions.put(figure, homeField);
                    //                    Field field = findSpecificField(player, Util.INDEX_OF_START_FIELD);
                    //                    positions.put(figure, field);
                    //                    field.occupant = figure;
                }
                else
                {
                    Field field = findSpecificField(player, Util.INDEX_OF_FIRST_PARKING_FIELD + i);
                    positions.put(figure, field);
                    field.occupant = figure;
                }
                i++;
            }
        }
    }

    /**
     * Creates all Fields that this Board consists of
     */
    protected void createBoard()
    {
        // Create all Fields of Board
        Circle<Player> players = game.getPlayers();
        for (Player player: players)
        {
            Field[] playerFields = new Field[Util.NOF_FIELDS_PER_PLAYER];
            playerFields[Util.INDEX_OF_HOME_FIELD] = new HomeField(player, Util.INDEX_OF_HOME_FIELD, this);
            playerFields[Util.INDEX_OF_START_FIELD] = new StartField(player, Util.INDEX_OF_START_FIELD, this);
            for (int i = Util.INDEX_OF_FIRST_PARKING_FIELD; i <= Util.INDEX_OF_LAST_PARKING_FIELD; i++)
                playerFields[i] = new ParkingLotField(player, i, this);
            for (int i = Util.INDEX_OF_FIRST_COMMON_FIELD; i <= Util.INDEX_OF_LAST_COMMON_FIELD; i++)
                playerFields[i] = new PublicField(player.ID, i, this);
            fields[player.ID] = playerFields;
        }

        // Set connections between Fields of Board
        for (int i = 0; i < nofPlayer; i++)
        {
            //HomeField connection to StartField
            if (fields[i][Util.INDEX_OF_HOME_FIELD] instanceof HomeField
                    && fields[i][Util.INDEX_OF_START_FIELD] instanceof StartField)
                ((HomeField)fields[i][Util.INDEX_OF_HOME_FIELD])
                        .setStartField((StartField)fields[i][Util.INDEX_OF_START_FIELD]);

            //StartField connections to parkingField and two general Fields
            if (fields[i][Util.INDEX_OF_START_FIELD] instanceof StartField)
            {
                Field nextField = fields[i][Util.INDEX_OF_FIRST_COMMON_FIELD];
                Field prevField = fields[prevIndex(i)][Util.INDEX_OF_LAST_COMMON_FIELD];
                ParkingLotField parkingField = null;
                if (fields[i][Util.INDEX_OF_FIRST_PARKING_FIELD] instanceof ParkingLotField)
                    parkingField = (ParkingLotField)fields[i][Util.INDEX_OF_FIRST_PARKING_FIELD];
                ((StartField)fields[i][Util.INDEX_OF_START_FIELD])
                        .setAdjacentFields(nextField, prevField, parkingField);
            }

            //ParkingLotField connection to next ParkingLotField
            for (int j = Util.INDEX_OF_FIRST_PARKING_FIELD; j < Util.INDEX_OF_LAST_PARKING_FIELD; j++)
            {
                if (fields[i][j] instanceof ParkingLotField && fields[i][j + 1] instanceof ParkingLotField)
                {
                    ((ParkingLotField)fields[i][j]).setNext((ParkingLotField)fields[i][j + 1]);
                }
            }

            //CommonField connections to previous and next Fields
            if (fields[i][Util.INDEX_OF_FIRST_COMMON_FIELD] instanceof PublicField
                    && fields[i][Util.INDEX_OF_START_FIELD] instanceof StartField)
            {
                ((PublicField)fields[i][Util.INDEX_OF_FIRST_COMMON_FIELD]).setAdjacentFields(
                        fields[i][Util.INDEX_OF_FIRST_COMMON_FIELD + 1], fields[i][Util.INDEX_OF_START_FIELD]);
            }
            for (int j = Util.INDEX_OF_FIRST_COMMON_FIELD + 1; j < Util.INDEX_OF_LAST_COMMON_FIELD; j++)
            {
                if (fields[i][j] instanceof PublicField)
                {
                    ((PublicField)fields[i][j]).setAdjacentFields(fields[i][j + 1], fields[i][j - 1]);
                }
            }
            if (fields[i][Util.INDEX_OF_LAST_COMMON_FIELD] instanceof PublicField
                    && fields[nextIndex(i)][Util.INDEX_OF_START_FIELD] instanceof StartField)
            {
                ((PublicField)fields[i][Util.INDEX_OF_LAST_COMMON_FIELD])
                        .setAdjacentFields(fields[nextIndex(i)][Util.INDEX_OF_START_FIELD],
                                fields[i][Util.INDEX_OF_LAST_COMMON_FIELD - 1]);
            }
        }

    }

    /**
     * Gets to next lower index
     * 
     * @param i
     *        Current index
     * @return Lower index, always between 0 and nofPlayer
     */
    private int prevIndex(int i)
    {
        int res = i - 1;
        if (res < 0)
            res += nofPlayer;
        if (res >= nofPlayer)
            res = res % nofPlayer;
        return res;
    }

    /**
     * Gets to next higher index
     * 
     * @param i
     *        Current index
     * @return Lower index, always between 0 and nofPlayer
     */
    private int nextIndex(int i)
    {
        int res = i + 1;
        if (res < 0)
            res += nofPlayer;
        if (res >= nofPlayer)
            res = res % nofPlayer;
        return res;
    }

    public String displayPosition(Figure figure)
    {
        String res = "Figure " + figure.ID + " on Field " + positions.get(figure).fieldID;
        return res;
    }

    /**
     * Getter for number of Players this Board can accommodate
     * 
     * @return Integer of Number of Player
     */
    public int getNofPlayer()
    {
        return nofPlayer;
    }

    protected HomeField findHomeField(Player player)
    {
        if (fields[player.ID][Util.INDEX_OF_HOME_FIELD] instanceof HomeField)
            return (HomeField)fields[player.ID][Util.INDEX_OF_HOME_FIELD];
        return null;
    }

    protected Field findSpecificField(Player player, int index)
    {
        return fields[player.ID][index];

    }

    public Board copy()
    {
        Board res = new Board(game, nofPlayer);

        res.createBoard();

        res.isVirtual = true;

        //copy positions
        for (Figure figure: positions.keySet())
        {
            Field original = positions.get(figure);
            Field target = res.accordingFieldOnBoard(original);
            res.positions.put(figure, target);
            target.occupant = figure;
        }

        return res;
    }

    public Field accordingFieldOnBoard(Field field)
    {
        return fields[field.playerID][field.fieldID];
    }
}
