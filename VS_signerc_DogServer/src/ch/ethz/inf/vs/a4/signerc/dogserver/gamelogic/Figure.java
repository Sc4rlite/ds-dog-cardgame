package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.HomeField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.ParkingLotField;


public class Figure
{
    /**
     * Player that owns this Figure
     */
    public final Player owner;

    /**
     * Together with the player, this ID uniquely defines a figure
     */
    public final int ID;

    /**
     * Determines if Figure if still active (has not yet reached parking lot)
     */
    public boolean active()
    {
        Field field = owner.getGame().getBoard().positions.get(this);
        return !((field instanceof ParkingLotField) && ((ParkingLotField)field).isInactiveParkingLotField());
    }

    /**
     * Determines if Figure is still at home, needs enter race before doing
     * anything else
     */
    public boolean atHome()
    {
        Field field = owner.getGame().getBoard().positions.get(this);
        return field instanceof HomeField;
    }

    /**
     * Constructor at start of game
     * 
     * @param o
     *        Player that owns this Figure
     */
    public Figure(Player o, int ID)
    {
        owner = o;
        this.ID = ID;
    }
}
