package ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.util.Log;
import ch.ethz.inf.vs.a4.signerc.dogserver.communication.ConnectionListener;
import ch.ethz.inf.vs.a4.signerc.dogserver.communication.ConnectionManager;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.BlueCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.JokerCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.OneElevenCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.SevenCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.SwitchCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.ThirteenCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Card.FourCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.HomeField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.Field.ParkingLotField;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameReceiveEvent.GameReceiveEventReady;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameReceiveEvent.GameReceiveEventReceiveCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameReceiveEvent.GameReceiveEventReceiveSwitchCard;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameReceiveEvent.GameReceiveEventRejoined;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameReceiveEvent.GameReceiveEventStart;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameSendEvent.DistributeCardsSendEvent;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameSendEvent.FinishedSendEvent;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameSendEvent.RejectCardSendEvent;
import ch.ethz.inf.vs.a4.signerc.dogserver.gamelogic.GameSendEvent.TurnSendEvent;
import ch.ethz.inf.vs.a4.signerc.dogserver.graphic.GamePanel;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Circle;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util;
import ch.ethz.inf.vs.a4.signerc.dogserver.util.Util.CardType;


/**
 * Class that represents the Game that is played at the moment
 * 
 * @author Paige and Sphinx
 *
 */
public class Game implements ConnectionListener
{
    /**
     * All Players that participate in this Game
     */
    private final Circle<Player> players;

    /**
     * The Board this Game is played on
     */
    private final Board board;

    /**
     * All Cards that belong to the Game
     */
    private final Map<Integer, Card> allCards;

    /**
     * All Cards that have not been distributed yet
     */
    private final List<Card> deck;

    /**
     * ConnectionManager that connects all participants of the game
     */
    public final ConnectionManager connection;

    /**
     * Name of this Game
     */
    private final String gameName;

    /**
     * Represents whether this Game has been finished
     */
    private boolean finished;

    /**
     * GameReceiveEvent of receiving a sent Card that happened last
     */
    private GameReceiveEventReceiveCard recentEvent;

    /**
     * the last received switch card events
     */
    private List<GameReceiveEventReceiveSwitchCard> recentSwitchEvents;

    /**
     * the last received ready events
     */
    private List<GameReceiveEventReady> recentReadyEvents = new LinkedList<GameReceiveEventReady>();

    private boolean waitingForSwitchCards;

    private Player waitingForCard;

    /**
     * Winner of this Game
     */
    private Player winner = null;

    /**
     * Handler that represents the main Thread this Game is played on
     */
    private Handler comebackHandler;

    /**
     * Main activity that this game runs on
     */
    private final GamePanel main;

    /**
     * Constructor
     * 
     * @param boardSize
     *        Number of Players the Board this Game is played on should
     *        accommodate
     * @param name
     *        Name of Game, null if none was entered
     */
    public Game(int boardSize, String name, GamePanel panel)
    {
        //create Board
        assert (boardSize == 4 || boardSize == 6);
        board = new Board(this, boardSize);
        //Initialise all final fields
        players = new Circle<Player>();
        //        comebackHandler = new Handler();
        if (name != null)
            gameName = name;
        else
            gameName = "DOG GAME";
        connection = new ConnectionManager();
        connection.registerListener(this);
        connection.startServer(gameName, boardSize);
        allCards = new HashMap<Integer, Card>();
        createAllCards();
        deck = new LinkedList<Card>();
        //shuffleCards();
        main = panel;
    }

    /**
     * Create all Cards that belong to this Game
     */
    private void createAllCards()
    {
        for (int id = Util.ID_OF_FIRST_1_11; id < Util.ID_OF_FIRST_1_11 + Util.NUMBER_OF_1_11_CARDS; id++)
        {
            allCards.put(id, new OneElevenCard(id));
        }
        for (int id = Util.ID_OF_FIRST_2; id < Util.ID_OF_FIRST_2 + Util.NUMBER_OF_2_CARDS; id++)
        {
            allCards.put(id, new BlueCard(id, 2));
        }
        for (int id = Util.ID_OF_FIRST_3; id < Util.ID_OF_FIRST_3 + Util.NUMBER_OF_3_CARDS; id++)
        {
            allCards.put(id, new BlueCard(id, 3));
        }
        for (int id = Util.ID_OF_FIRST_4; id < Util.ID_OF_FIRST_4 + Util.NUMBER_OF_4_CARDS; id++)
        {
            allCards.put(id, new FourCard(id));
        }
        for (int id = Util.ID_OF_FIRST_5; id < Util.ID_OF_FIRST_5 + Util.NUMBER_OF_5_CARDS; id++)
        {
            allCards.put(id, new BlueCard(id, 5));
        }
        for (int id = Util.ID_OF_FIRST_6; id < Util.ID_OF_FIRST_6 + Util.NUMBER_OF_6_CARDS; id++)
        {
            allCards.put(id, new BlueCard(id, 6));
        }
        for (int id = Util.ID_OF_FIRST_7; id < Util.ID_OF_FIRST_7 + Util.NUMBER_OF_7_CARDS; id++)
        {
            allCards.put(id, new SevenCard(id));
        }
        for (int id = Util.ID_OF_FIRST_8; id < Util.ID_OF_FIRST_8 + Util.NUMBER_OF_8_CARDS; id++)
        {
            allCards.put(id, new BlueCard(id, 8));
        }
        for (int id = Util.ID_OF_FIRST_9; id < Util.ID_OF_FIRST_9 + Util.NUMBER_OF_9_CARDS; id++)
        {
            allCards.put(id, new BlueCard(id, 9));
        }
        for (int id = Util.ID_OF_FIRST_10; id < Util.ID_OF_FIRST_10 + Util.NUMBER_OF_10_CARDS; id++)
        {
            allCards.put(id, new BlueCard(id, 10));
        }
        for (int id = Util.ID_OF_FIRST_12; id < Util.ID_OF_FIRST_12 + Util.NUMBER_OF_12_CARDS; id++)
        {
            allCards.put(id, new BlueCard(id, 12));
        }
        for (int id = Util.ID_OF_FIRST_13; id < Util.ID_OF_FIRST_13 + Util.NUMBER_OF_13_CARDS; id++)
        {
            allCards.put(id, new ThirteenCard(id));
        }
        for (int id = Util.ID_OF_FIRST_SWITCH; id < Util.ID_OF_FIRST_SWITCH + Util.NUMBER_OF_SWITCH_CARDS; id++)
        {
            allCards.put(id, new SwitchCard(id));
        }
        for (int id = Util.ID_OF_FIRST_JOKER; id < Util.ID_OF_FIRST_JOKER + Util.NUMBER_OF_JOKER_CARDS; id++)
        {
            allCards.put(id, new JokerCard(id));
        }
    }

    /**
     * Start Game by initialising all Players and create and initialising Board
     * 
     * @param event
     *        The GameReceiveEventStart that triggered the call of this function
     */
    private void startGame(GameReceiveEventStart event)
    {
        //implement startGame with creating Players and initialising Board
        Player[] playersArray = new Player[event.names.length];
        for (int i = 0; i < event.names.length; i++)
        {
            Player player = new Player(event.names[i], i, this);
            player.setColour(event.colours[i]);
            players.add(player);
            playersArray[i] = player;
        }
        //set Teammates
        playersArray[0].setTeam(playersArray[2]);
        playersArray[1].setTeam(playersArray[3]);
        assert (players.size == board.nofPlayer);
        board.initialiseBoard();
        main.setPlayers(playersArray, board);
        finished = false;
        Log.i("Game", "Started");
        play();
    }

    /**
     * Play game until winners are determined
     */
    private void play()
    {
        //wait before starting to play until all players are ready
        while (recentReadyEvents.size() != players.size)
        {
            try
            {
                synchronized (this)
                {
                    wait();
                }
            }
            catch (InterruptedException e)
            {
                // TODO Catch Problem with wait
                e.printStackTrace();
            }
        }

        Log.i("Game", "All powders ready");

        while (!finished)
        {
            Log.i("Game", "New round started");
            for (int i = 6; i > 1; i--)
            {
                if (finished)
                    break;

                distributeCards(i);

                Log.i("Game", "wait for switch cards");
                waitingForSwitchCards = true;
                //wait for SwitchCards and when all are received apply switching and inform players
                while (recentSwitchEvents.size() != players.size)
                {
                    try
                    {
                        synchronized (this)
                        {
                            wait();
                        }
                    }
                    catch (InterruptedException e)
                    {
                        // TODO Catch Problem with wait
                        e.printStackTrace();
                    }
                }
                waitingForSwitchCards = false;
                for (GameReceiveEventReceiveSwitchCard event: recentSwitchEvents)
                {
                    Player player = getPlayerWithID(event.sender);
                    Card card = allCards.get(event.getCardID());
                    Log.i("Switch Card", String.format("Player %d sent Card %d", player.ID, card.ID));
                    Log.i("Switch card", player.displayHand());
                    if (!player.hand.contains(card))
                        Log.i("Switch card", "Card not in hand");
                    player.sendCard(card);
                    player.getTeamMember().receiveCard(card);
                    List<Card> cards = new LinkedList<Card>();
                    cards.add(card);
                    sendMessage(new DistributeCardsSendEvent(player.getTeamMember().ID, cards, false));
                }

                for (int j = 0; j < i; j++)
                {
                    for (Player player: players)
                    {
                        if (turn(player))
                            break;
                    }
                    if (finished)
                        break;
                }
                players.advance();
                try
                {
                    Thread.sleep(5000);
                }
                catch (InterruptedException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        //announce winner team
        for (Player player: players)
            sendMessage(new FinishedSendEvent(player.ID, winner));

        //wait a moment for clients to receive winner announcement
        try
        {
            Thread.sleep(20000);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        connection.disconnect();
        connection.startServer(null, 4);
        main.showRestart(winner.name + " and " + winner.teamMember.name + " have won! Congratulations!!");
    }

    /**
     * distributes Cards to all Players
     * 
     * @param amount
     *        Amount of Cards that distributed to each Player
     */
    private void distributeCards(int amount)
    {
        //TODO test distributeCards
        if (amount * players.size > deck.size())
            shuffleCards();
        for (Player player: players)
        {
            Log.i("Distribute Cards", "To Player " + player.name);
            List<Card> cards = new LinkedList<Card>();
            for (int i = 0; i < amount; i++)
            {
                cards.add(deck.get(0));
                deck.remove(0);
            }
            player.receiveHand(cards);
            recentSwitchEvents = new LinkedList<GameReceiveEventReceiveSwitchCard>();
            sendMessage(new DistributeCardsSendEvent(player.ID, cards, true));
        }
    }

    /**
     * Send given GameSendEvent to its receiver
     * 
     * @param event
     *        Event that has to be sent
     */
    private void sendMessage(GameSendEvent event)
    {
        connection.send(event.receiver, event.json);
    }

    /**
     * Fills and shuffles deck. Afterwards all Cards are in deck.
     */
    private void shuffleCards()
    {
        deck.clear();
        for (Card card: allCards.values())
        {
            deck.add(card);
        }
        Collections.shuffle(deck);
    }

    /**
     * One turn of a certain Player
     * 
     * @param player
     *        Player whose turn it is
     * @return Boolean whether game was finished. True means game was finished.
     */
    private boolean turn(Player player)
    {
        //        for (Figure figure: board.positions.keySet())
        //            Log.i("position", board.displayPosition(figure));
        //TODO implement turn of a Player
        Map<Card, List<Move>> possibleMoves = player.possiblePlays();

        for (Card c: possibleMoves.keySet())
        {
            for (Move m: possibleMoves.get(c))
                Log.i("PossibleMoves", "Card " + c.ID + " " + m.displayMove());
        }
        //event that is sent to player
        GameSendEvent event;
        //card that was received from player
        Card card;
        //possible moves of the received card
        List<Move> moves = null;
        //determines whether first card has been requested or card has been rejected
        boolean first = true;
        //check whether player has valid moves
        Log.i("Turn", player.displayHand());
        if (possibleMoves != null && !possibleMoves.isEmpty())
        {
            waitingForCard = player;
            event = new TurnSendEvent(player.ID, true);
            do
            {
                recentEvent = null;
                if (!first)
                {
                    event = new RejectCardSendEvent(player.ID);
                }
                sendMessage(event);
                while (recentEvent == null || recentEvent.sender != player.ID)
                {
                    try
                    {
                        //                    if (recentEvent.sender != player.ID)
                        //                        recentEvent = null;
                        synchronized (this)
                        {
                            wait();
                        }
                    }
                    catch (InterruptedException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                assert (recentEvent.sender == player.ID);
                GameReceiveEventReceiveCard myEvent = recentEvent;
                card = allCards.get(recentEvent.getCardID());
                if (!player.hand.contains(card))
                    Log.i("Play card", "Card not in hand");
                if (card != null && card.cardType != CardType.JOKER && possibleMoves.get(card) != null)
                    moves = new LinkedList<Move>(possibleMoves.get(card));
                else if (card != null)
                {
                    if (player.allFiguresParked())
                        moves = myEvent.getCardOfType().play(player.teamMember);
                    else
                        moves = myEvent.getCardOfType().play(player);
                }
                first = false;
            } while (moves == null || moves.isEmpty());

            player.playCard(card);
            waitingForCard = null;

            Move chosenMove = null;

            for (Move m: moves)
                Log.i("Moves", m.displayMove());

            chosenMove = main.chooseMove(player, card.cardType, moves);

            Log.i("Move", "Move chosen");

            if (chosenMove != null)
            {
                for (Figure figure: chosenMove.movedFigures())
                {
                    //TODO check if Figure is parked (!active)
                    Field oldField = board.positions.get(figure);
                    Field newField = chosenMove.move(figure);
                    if (oldField.occupant != null && oldField.occupant.equals(figure))
                        oldField.occupant = null;
                    if (oldField instanceof HomeField)
                        ((HomeField)oldField).leaveHome(figure);
                    if (newField instanceof HomeField)
                        ((HomeField)newField).comeHome(figure);
                    newField.occupant = figure;
                    board.positions.put(figure, newField);
                }
            }
        }
        else if (!player.hand.isEmpty())
        {
            sendMessage(new TurnSendEvent(player.ID, false));
            player.dumpCards();
        }

        if (!player.allFiguresParked())
            return false;
        if (!player.getTeamMember().allFiguresParked())
            return false;
        winner = player;
        finished = true;
        return true;
    }

    public void handlingRejoin(GameReceiveEventRejoined event)
    {
        Player player = getPlayerWithID(event.getID());
        if (waitingForSwitchCards && containsSwitchEventFrom(event.getID()) != null)
            recentSwitchEvents.remove(containsSwitchEventFrom(event.getID()));
        sendMessage(new DistributeCardsSendEvent(player.ID, player.hand, waitingForSwitchCards));
        if (waitingForCard != null && waitingForCard.equals(player))
            sendMessage(new TurnSendEvent(player.ID, true));
    }

    private GameReceiveEventReceiveSwitchCard containsSwitchEventFrom(int playerID)
    {
        for (GameReceiveEventReceiveSwitchCard temp: recentSwitchEvents)
        {
            if (temp.sender == playerID)
                return temp;
        }
        return null;
    }

    @Override public Handler getHandler()
    {
        //TODO implement getHandler
        synchronized (this)
        {
            notifyAll();
        }
        return comebackHandler;
    }

    @Override public void ReceiveEvent(final int sender, final JSONObject json)
    {
        //TODO implement ReceiveEvent

        assert (json != null);
        try
        {
            if (json.getString("cmd").equals("playCard"))
                recentEvent = new GameReceiveEventReceiveCard(json, sender);
            else if (json.getString("cmd").equals("switchCard"))
            {
                GameReceiveEventReceiveSwitchCard event = new GameReceiveEventReceiveSwitchCard(json, sender);

                if (containsSwitchEventFrom(event.sender) == null)
                    recentSwitchEvents.add(event);
            }
            else if (json.getString("cmd").equals("ready"))
            {
                GameReceiveEventReady event = new GameReceiveEventReady(json, sender);
                boolean contained = false;
                for (GameReceiveEventReady temp: recentReadyEvents)
                {
                    if (temp.sender == event.sender)
                    {
                        contained = true;
                        break;
                    }
                }
                if (!contained)
                    recentReadyEvents.add(event);
            }
            else if (json.getString("cmd").equals("start"))
            {

                new Thread(new Runnable()
                {

                    @Override public void run()
                    {
                        startGame(new GameReceiveEventStart(json, sender));

                    }
                }).start();

            }
            else if (json.getString("cmd").equals("exception"))
            {
                new Thread(new Runnable()
                {

                    @Override public void run()
                    {
                        //call main
                        //main.showRestart("There has been an exception in the Bluetooth connection.");
                    }
                }).start();
            }
            else if (json.getString("cmd").equals("rejoined"))
            {
                new Thread(new Runnable()
                {
                    @Override public void run()
                    {
                        handlingRejoin(new GameReceiveEventRejoined(json, sender));
                    }
                }).start();
            }
            else
                //TODO raise customised exception
                ;
        }
        catch (JSONException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        synchronized (this)
        {
            notifyAll();
        }
    }

    /**
     * Getter for players
     * 
     * @return Circle of Players that are participating in this Game
     */
    public Circle<Player> getPlayers()
    {
        return players;
    }

    /**
     * Getter method for board
     * 
     * @return Board of this game
     */
    public Board getBoard()
    {
        return board;
    }

    /**
     * Get Player object with a certain Player ID
     * 
     * @param id
     *        ID of requested Player
     * @return Player thats ID is id or null
     */
    public Player getPlayerWithID(int id)
    {
        for (Player player: players)
        {
            if (player.ID == id)
                return player;
        }
        return null;
    }

}
